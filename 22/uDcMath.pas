unit uDcMath;

interface

uses
{$IF CompilerVersion >= 23.0}
  WinApi.Windows,
  System.Math,
  // System.Math.Vectors,
{$ELSE}
  Windows,
  Math,
{$IFEND}
  uRecords,
  uConstants;

type
  // TFloat = Single;
  // TFloat = Double;
  TFloat = Extended;

  // TdcPointArray = array of dcPoint;

  TPolyEdge3D = record
    Pt1, Pt2: dcPoint;
  end;

  TPolygon3D = packed record
    Points: array of dcPoint;
    Edges: array of TPolyEdge3D;
    nPnts,
       MaxPoly: Integer;
  end;

  TPlane3D = record
    Point: dcPoint;
    Normal: dcPoint;
  end;

  TLine3D = array [1 .. 2] of dcPoint3D;
  // TLine3DArray = array of TLine3D;

  TTriangle3D = array [1 .. 3] of dcPoint;

  // TMaxPntArr = array [1 .. MaxPoly] of dcPoint;

  TDirection = (dirX, dirY, dirZ);

  TPolyEdge = array [1 .. 2] of Integer;
  TPolyEdgeNeighbors = array [1 .. 2] of TPolyEdge;

  TStartOption = (soHighest, soLowest);
  TIntersectionResult = (irNone, irIntersect, irParallel, irSkew, irCoincident);

  TPointPair = record
    Point1, Point2: dcPoint;
  end;

const
  Epsilon: TFloat = 1E-6;
  cTiny: TFloat = 1E-3;
  cTiny_E12: TFloat = 1.0E-12; { One Trillionth }
  cFarAway: TFloat = 3.84E+8; { One Million Feet }
  MatSize: Integer = 4;
  cZero: TFloat = 0.0;
  cOne: TFloat = 1.0;
  cZeroPt: Point = (x: 0.0; y: 0.0; z: 0.0);
  cZeroVec: Point = (v: (0.0, 0.0, 0.0));

var
  varZeroPt: Point = (x: 0.0; y: 0.0; z: 0.0);
  varZeroVec: Point = (v: (0.0, 0.0, 0.0));

  // function Point3D_DC(PtA: TPoint3D): dcPoint;
  // function dcPoint_3D(Pt1: dcPoint): TPoint3D;

function dcPointNew(x: TFloat = 0.0; y: TFloat = 0.0; z: TFloat = 0.0): dcPoint;

function TooFarAway(Pt: dcPoint; TestPt: dcPoint; TooFar: TFloat = 3.84E+8; IgnoreZ: Boolean = False): Boolean;

// function dcPntArr(PtArr: PntArr): PntArr;
// function MaxPntArr(PtArr: PntArr): PntArr;

function AddPoints(const A, B: dcPoint): dcPoint; overload;
function AddPoints(const A, B: TPoint): dcPoint; overload;

function SubtractPoints(const A, B: dcPoint): dcPoint; overload;
function SubtractPoints(const A, B: TPoint): dcPoint; overload;

// function CrossProduct(const A, B: dcPoint): dcPoint; overlaod;

// function MultiplyPointByScalar(const A: dcPoint; const Scalar: TFloat): dcPoint; { overload; }

function DivideVectorByScalar(const Vec: dcPoint; Scalar: TFloat): dcPoint;

// function DotProduct(const A, B: dcPoint): TFloat; { overload; }

function SquaredDistance(Pt: dcPoint): TFloat; { overload; }

procedure NormalizeVector(var Pt1: dcPoint);

function SurfaceNormal(const p1, p2, p3: dcPoint): dcPoint; overload;
function SurfaceNormal(const PtArr: PntArr): dcPoint; overload;

function OrthoNormalize(const v1, v2: dcPoint): dcPoint;

function NormalsAreEqual(const n1, n2: dcPoint): Boolean; { overload; }

function NormalsAreOpposite(const n1, n2: dcPoint): Boolean; { overload; }

function NewTriangle(): TTriangle3D; { overload; }

function FarPoint(Pt1, Pt2, TestPoint: dcPoint): dcPoint;

function NearPoint(Pt1, Pt2, TestPoint: dcPoint): dcPoint; { overload; }

function SamePoint(Pt1, Pt2: dcPoint; Epsilon: TFloat; IgnoreZ: Boolean = False): Boolean; { overload; }

procedure SwapPoint(var Pt1, Pt2: dcPoint);

function SameLine(const Ln1A, Ln1B, Ln2A, Ln2B: dcPoint; const Epsilon: TFloat; SameDir: Boolean = False): Boolean;

procedure MatSet(var Mat: ModMat; Scalar: TFloat);
procedure SetIdent(var Mat: ModMat);

function MultiplyMatPoint(const Mat: ModMat; const Pt: dcPoint): dcPoint; { overload; }

function CopyMat(const Mat: ModMat): ModMat;

procedure CopyTransMat(const Mat1: ModMat; var Mat2: ModMat);

function LineToMatrix(const Line: TLine3D): ModMat;

function MatrixMultiply(Mat1, Mat2: ModMat): ModMat;

function TransposeMatrix(const M: ModMat): ModMat;

function TranslationMatrix(const Tx, Ty, Tz: TFloat): ModMat;

function RotatePoints(Points: PntArr; nPnts: Integer; RotMat: ModMat): PntArr;

function ProjectPoint3D(RefPnt, InPnt: dcPoint; NewLen: TFloat): dcPoint;

function RotationMatrixFromNormal(const Normal: dcPoint): ModMat;
function RotationMatrixToXYPlane(const Normal: dcPoint): ModMat;

function RotatePolyToXYPlane(const Poly: PntArr; nPnts: Integer; out Centroid: dcPoint; out RotMat: ModMat): PntArr;

function Magnitude(v: dcPoint): TFloat; { overload; }

function Distance3D(Pt1, Pt2: dcPoint; IgnoreZ: Boolean = False): TFloat; overload;
function Distance3D(Ln1: TLine3D; IgnoreZ: Boolean = False): TFloat; overload;

function MidPoint(const Pt1, Pt2: dcPoint): dcPoint;

function FindSamePointInArray(Pt: dcPoint; Points: PntArr; nPnts: Integer): Integer; { overload; }
function FindNearPointInArray(InPt: dcPoint; Points: PntArr; nPnts: Integer; IgnoreZ: Boolean = False): Integer;

function PointsAreCoplanar(p1, p2, p3, p4: dcPoint): Boolean; overload;
function PointsAreCoplanar( { const } Points: PntArr; nPnts: aSInt): Boolean; overload;

procedure FixPolygonPoints(var PPnts: PntArr; nPnts: Integer; var OrigNormal: dcPoint; FixIt: Boolean = True);

function PointsAreCollinear(p1, p2, p3: dcPoint): Boolean;

function RemoveCollinearPoints(const Points: PntArr; nPnts: Integer; out NewPoints: PntArr; out nPntsNew: Integer): Boolean;

function IsPointOnLine(Pt1: dcPoint; Ln1: TLine3D; InBetween: Boolean = True; SameDir: Boolean = False): Boolean;

function MatString(const aMat: ModMat): string;

function RemovePointFromPolygonFunc(const Points: PntArr; nPnts: Integer; IndexToRemove: Integer): PntArr; overload;
procedure RemovePointFromPolygon(var Points: PntArr; var nPnts: Integer; const IndexToRemove: Integer); overload;

function RemoveDuplicatePoints(const Points: PntArr; nPnts: Integer; Epsilon: TFloat; out NewPoints: PntArr; out nPntsNew: Integer): Boolean;

// function PointToLineDistance(Pt, LnStart, LnEnd: dcPoint; IgnoreZ: Boolean = False): TFloat;

function ClosestEdge(Pt: dcPoint; Polygon: PntArr; nPnts: aSInt; out Neighbors: TPolyEdgeNeighbors; IgnoreZ: Boolean = False): TPolyEdge;

// function LineIntersectionPoint3D(Ln1, Ln2: TLine3D; var NewPoint: dcPoint; Flatten: Boolean = False): TIntersectionResult;

function LineIntersectionToPlane(IntPnt: dcPoint; Ln1, Ln2: TLine3D): TTriangle3D;

function VirtualLineIntersection3D(Ln1A, Ln1B, Ln2A, Ln2B: dcPoint; var IntPoint: dcPoint3D; Flatten: Boolean = False; ErrMsg: Boolean = False;
   TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
function VirtualLineIntersection3D(const Ln1, Ln2: TLine3D; var IntPoint: dcPoint3D; Flatten: Boolean = False; ErrMsg: Boolean = False; TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
function VirtualLineIntersection3D(Edge, PolyEdge: TPolyEdge3D; var IntPoint: dcPoint3D; Flatten: Boolean = False; ErrMsg: Boolean = False;
   TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
function VirtualLineIntersection3D(Ln1Pt1, Ln1Pt2, Ln2Pt1, Ln2Pt2: dcPoint; Flatten: Boolean = False; ErrMsg: Boolean = False;
   TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;

function LineIntersectionPoint3D(Ln1, Ln2: TLine3D; var NewPoint: dcPoint; Flatten: Boolean = False): TIntersectionResult;

function IntersectionLine2Poly(const Line: TLine3D; const Points: PntArr; nPnts: aSInt; out IPoint: dcPoint3D; TooFar: TFloat = 3.84E+8): Boolean;
function IntersectionLine2Plane(const Line: TLine3D; const PlanePoint, PlaneNormal: dcPoint; out IPoint: dcPoint): Boolean;

function IntersectionOfTwoPlanesLoHi(Poly1, Poly2: PntArr; nPnts1, nPnts2: aSInt;
   Adjacent1, Adjacent2: TPolyEdgeNeighbors; out IntPntAdj1, IntPntAdj2: TLine3D; out IntPoints: TArray<dcPoint>;
   out IntLine: TLine3D): Boolean; { overload; }

// function PlaneIntersection(n2, A2, n1, A1: dcPoint; out IntersectionLine: TLine3D): Boolean;
// function FindIntersectionLine(p1, p2, P3: dcPoint; out IntersectionLine: TLine3D): Boolean;
// function FindIntersectionLineBetweenPlanes(p1, p2, P3, q1, q2, Q3: dcPoint; out IntersectionLine: TLine3D): Boolean;

function GetIntersectionPlane(const Ln1, Ln2: TLine3D): TPlane3D;
function GetEquationOfPlane(const Ln1, Ln2: TLine3D): string;

function OffsetLine3D(const Line: TLine3D; Distance: TFloat; Direction: TDirection): TLine3D;
function PerpendicularOffsetLine3D(const Line: TLine3D; Distance: TFloat; { const } PerpendicularDirection: dcPoint): TLine3D;
function IntersectionOffsetLines3D(const Line1, Line2: TLine3D; Distance: TFloat): TArray<TLine3D>;

function FindHorizontalLine( { const } Points: PntArr; nPnts: aSInt; StartOption: TStartOption; out Angle: TFloat;
   out StartPoint, EndPoint: dcPoint): Boolean;

procedure ClosestEndPointPair(var Edge1, Edge2: TLine3D; IgnoreZ: Boolean = False);

function Centroid3D(const Polygon: PntArr; nPnts: aSInt): dcPoint; { overload; }

function CentroidOfTriangle(TT: TTriangle3D): dcPoint;

function PolygonHorizontalAngle(const Polygon: PntArr; nPnts: aSInt; Perpendicular: Boolean = False): TFloat; { overload; }

function DirectionDistanceLineHorizontal(Direction: TFloat; Distance: TFloat; Pt: dcPoint): TLine3D;

function ArePolygonEdgesCrossing(const Polygon: PntArr; nPnts: Integer): Boolean;

procedure ReversePolygonPoints(var Polygon: Entity);

procedure LineEndToKeep(Ln1: TLine3D; Pick1: dcPickPt; Intersection: dcPoint3D; out LE1, Opp1: dcPoint; Opposite: Boolean = False; IsOrtho: Boolean = True);
procedure LineEndsToKeep(Ln1, Ln2: TLine3D; Pick1, Pick2: dcPickPt; Intersection: dcPoint3D; out LE1, Opp1, LE2, Opp2: dcPoint; Opposite: Boolean = False;
   IsOrtho: Boolean = True);

procedure ClosestEndPoint(Ln: TLine3D; TestPt: dcPoint; out ClosePt, FarPt: dcPoint; IgnoreZ: Boolean = False);

function ClosestPointOnLine(Ln: TLine3D; TestPt: dcPoint; IgnoreZ: Boolean = False): dcPoint;

function GetIncludedAngle(const Ln1, Ln2: TLine3D): Double;
function GetAngleBisectorLine(const Ln1, Ln2: TLine3D): TLine3D;

procedure CalculateIntersectionPlane(const p1, v1, p2, v2: dcPoint; out PlanePoint, PlaneNormal: dcPoint);

function Hypot(const x, y: TFloat): TFloat; overload;
function Hypot(const x, y, z: TFloat): TFloat; overload;

function PolygonWasFixed(var Ent: Entity; FixIt: Boolean = True): Boolean;

implementation

uses
{$IF CompilerVersion >= 23.0}
  System.SysUtils,
{$ELSE}
  SysUtils,
{$IFEND}
  uInterfaces,
  uInterfacesRecords,
  uDcalAddEntity,
  Trim3D_Main;

(*
  function Point3D_DC(PtA: TPoint3D): dcPoint;
  { Convert TPoint3D to dcPoint }
  begin
  Result.x := Double(PtA.x);
  Result.y := Double(PtA.y);
  Result.z := Double(PtA.z);
  end; { Point3D_DC }

  function dcPoint_3D(Pt1: dcPoint): TPoint3D;
  { Convert dcPoint to TPoint3D }
  begin
  Result := TPoint3D.Create(Pt1.x, Pt1.y, Pt1.z);
  end; { dcPoint_3D }
*)
function TooFarAway(Pt: dcPoint; TestPt: dcPoint; TooFar: TFloat = 3.84E+8; IgnoreZ: Boolean = False): Boolean;
begin
  Result := (Abs(Distance3D(TestPt, Pt, IgnoreZ)) > TooFar);
end; { TooFarAway }

function dcPointNew(x: TFloat = 0.0; y: TFloat = 0.0; z: TFloat = 0.0): dcPoint;
begin
  Result.x := x;
  Result.y := y;
  Result.z := z;
end; { dcPointNew }

(*
  function dcPntArr(PtArr: PntArr): PntArr;
  var
  i: Integer;
  begin
  for i := 1 to MaxPoly do begin
  Result[i].x := Double(PtArr[i].x);
  Result[i].y := Double(PtArr[i].y);
  Result[i].z := Double(PtArr[i].z);
  end;
  end; { dcPntArr }

  function MaxPntArr(PtArr: PntArr): PntArr;
  var
  i: Integer;
  begin
  for i := 1 to MaxPoly do begin
  Result[i].x := TFloat(PtArr[i].x);
  Result[i].y := TFloat(PtArr[i].y);
  Result[i].z := TFloat(PtArr[i].z);
  end;
  end; { MaxPntArr }
*)
function AddPoints(const A, B: dcPoint): dcPoint; overload;
begin
  Result.x := A.x + B.x;
  Result.y := A.y + B.y;
  Result.z := A.z + B.z;
end; { AddPoints }

function AddPoints(const A, B: TPoint): dcPoint; overload;
begin
  Result.x := A.x + B.x;
  Result.y := A.y + B.y;
  Result.z := cZero;
end; { AddPoints }

function SubtractPoints(const A, B: dcPoint): dcPoint; overload;
begin
  Result.x := A.x - B.x;
  Result.y := A.y - B.y;
  Result.z := A.z - B.z;
end; { SubtractPoints }

function SubtractPoints(const A, B: TPoint): dcPoint; overload;
begin
  Result.x := A.x - B.x;
  Result.y := A.y - B.y;
  Result.z := cZero;
end; { SubtractPoints }

function CrossProduct(const A, B: dcPoint): dcPoint;
begin
  Result.x := A.y * B.z - A.z * B.y;
  Result.y := A.z * B.x - A.x * B.z;
  Result.z := A.x * B.y - A.y * B.x;
end; { CrossProduct - Multiply }

function MultiplyPoints(const Pt1, Pt2: dcPoint): dcPoint; { overload; }
begin
  Result.x := Pt1.x * Pt2.x;
  Result.y := Pt1.y * Pt2.y;
  Result.z := Pt1.z * Pt2.z;
end; { MultiplyPoints }

function MultiplyPointByScalar(const Pt: dcPoint; const Scalar: TFloat): dcPoint; { overload; }
begin
  Result.x := Pt.x * Scalar;
  Result.y := Pt.y * Scalar;
  Result.z := Pt.z * Scalar;
end; { MultiplyPointByScalar }

function DivideVectorByScalar(const Vec: dcPoint; Scalar: TFloat): dcPoint;
begin
  Result.x := Vec.x / Scalar;
  Result.y := Vec.y / Scalar;
  Result.z := Vec.z / Scalar;
end; { DivideVectorByScalar }

function DotProduct(const A, B: dcPoint): TFloat; { overload; }
{ If the dot product of two points (a.k.a. vectors) is zero, they are perpendicular to each other. }
begin
  Result := A.x * B.x + A.y * B.y + A.z * B.z;
end; { DotProduct }

function SquaredDistance(Pt: dcPoint): TFloat; { overload; }
begin
  Result := Sqr(Pt.x) + Sqr(Pt.y) + Sqr(Pt.z);
end; { SquaredDistance }

procedure NormalizeVector(var Pt1: dcPoint);
var
  Len: TFloat;
begin
  Len := Sqrt(Pt1.x * Pt1.x + Pt1.y * Pt1.y + Pt1.z * Pt1.z);
  if Len > cZero then
  begin
    Pt1.x := Pt1.x / Len;
    Pt1.y := Pt1.y / Len;
    Pt1.z := Pt1.z / Len;
  end;
end;

function NormalizeVectorFunc(const v: dcPoint): dcPoint; { overload; }
var
  Mag: TFloat;
begin
  Mag := Magnitude(v);

  if (Mag <> cZero) then begin
    Result.x := v.x / Mag;
    Result.y := v.y / Mag;
    Result.z := v.z / Mag;
  end
  else begin
    Result := v;
  end;
end; { NormalizeVectorFunc }

function SurfaceNormal(const p1, p2, p3: dcPoint): dcPoint; { overload; }
var
  u, v: dcPoint;
begin
  u := SubtractPoints(p2, p1);
  v := SubtractPoints(p3, p1);
  Result := CrossProduct(u, v);
end; { SurfaceNormal }

function SurfaceNormal(const PtArr: PntArr): dcPoint;
begin
  Result := SurfaceNormal(PtArr[1], PtArr[2], PtArr[3]);
end; { SurfaceNormal }

function OrthoNormalize(const v1, v2: dcPoint): dcPoint;
begin
  Result := CrossProduct(v1, v2);
  Result := NormalizeVectorFunc(Result);
end; { OrthoNormalize }

function NormalsAreEqual(const n1, n2: dcPoint): Boolean;
{ Same dcPoint }
begin
  Result :=
     (Abs(n1.x - n2.x) < Epsilon) and
     (Abs(n1.y - n2.y) < Epsilon) and
     (Abs(n1.z - n2.z) < Epsilon);
end; { NormalsAreEqual }

function NormalsAreOpposite(const n1, n2: dcPoint): Boolean; { overload; }
var
  DotProductValue: TFloat;
begin
  Result := False;

  { Calculate the dot product of the two normals }
  DotProductValue := DotProduct(n1, n2);

  if (Abs(DotProductValue + cOne) < cTiny) then
    { The normals are opposite (flipped) }
       Result := True
  else
    { The normals are not flipped }
end; { NormalsAreOpposite }

function NewTriangle(): TTriangle3D; overload;
begin
  Result[1] := dcPointNew(cZero, cOne, cZero);
  Result[2] := dcPointNew(-0.866, -0.5, cZero);
  Result[3] := dcPointNew(0.866, -0.5, cZero);
end; { NewTriangle }

function NewTriangle(Pt1, Pt2, Pt3: dcPoint): TTriangle3D; overload;
begin
  Result[1] := Pt1;
  Result[2] := Pt2;
  Result[3] := Pt3;
end; { NewTriangle }

function FarPoint(Pt1, Pt2, TestPoint: dcPoint): dcPoint;
var
  Dist1, Dist2: TFloat;
begin
  Dist1 := Distance3D(Pt1, TestPoint);
  Dist2 := Distance3D(Pt2, TestPoint);
  if (Dist1 > Dist2) then
       Result := Pt1
  else Result := Pt2;
end; { FarPoint }

function NearPoint(Pt1, Pt2, TestPoint: dcPoint): dcPoint; { overload; }
var
  Dist1, Dist2: TFloat;
begin
  Dist1 := Distance3D(Pt1, TestPoint);
  Dist2 := Distance3D(Pt2, TestPoint);
  if (Dist1 < Dist2) then
       Result := Pt1
  else Result := Pt2;
end; { NearPoint }

function SamePoint(Pt1, Pt2: dcPoint; Epsilon: TFloat; IgnoreZ: Boolean = False): Boolean; { overload; }
var
  PtA, PtB: dcPoint;
begin
  PtA := Pt1;
  PtB := Pt2;
  if IgnoreZ then begin
    PtA.z := cZero;
    PtB.z := cZero;
  end;
  Result := (Abs(Pt1.x - Pt2.x) <= Epsilon) and (Abs(Pt1.y - Pt2.y) <= Epsilon) and (Abs(Pt1.z - Pt2.z) <= Epsilon);
end; { SamePoint }

procedure SwapPoint(var Pt1, Pt2: dcPoint);
var
  TmpPt: dcPoint;
begin
  TmpPt := Pt1;
  Pt1 := Pt2;
  Pt2 := TmpPt;
end; { SwapPoint }

function SameLine(const Ln1A, Ln1B, Ln2A, Ln2B: dcPoint; const Epsilon: TFloat; SameDir: Boolean = False): Boolean;
begin
  { Same Direction }
  Result := SamePoint(Ln1A, Ln2A, Epsilon) and SamePoint(Ln1B, Ln2B, Epsilon);
  { Opposite Directions }
  if not Result and not SameDir then
       Result := SamePoint(Ln1A, Ln2B, Epsilon) and SamePoint(Ln1B, Ln2A, Epsilon);
end; { SameLine }

function IsAlmostEqual(A, B, Epsilon: TFloat): Boolean;
begin
  Result := Abs(A - B) < Epsilon;
end; { IsAlmostEqual }

function TranslationMatrix(const Tx, Ty, Tz: TFloat): ModMat;
begin
  SetIdent(Result);
  Result[1, 4] := Tx;
  Result[2, 4] := Ty;
  Result[3, 4] := Tz;
end; { TranslationMatrix }

function MatrixMultiply(Mat1, Mat2: ModMat): ModMat; overload;
var
  i, j, k: Integer;
  Temp: TFloat;

begin
  for i := 1 to 4 do
    for j := 1 to 4 do begin
      Temp := cZero;
      for k := 1 to 4 do
           Temp := Temp + Mat1[i][k] * Mat2[k][j];
      Result[i][j] := Temp;
    end;
end; { MatrixMultiply }

function MultiplyMatPoint(const Mat: ModMat; const Pt: dcPoint): dcPoint; { overload; }
var
  i: Integer;
  Temp: array [1 .. 4] of TFloat;
begin
  for i := 1 to 4 do begin
    Temp[i] :=
       Mat[i, 1] * Pt.x +
       Mat[i, 2] * Pt.y +
       Mat[i, 3] * Pt.z +
       Mat[i, 4]; { Add the translation component for points }
  end;

  { Convert back to 3D dcPoint by dividing by the homogeneous coordinate (temp[4]) }
  Result.x := Temp[1] / Temp[4];
  Result.y := Temp[2] / Temp[4];
  Result.z := Temp[3] / Temp[4];
end; { Mat4x4MultPoint }

function CopyMat(const Mat: ModMat): ModMat;
var
  i, j: Integer;
begin
  for i := 1 to 4 do
    for j := 1 to 4 do
         Result[i][j] := Mat[i][j];
end; { CopyMat }

procedure CopyTransMat(const Mat1: ModMat; var Mat2: ModMat);
{ Copy translation portion of matrix }
var
  i: Integer;
begin
  for i := 1 to 3 do
       Mat2[i][4] := Mat1[i][4];
end; { CopyTransMat }

function LineToMatrix(const Line: TLine3D): ModMat;
var
  Direction, XAxis, YAxis, ZAxis: dcPoint;
begin
  // Calculate the direction vector
  Direction := SubtractPoints(Line[2].WorldPt, Line[1].WorldPt);

  // Normalize the direction vector
  ZAxis := NormalizeVectorFunc(Direction);

  // Choose an arbitrary vector to compute the X axis
  if (Abs(ZAxis.x) < Abs(ZAxis.y)) and (Abs(ZAxis.x) < Abs(ZAxis.z)) then
       XAxis := NormalizeVectorFunc(CrossProduct(ZAxis, dcPointNew(cOne, cZero, cZero)))
  else
       XAxis := NormalizeVectorFunc(CrossProduct(ZAxis, dcPointNew(cZero, cOne, cZero)));

  // Compute the Y axis
  YAxis := NormalizeVectorFunc(CrossProduct(ZAxis, XAxis));

  // Create the transformation matrix
  Result[1, 1] := XAxis.x;
  Result[1, 2] := XAxis.y;
  Result[1, 3] := XAxis.z;
  Result[1, 4] := cZero;

  Result[2, 1] := YAxis.x;
  Result[2, 2] := YAxis.y;
  Result[2, 3] := YAxis.z;
  Result[2, 4] := cZero;

  Result[3, 1] := ZAxis.x;
  Result[3, 2] := ZAxis.y;
  Result[3, 3] := ZAxis.z;
  Result[3, 4] := cZero;

  // Result[4, 1] := Line[1].X;
  // Result[4, 2] := Line[1].Y;
  // Result[4, 3] := Line[1].Z;
  Result[4, 4] := cOne;
end; { LineToMatrix }

function RotatePoint(const Pt: dcPoint; RotMat: ModMat): dcPoint;
begin
  Result := MultiplyMatPoint(RotMat, Pt);
end; { RotatePoint }

function RotatePoints(Points: PntArr; nPnts: Integer; RotMat: ModMat): PntArr; { overload; }
var
  i: Integer;
begin
  for i := 1 to nPnts do
       Result[i] := RotatePoint(Points[i], RotMat);
end; { RotatePoints }

function ProjectPoint3D(RefPnt, InPnt: dcPoint; NewLen: TFloat): dcPoint;
{ Project InPnt along line extending from RefPnt to InPnt so Result is located at a distance of NewLen from RefPnt. }
var
  Factor, OldLen: Double;
begin
  OldLen := Distance3D(RefPnt, InPnt);
  if (OldLen < Epsilon) then begin
    Result := RefPnt;
    Exit;
  end;

  Factor := NewLen / OldLen;
  Result := AddPoints(RefPnt, MultiplyPointByScalar((SubtractPoints(InPnt, RefPnt)), Factor));

  // Result.x := RefPnt.x + (InPnt.x - RefPnt.x) * Factor;
  // Result.y := RefPnt.y + (InPnt.y - RefPnt.y) * Factor;
  // Result.z := RefPnt.z + (InPnt.z - RefPnt.z) * Factor;
end; { ProjectPoint3D }

procedure MatSet(var Mat: ModMat; Scalar: TFloat);
var
  i, j: Integer;
begin
  for i := 1 to MatSize do begin
    for j := 1 to MatSize do begin
      Mat[i, j] := Scalar;
    end;
  end;
end; { MatSet }

procedure SetIdent(var Mat: ModMat);
var
  i: aSInt;
begin
  MatSet(Mat, cZero);
  for i := 1 to MatSize do begin
    Mat[i, i] := cOne;
  end;
end; { SetIdent }

function TransposeMatrix(const M: ModMat): ModMat;
var
  i, j: Integer;
begin
  for i := 1 to 4 do
    for j := 1 to 4 do
         Result[j, i] := M[i, j];
end; { TransposeMatrix }

function RotationMatrixFromNormal(const Normal: dcPoint): ModMat;
var
  XAxis, YAxis, ZAxis: dcPoint;
  arbitraryVec: dcPoint;

begin
  ZAxis := NormalizeVectorFunc(Normal);

  if (Abs(ZAxis.x) <= Abs(ZAxis.y)) and (Abs(ZAxis.x) <= Abs(ZAxis.z)) then
       arbitraryVec := dcPointNew(cOne, cZero, cZero)
  else if Abs(ZAxis.y) <= Abs(ZAxis.z) then
       arbitraryVec := dcPointNew(cZero, cOne, cZero)
  else
       arbitraryVec := dcPointNew(cZero, cZero, cOne);

  XAxis := OrthoNormalize(ZAxis, arbitraryVec);
  YAxis := OrthoNormalize(ZAxis, XAxis);

  SetIdent(Result);
  Result[1, 1] := XAxis.x;
  Result[1, 2] := XAxis.y;
  Result[1, 3] := XAxis.z;

  Result[2, 1] := YAxis.x;
  Result[2, 2] := YAxis.y;
  Result[2, 3] := YAxis.z;

  Result[3, 1] := ZAxis.x;
  Result[3, 2] := ZAxis.y;
  Result[3, 3] := ZAxis.z;
end; { RotationMatrixFromNormal }

function RotationMatrixToXYPlane(const Normal: dcPoint): ModMat;
{ Create a rotation matrix that can be used to rotate a point or a set of points in a way
  that the normal of a plane aligns with the Z axis, 'flattening' the plane onto the XY plane. }
var
  XAxis, YAxis, ZAxis: dcPoint;
begin
  { Normalize the input normal vector, which will be used as the Z axis of the rotation matrix.
    Normalizing ensures that the vector length is 1. }
  ZAxis := NormalizeVectorFunc(Normal);

  { This section determines the X axis of the rotation matrix.
    To avoid zero vectors when the normal is parallel to one of the cardinal axes
    (i.e., (1,0,0), (0,1,0), or (0,0,1)), we use a different vector for the cross product
    depending on the direction of ZAxis. }
  if (Abs(ZAxis.x) <= Abs(ZAxis.y)) and (Abs(ZAxis.x) <= Abs(ZAxis.z)) then
       { If ZAxis.x is the smallest component, cross with the Y axis vector. }
       XAxis := CrossProduct(dcPointNew(cZero, cOne, cZero), ZAxis)
  else
       { Otherwise, cross with the Z axis vector. }
       XAxis := CrossProduct(dcPointNew(cZero, cZero, cOne), ZAxis);

  { Normalize the X axis vector, which makes it a unit vector. }
  XAxis := NormalizeVectorFunc(XAxis);

  { Calculate the Y axis vector using the cross product of ZAxis and XAxis.
    This ensures that the Y axis is orthogonal to both the Z and X axes. }
  YAxis := CrossProduct(ZAxis, XAxis);

  { Initialize the resulting matrix as an identity matrix. }
  SetIdent(Result);

  { Set the rows of the matrix to the calculated X, Y, and Z axes.
    This forms a rotation matrix that will rotate the normal vector to align with the Z axis,
    effectively rotating the input polygon to lie on the XY plane. }
  Result[1, 1] := XAxis.x;
  Result[1, 2] := XAxis.y;
  Result[1, 3] := XAxis.z;

  Result[2, 1] := YAxis.x;
  Result[2, 2] := YAxis.y;
  Result[2, 3] := YAxis.z;

  Result[3, 1] := ZAxis.x;
  Result[3, 2] := ZAxis.y;
  Result[3, 3] := ZAxis.z;
end; { RotationMatrixToXYPlane }

function RotatePolyToXYPlane(const Poly: PntArr; nPnts: Integer; out Centroid: dcPoint; out RotMat: ModMat): PntArr;
var
  i: Integer;
  Normal: dcPoint;
  TransMat: ModMat;
  TransPnts: PntArr;

begin
  { Find center of polygon points }
  Centroid := Centroid3D(Poly, nPnts);
  { Move points to origin about centroid }
  TransMat := TranslationMatrix(-Centroid.x, -Centroid.y, -Centroid.z);
  for i := 1 to nPnts do
       TransPnts[i] := MultiplyMatPoint(TransMat, Poly[i]);
  { Calculate normal for translated points }
  Normal := SurfaceNormal(TransPnts);
  { Calculate rotation matrix from normal }
  RotMat := RotationMatrixToXYPlane(Normal);
  { Rotate translated points to xy plane }
  Result := RotatePoints(TransPnts, nPnts, RotMat);
end; { RotatPolytoXYPlane }

function Magnitude(v: dcPoint): TFloat; { overload; }
begin
  Result := Sqrt(Sqr(v.x) + Sqr(v.y) + Sqr(v.z));
end; { Magnitude }

function Distance3D(Pt1, Pt2: dcPoint; IgnoreZ: Boolean = False): TFloat; overload;
var
  dx, dy, dz: TFloat;
begin
  dx := Pt2.x - Pt1.x;
  dy := Pt2.y - Pt1.y;
  if IgnoreZ then
       dz := cZero
  else
       dz := Pt2.z - Pt1.z;
  Result := Sqrt(Sqr(dx) + Sqr(dy) + Sqr(dz));
end; { Distance3D }

function Distance3D(Ln1: TLine3D; IgnoreZ: Boolean = False): TFloat; overload;
begin
  Result := Distance3D(Ln1[1].WorldPt, Ln1[2].WorldPt, IgnoreZ);
end; { Distance3D }

function MidPoint(const Pt1, Pt2: dcPoint): dcPoint;
begin
  Result.x := (Pt1.x + Pt2.x) / 2.0;
  Result.y := (Pt1.y + Pt2.y) / 2.0;
  Result.z := (Pt1.z + Pt2.z) / 2.0;
end; { MidPoint }

function FindSamePointInArray(Pt: dcPoint; Points: PntArr; nPnts: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 1 to nPnts do begin
    if SamePoint(Pt, Points[i], Epsilon) then begin
      Result := i;
      Exit
    end;
  end;
end; { FindPointInArray }

function FindNearPointInArray(InPt: dcPoint; Points: PntArr; nPnts: Integer; IgnoreZ: Boolean = False): Integer;
var
  i: Integer;
  MinDist, Dist: TFloat;

begin
  Result := -1;
  MinDist := MaxDouble;

  for i := 1 to nPnts do
  begin
    if IgnoreZ then
         Dist := Distance3D(InPt, Points[i], IgnoreZ)
    else
         Dist := Distance3D(InPt, Points[i]);

    if Dist < MinDist then
    begin
      MinDist := Dist;
      Result := i;
    end;
  end;
end; { FindNearPointInArray }

// procedure DebugOutputPolygons(Poly1: PntArr; nPnts1: aSInt; Poly2: PntArr; nPnts2: aSInt);
procedure DebugOutputPolygons(Poly1: PntArr; nPnts1: aSInt);
var
  i: Integer;
  PolyStr: string;
begin
  PolyStr := 'Poly1 (nPnts1 = ' + IntToStr(nPnts1) + '):' + sLineBreak;
  for i := 1 to nPnts1 do
       PolyStr := PolyStr + Format('dcPoint%d: (%.2f, %.2f, %.2f)', [i, Poly1[i].x, Poly1[i].y, Poly1[i].z]) + sLineBreak;

  OutputDebugString(PChar(PolyStr));
  (*
    PolyStr := 'Poly2 (nPnts2 = ' + IntToStr(nPnts2) + '):' + sLineBreak;
    for i := 1 to nPnts2 do
    PolyStr := PolyStr + Format('dcPoint%d: (%.2f, %.2f, %.2f)', [i, Poly2[i].x, Poly2[i].y, Poly2[i].z]) + sLineBreak;

    OutputDebugString(PChar(PolyStr));
  *)
end;

function PointsAreCoplanar(p1, p2, p3, p4: dcPoint): Boolean; overload;
var
  v1, v2, v3: dcPoint;
  ScalarTripleProduct: TFloat;

begin
  { Calculate vectors from points }
  v1 := SubtractPoints(p2, p1);
  v2 := SubtractPoints(p3, p1);
  v3 := SubtractPoints(p4, p1);

  { Calculate scalar triple product }
  ScalarTripleProduct :=
     v1.x * (v2.y * v3.z - v2.z * v3.y) -
     v1.y * (v2.x * v3.z - v2.z * v3.x) +
     v1.z * (v2.x * v3.y - v2.y * v3.x);

  { Check for coplanarity }
  Result := Abs(ScalarTripleProduct) < cTiny; // 0.0001;
end; { ArePointsCoplanar }

function PointsAreCoplanar( { const } Points: PntArr; nPnts: aSInt): Boolean; overload;
const
  Epsilon: TFloat = 1E-6;

var
  v1, v2, v3: dcPoint;
  ScalarTripleProduct: TFloat;
  i: Integer;
begin
  if (nPnts < 3) then Exit; { Can't have less than 3 points }

  { Calculate reference vectors from the first three points }
  v1 := SubtractPoints(Points[2], Points[1]);
  v2 := SubtractPoints(Points[3], Points[1]);

  { Loop through the rest of the points and check for coplanarity }
  for i := 4 to high(Points) do begin
    v3 := SubtractPoints(Points[i], Points[1]);

    { Calculate scalar triple product }
    ScalarTripleProduct :=
       v1.x * (v2.y * v3.z - v2.z * v3.y) -
       v1.y * (v2.x * v3.z - v2.z * v3.x) +
       v1.z * (v2.x * v3.y - v2.y * v3.x);

    if Abs(ScalarTripleProduct) >= Epsilon then begin
      Result := False;
      Exit;
    end;
  end;

  Result := True;
end; { ArePointsCoplanar }

procedure FixPolygonPoints(var PPnts: PntArr; nPnts: Integer; var OrigNormal: dcPoint; FixIt: Boolean = True);
var
  First3: array [1 .. 3] of dcPoint;
  vect: dcPoint;
  longi: TFloat;
  v, w: dcPoint;
  Normal: dcPoint;
  D: TFloat;
  fixcoeff, coeff: TFloat;
  i: Integer;
  good: Boolean;

begin
  if (nPnts < 4) then Exit; { no need to test triangles }

  First3[1] := PPnts[1];
  First3[2] := PPnts[2];

  good := False;
  i := 2;
  longi := 1;
  while (i <= nPnts) and (not good) do begin
    if (i = nPnts) then
         First3[3] := PPnts[1]
    else
         First3[3] := PPnts[i];

    v := SubtractPoints(First3[1], First3[2]);
    w := SubtractPoints(First3[3], First3[2]);
    vect := CrossProduct(v, w);
    longi := Magnitude(vect);
    good := (longi > 1E-5);
    First3[1] := First3[2];
    First3[2] := First3[3];
    Inc(i);
  end;

  if good then longi := 1 / longi
  else longi := 1;

  Normal := MultiplyPointByScalar(vect, longi);
  D := -DotProduct(First3[1], Normal);
  fixcoeff := SquaredDistance(Normal);
  OrigNormal := Normal;
  if not FixIt then Exit;
  if good and (fixcoeff > cZero) then begin
    for i := 1 to nPnts do begin
      coeff := (DotProduct(Normal, PPnts[i]) + D) / fixcoeff;
      if Abs(coeff) > 0.01 then begin
        PPnts[i] := SubtractPoints(PPnts[i], MultiplyPointByScalar(Normal, coeff));
      end;
    end;
  end;
end; { FixPolygonPoints }

function RemovePointFromPolygonFunc(const Points: PntArr; nPnts: Integer; IndexToRemove: Integer): PntArr; overload;
var
  i, j: Integer;
begin
  if (nPnts < 4) then Exit; { Can't have less than 3 points }

  if (IndexToRemove < 1) or (IndexToRemove > nPnts) then
       raise Exception.Create('Invalid index to remove.');

  j := 1;
  for i := 1 to nPnts do begin
    if (i <> IndexToRemove) then begin
      Result[j] := Points[i];
      Inc(j);
    end;
  end;

  nPnts := j;
end; { RemovePointFromPolygon }

procedure RemovePointFromPolygon(var Points: PntArr; var nPnts: Integer; const IndexToRemove: Integer); overload;
var
  i, j: Integer;
  TempPoints: PntArr;

begin
  if (nPnts < 4) then Exit; { Can't have less than 3 points }

  if (IndexToRemove < 1) or (IndexToRemove > nPnts) then
       raise Exception.Create('Invalid index to remove.');

  j := 1;
  for i := 1 to nPnts do begin
    if (i <> IndexToRemove) then begin
      TempPoints[j] := Points[i];
      Inc(j);
    end;
  end;

  if (j > 1) then begin
    Points := TempPoints;
    nPnts := nPnts - 1;
  end;
end; { RemovePointFromPolygon }

function RemoveDuplicatePoints(const Points: PntArr; nPnts: Integer; Epsilon: TFloat; out NewPoints: PntArr; out nPntsNew: Integer): Boolean;
var
  i, j, k: Integer;
  HasDuplicate: Boolean;

begin
  Result := False;
  k := 1;

  for i := 1 to nPnts do begin
    HasDuplicate := False;
    for j := i + 1 to nPnts do begin
      if SamePoint(Points[i], Points[j], Epsilon) then begin
        HasDuplicate := True;
        Result := True;
        Break;
      end;
    end;

    if not HasDuplicate then begin
      NewPoints[k] := Points[i];
      Inc(k);
    end;
  end;

  nPntsNew := k;

  for i := k to MaxPoly do
       NewPoints[i] := dcPointNew();
end; { RemoveDuplicatePoints }

function PointsAreCollinear(p1, p2, p3: dcPoint): Boolean;
var
  Cross: dcPoint;

begin
  Cross := CrossProduct(SubtractPoints(p2, p1), SubtractPoints(p3, p1));

  if not((Abs(Cross.x) < Epsilon) and (Abs(Cross.y) < Epsilon) and (Abs(Cross.z) < Epsilon)) then begin
    { The three points are not collinear. }
    Result := False;
  end
  else begin
    Result := True;
  end;
end; { PointsAreCollinear }

function RemoveCollinearPoints(const Points: PntArr; nPnts: Integer; out NewPoints: PntArr; out nPntsNew: Integer): Boolean;
var
  i, prev, next: Integer;
begin
  Result := False;
  nPntsNew := 0;

  if (nPnts < 3) then begin
    NewPoints := Points;
    nPntsNew := nPnts;
    Exit;
  end;

  for i := 1 to nPnts do begin
    prev := i - 1;
    if prev < 1 then prev := nPnts;
    next := (i mod nPnts) + 1;

    if not PointsAreCollinear(Points[prev], Points[i], Points[next]) then begin
      nPntsNew := nPntsNew + 1;
      NewPoints[nPntsNew] := Points[i];
      Result := True;
    end;
  end;
end; { RemoveCollinearPoints }

function IsPointOnLine(Pt1: dcPoint; Ln1: TLine3D; InBetween: Boolean = True; SameDir: Boolean = False): Boolean;
var
  Vec1, Vec2, Vec3, CrossProd: dcPoint;
  Dir1, Dir2: dcPoint;

begin
  // Vec1 := SubtractPoints(Pt1, Ln1[1]);

  Vec1.x := Pt1.x - Ln1[1].WorldPt.x;
  Vec1.y := Pt1.y - Ln1[1].WorldPt.y;
  Vec1.z := Pt1.z - Ln1[1].WorldPt.z;

  // Vec2 := SubtractPoints(Ln1[2], Pt1);

  Vec2.x := Ln1[2].WorldPt.x - Pt1.x;
  Vec2.y := Ln1[2].WorldPt.y - Pt1.y;
  Vec2.z := Ln1[2].WorldPt.z - Pt1.z;

  // Vec3 := SubtractPoints(Ln1[2], Ln1[1]);

  Vec3.x := Ln1[2].WorldPt.x - Ln1[1].WorldPt.x;
  Vec3.y := Ln1[2].WorldPt.y - Ln1[1].WorldPt.y;
  Vec3.z := Ln1[2].WorldPt.z - Ln1[1].WorldPt.z;

  // CrossProd := CrossProduct(Vec1, Vec3);

  CrossProd.x := Vec1.y * Vec3.z - Vec1.z * Vec3.y;
  CrossProd.y := Vec1.z * Vec3.x - Vec1.x * Vec3.z;
  CrossProd.z := Vec1.x * Vec3.y - Vec1.y * Vec3.x;

  Dir1 := SubtractPoints(Ln1[2].WorldPt, Ln1[1].WorldPt);
  Dir2 := SubtractPoints(Pt1, Ln1[1].WorldPt);

  Result := (Abs(CrossProd.x - cZero) <= cTiny) and (Abs(CrossProd.y - cZero) <= cTiny) and (Abs(CrossProd.z - cZero) <= cTiny) and
     (Abs(Vec1.x + Vec2.x - Vec3.x) <= cTiny) and (Abs(Vec1.y + Vec2.y - Vec3.y) <= cTiny) and (Abs(Vec1.z + Vec2.z - Vec3.z) <= cTiny) and
     (SameDir or (DotProduct(Dir1, Dir2) >= cZero));

  { If InBetween is True, check to see if Pt1 is between Ln1[1] and Ln1[2]. }
  if InBetween then begin
    Result := Result and (Pt1.x >= Min(Ln1[1].WorldPt.x, Ln1[2].WorldPt.x)) and (Pt1.x <= Max(Ln1[1].WorldPt.x, Ln1[2].WorldPt.x)) and
       (Pt1.y >= Min(Ln1[1].WorldPt.y, Ln1[2].WorldPt.y)) and (Pt1.y <= Max(Ln1[1].WorldPt.y, Ln1[2].WorldPt.y)) and
       (Pt1.z >= Min(Ln1[1].WorldPt.z, Ln1[2].WorldPt.z)) and (Pt1.z <= Max(Ln1[1].WorldPt.z, Ln1[2].WorldPt.z));
  end;
end; { IsPointOnLine }

function CrossProduct2D(p1, p2, p3: dcPoint): TFloat;
begin
  Result := (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
end; { CrossProduct2D }

function PointOnSegment(p, q, r: dcPoint): Boolean;
begin
  Result := (q.x <= Max(p.x, r.x)) and (q.x >= Min(p.x, r.x)) and (q.y <= Max(p.y, r.y)) and (q.y >= Min(p.y, r.y));
end; { PointOnSegment }

function Direction(A, B, C: dcPoint): TFloat;
begin
  Result := (C.x - A.x) * (B.y - A.y) - (B.x - A.x) * (C.y - A.y);
end; { Direction }

function MatString(const aMat: ModMat): string;
var
  i, j: Integer;
  Row: array [1 .. 4] of string;

begin
  for i := 1 to 4 do begin
    for j := 1 to 4 do begin
      Row[i] := Row[i] + '(' + '[' + IntToStr(i) + ',' + IntToStr(j) + ']: ' + FloatToStr(aMat[i, j]) + ')';
    end;
    LogDCAL(Row[i]);
  end;
end; { MatString }

function PointToLineDistance(Pt, LnStart, LnEnd: dcPoint; IgnoreZ: Boolean = False): TFloat;
var
  LineVector, PointVector, LineUnitVector, ClosestPoint: dcPoint;
  LineLength, ProjectionLength: TFloat;
begin
  if IgnoreZ then begin
    Pt.z := cZero;
    LnStart.z := cZero;
    LnEnd.z := cZero;
  end;

  LineVector := SubtractPoints(LnEnd, LnStart);
  PointVector := SubtractPoints(Pt, LnStart);
  LineLength := Sqrt(Sqr(LineVector.x) + Sqr(LineVector.y) + Sqr(LineVector.z));
  LineUnitVector := MultiplyPointByScalar(LineVector, 1 / LineLength);
  ProjectionLength := DotProduct(PointVector, LineUnitVector);

  if (ProjectionLength < cZero) then ClosestPoint := LnStart
  else if (ProjectionLength > LineLength) then ClosestPoint := LnEnd
  else ClosestPoint := AddPoints(LnStart, MultiplyPointByScalar(LineUnitVector, ProjectionLength));

  Result := Sqrt(Sqr(Pt.x - ClosestPoint.x) + Sqr(Pt.y - ClosestPoint.y) + Sqr(Pt.z - ClosestPoint.z));
end; { PointToLineDistance }

function ClosestEdge(Pt: dcPoint; Polygon: PntArr; nPnts: aSInt; out Neighbors: TPolyEdgeNeighbors; IgnoreZ: Boolean = False): TPolyEdge;
var
  MinDistance, Distance: TFloat;
  i, ClosestEdgeIndex: Integer;
  EdgeList: array of TPolyEdge; { array [1 .. 2] of Integer; }

begin
  ClosestEdgeIndex := -1;
  MinDistance := Infinity;

  SetLength(EdgeList, nPnts);
  for i := 1 to nPnts do begin
    EdgeList[i - 1][1] := i;
    EdgeList[i - 1][2] := (i mod nPnts) + 1;
  end;

  if IgnoreZ then Pt.z := cZero;

  for i := 1 to nPnts do begin
    if IgnoreZ then begin
      Polygon[EdgeList[i - 1][1]].z := cZero;
      Polygon[EdgeList[i - 1][2]].z := cZero;
    end;
    Distance := PointToLineDistance(Pt, Polygon[EdgeList[i - 1][1]], Polygon[EdgeList[i - 1][2]], IgnoreZ);
    if Distance < MinDistance then begin
      MinDistance := Distance;
      ClosestEdgeIndex := i - 1;
    end;
  end;

  { Update the Neighbors array with the neighboring edges of the closest edge }

  { Previous Edge }
  Neighbors[1][1] := EdgeList[(ClosestEdgeIndex - 1 + nPnts) mod nPnts][1];
  Neighbors[1][2] := EdgeList[(ClosestEdgeIndex - 1 + nPnts) mod nPnts][2];

  { Next Edge }
  Neighbors[2][1] := EdgeList[(ClosestEdgeIndex + 1) mod nPnts][1];
  Neighbors[2][2] := EdgeList[(ClosestEdgeIndex + 1) mod nPnts][2];

  Result := EdgeList[ClosestEdgeIndex];
end; { ClosestEdge }

procedure ClosestEndPointPair(var Edge1, Edge2: TLine3D; IgnoreZ: Boolean = False);
var
  Distances: array [0 .. 3] of TFloat;
  MinIndex, i: Integer;

  procedure Swap(var Point1, Point2: dcPoint3D);
  var
    Temp: dcPoint3D;
  begin
    Temp := Point1;
    Point1 := Point2;
    Point2 := Temp;
  end;

begin
  if IgnoreZ then begin
    Edge1[1].WorldPtZ := cZero;
    Edge1[2].WorldPtZ := cZero;
    Edge2[1].WorldPtZ := cZero;
    Edge2[2].WorldPtZ := cZero;
  end;

  Distances[0] := Distance3D(Edge1[1].WorldPt, Edge2[1].WorldPt);
  Distances[1] := Distance3D(Edge1[1].WorldPt, Edge2[2].WorldPt);
  Distances[2] := Distance3D(Edge1[2].WorldPt, Edge2[1].WorldPt);
  Distances[3] := Distance3D(Edge1[2].WorldPt, Edge2[2].WorldPt);

  MinIndex := 0;
  for i := 1 to 3 do
    if Distances[i] < Distances[MinIndex] then
         MinIndex := i;

  { Swap the points in Edge1 and Edge2 if needed }
  if MinIndex = 1 then
       Swap(Edge2[1], Edge2[2])
  else if MinIndex = 2 then
       Swap(Edge1[1], Edge1[2])
  else if MinIndex = 3 then
  begin
    Swap(Edge1[1], Edge1[2]);
    Swap(Edge2[1], Edge2[2]);
  end;
end; { ClosestEndpointPair }

(*
  function RotatePointAroundLine(const InPnt: dcPoint; Ln1: TLine3D; Angle: TFloat): dcPoint;
  var
  Axis, OriginToPnt, RotatedPnt: dcPoint;
  RadAngle, CosAngle, SinAngle: TFloat;
  begin
  // Normalize the axis of rotation
  Axis := SubtractPoints(Ln1[2], Ln1[1]);
  OriginToPnt := SubtractPoints(InPnt, Ln1[1]);

  RadAngle := DegToRad(Angle);
  CosAngle := Cos(RadAngle);
  SinAngle := Sin(RadAngle);

  // Rodrigues' rotation formula
  RotatedPnt := AddPoints(
  AddPoints(MultiplyPointByScalar(OriginToPnt, CosAngle),
  MultiplyPointByScalar(CrossProduct(Axis, OriginToPnt), SinAngle)),
  MultiplyPointByScalar(Axis, DotProduct(OriginToPnt, Axis) * (1 - CosAngle))
  );

  // Translate the rotated point back to its original position
  Result := AddPoints(RotatedPnt, Ln1[1]);
  end; { RotatePointAroundLine }
*)
procedure SetRotate(var rMat: ModMat; rAngle: TFloat; const Axis: dcPoint);
{ Set rotation around arbitrary axis ( normalized vector ) }
var
  Cosinus, Sinus: aFloat;
  ux, uy, uz: aFloat;
begin
  Cosinus := Cos(rAngle);
  Sinus := Sin(rAngle);

  ux := Axis.x;
  uy := Axis.y;
  uz := Axis.z;

  MatSet(rMat, cZero);
  rMat[1, 1] := Cosinus + ux * ux * (cOne - Cosinus);
  rMat[1, 2] := ux * uy * (cOne - Cosinus) - uz * Sinus;
  rMat[1, 3] := ux * uz * (cOne - Cosinus) + uy * Sinus;
  rMat[1, 4] := cZero;

  rMat[2, 1] := uy * ux * (cOne - Cosinus) + uz * Sinus;
  rMat[2, 2] := Cosinus + uy * uy * (cOne - Cosinus);
  rMat[2, 3] := uy * uz * (cOne - Cosinus) - ux * Sinus;
  rMat[2, 4] := cZero;

  rMat[3, 1] := uz * ux * (cOne - Cosinus) - uy * Sinus;
  rMat[3, 2] := uz * uy * (cOne - Cosinus) + ux * Sinus;
  rMat[3, 3] := Cosinus + uz * uz * (cOne - Cosinus);
  rMat[3, 4] := cZero;

  rMat[4, 1] := cZero;
  rMat[4, 2] := cZero;
  rMat[4, 3] := cZero;
  rMat[4, 4] := cOne;
end; { SetRotate }

function RotatePointAroundLine(const InPnt: dcPoint; Ln1: TLine3D; Angle: TFloat): dcPoint;
var
  Axis, OriginToPnt, RotatedPnt, TranslatedPnt: dcPoint;
  RMatrix: ModMat;

begin
  // Normalize the axis of rotation
  Axis := NormalizeVectorFunc(SubtractPoints(Ln1[2].WorldPt, Ln1[1].WorldPt));

  // Translate the input point and line so that the first point of the line is at the origin
  OriginToPnt := SubtractPoints(InPnt, Ln1[1].WorldPt);

  // Calculate the rotation matrix for the given angle and axis
  SetIdent(RMatrix);
  SetRotate(RMatrix, Pi / 2, Axis);

  // Rotate the translated point using the rotation matrix
  RotatedPnt := MultiplyMatPoint(RMatrix, OriginToPnt);

  // Translate the rotated point back to its original position
  TranslatedPnt := AddPoints(RotatedPnt, Ln1[1].WorldPt);

  Result := TranslatedPnt;
end;

function LineIntersectionToPlane(IntPnt: dcPoint; Ln1, Ln2: TLine3D): TTriangle3D;
{ This function assumes the line intersection has already been calculated }
var
  FarPnt1, FarPnt2: dcPoint;
  Poly: PntArr;
  i: Integer;
  RotAxis: TLine3D;

begin
  { Determine which line ends are opposite intersection point }
  if SamePoint(IntPnt, Ln1[1].WorldPt, Epsilon) then
       FarPnt1 := Ln1[2].WorldPt
  else FarPnt1 := Ln1[1].WorldPt;
  if SamePoint(IntPnt, Ln2[1].WorldPt, Epsilon) then
       FarPnt2 := Ln2[2].WorldPt
  else FarPnt2 := Ln2[1].WorldPt;

  { Create triangle (i.e., plane) from intersection }
  Result[1] := IntPnt;
  Result[2] := ProjectPoint3D(IntPnt, FarPnt1, 384.0);
  Result[3] := ProjectPoint3D(IntPnt, FarPnt2, 384.0);

  for i := 1 to 3 do
       Poly[i] := Result[i];

  AddPolygon(Poly, 3, 15);

  { Create Axis of Rotation (angle bisector of triangle) }
  RotAxis[1].WorldPt := IntPnt;
  RotAxis[2].WorldPt := MidPoint(Result[2], Result[3]);

  Poly[1] := IntPnt;
  Poly[2] := RotatePointAroundLine(Result[2], RotAxis, Pi / 2.0);
  Poly[3] := RotatePointAroundLine(Result[3], RotAxis, Pi / 2.0);

  AddPolygon(Poly, 3, 12);
end; { LineIntersectionToPlane }

function VirtualLineIntersection3D(Ln1A, Ln1B, Ln2A, Ln2B: dcPoint; var IntPoint: dcPoint3D; Flatten: Boolean = False; ErrMsg: Boolean = False;
   TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
var
  u, v, w, cross_uv: dcPoint;
  A, B, C, D, e, f, denom, S, T: TFloat;
  ClosePt1, ClosePt2: dcPoint;

begin
  Result := irNone;

  if Flatten then begin
    Ln1A.z := cZero;
    Ln1B.z := cZero;
    Ln2A.z := cZero;
    Ln2B.z := cZero;
  end;

  u := SubtractPoints(Ln1B, Ln1A);
  v := SubtractPoints(Ln2B, Ln2A);
  w := SubtractPoints(Ln1A, Ln2A);

  cross_uv := CrossProduct(u, v);

  A := DotProduct(u, u);
  B := DotProduct(u, v);
  C := DotProduct(v, v);
  D := DotProduct(u, w);
  e := DotProduct(v, w);
  f := DotProduct(w, w);

  denom := (A * C) - (B * B);

  { Check if the lines are parallel, coincident or skew }
  if (Abs(denom) < Epsilon) then begin
    if (cross_uv.x * cross_uv.x + cross_uv.y * cross_uv.y + cross_uv.z * cross_uv.z) < Epsilon then begin
      Result := irCoincident;
    end
    else begin
      Result := irParallel;
    end;
  end;

  if (Result <> irCoincident) and (Result <> irParallel) then begin
    S := ((B * e) - (C * D)) / denom;
    T := ((A * e) - (B * D)) / denom;

    ClosePt1 := AddPoints(Ln1A, MultiplyPointByScalar(u, S));
    ClosePt2 := AddPoints(Ln2A, MultiplyPointByScalar(v, T));

    { Check if the closest points are the same (i.e., lines intersect) }
    if (SameValue(ClosePt1.x, ClosePt2.x, Epsilon) and SameValue(ClosePt1.y, ClosePt2.y, Epsilon) and SameValue(ClosePt1.z, ClosePt2.z, Epsilon)) then begin
      IntPoint.WorldPt := ClosePt1;
      if not TooFarAway(IntPoint.WorldPt, cZeroPt, cFarAway) then
           Result := irIntersect
      else
           Result := irNone;
    end
    else begin
      Result := irSkew;
    end;
  end;

  if ErrMsg then begin
    LogDCAL('ErrMsg');
    case Result of
      irParallel: WrtErr('These lines are prallel');
      irCoincident: WrtErr('These lines are coincident');
      irSkew: WrtErr('These lines are skew and do not intersect');
    end;
  end;
end; { VirtualLineIntersection3D }

function VirtualLineIntersection3D(const Ln1, Ln2: TLine3D; var IntPoint: dcPoint3D; Flatten: Boolean = False; ErrMsg: Boolean = False; TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
begin
  Result := VirtualLineIntersection3D(Ln1[1].WorldPt, Ln1[2].WorldPt, Ln2[1].WorldPt, Ln2[2].WorldPt, IntPoint, Flatten, ErrMsg);
end; { VirtualLineIntersection3D }

function VirtualLineIntersection3D(Edge, PolyEdge: TPolyEdge3D; var IntPoint: dcPoint3D; Flatten: Boolean = False; ErrMsg: Boolean = False;
   TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
begin
  Result := VirtualLineIntersection3D(Edge.Pt1, Edge.Pt2, PolyEdge.Pt1, PolyEdge.Pt2, IntPoint, Flatten, ErrMsg);
end; { VirtualLineIntersection3D }

function VirtualLineIntersection3D(Ln1Pt1, Ln1Pt2, Ln2Pt1, Ln2Pt2: dcPoint; Flatten: Boolean = False; ErrMsg: Boolean = False;
   TooFar: TFloat = 3.84E+8)
   : TIntersectionResult; overload;
var
  IntPoint: dcPoint3D;
begin
  IntPoint := dcPoint3D.Create(dcPointNew);
  Result := VirtualLineIntersection3D(Ln1Pt1, Ln1Pt2, Ln2Pt1, Ln2Pt2, IntPoint, Flatten, ErrMsg);
end; { VirtualLineIntersection3D }

function LineIntersectionPoint3D(Ln1, Ln2: TLine3D; var NewPoint: dcPoint; Flatten: Boolean = False): TIntersectionResult;
var
  u, v, w, cross_uv: dcPoint;
  A, B, C, D, e, f, denom, S, T: TFloat;
  closestPoint1, closestPoint2: dcPoint;

begin
  if Flatten then begin
    Ln1[1].WorldPtZ := cZero;
    Ln2[1].WorldPtZ := cZero;
    Ln1[2].WorldPtZ := cZero;
    Ln2[2].WorldPtZ := cZero;
  end;

  u := SubtractPoints(Ln1[2].WorldPt, Ln1[1].WorldPt);
  v := SubtractPoints(Ln2[2].WorldPt, Ln2[1].WorldPt);
  w := SubtractPoints(Ln1[1].WorldPt, Ln2[1].WorldPt);

  cross_uv := CrossProduct(u, v);

  A := DotProduct(u, u);
  B := DotProduct(u, v);
  C := DotProduct(v, v);
  D := DotProduct(u, w);
  e := DotProduct(v, w);
  f := DotProduct(w, w);

  denom := (A * C) - (B * B);

  { Check if the lines are parallel, coincident or skew }
  if (Abs(denom) < Epsilon) then begin
    if (cross_uv.x * cross_uv.x + cross_uv.y * cross_uv.y + cross_uv.z * cross_uv.z) < Epsilon then begin
      Result := irCoincident;
    end
    else begin
      Result := irParallel;
    end;
    Exit;
  end;

  S := ((B * e) - (C * D)) / denom;
  T := ((A * e) - (B * D)) / denom;

  closestPoint1 := AddPoints(Ln1[1].WorldPt, MultiplyPointByScalar(u, S));
  closestPoint2 := AddPoints(Ln2[1].WorldPt, MultiplyPointByScalar(v, T));

  { Check if the intersection dcPoint is within the line segments }
  if (S >= cZero) and (S <= cOne) and (T >= cZero) and (T <= cOne) then begin
    { The closest points are the same (i.e., lines intersect) }
    NewPoint := closestPoint1;
    if not TooFarAway(NewPoint, cZeroPt, cFarAway) then
         Result := irIntersect
    else
         Result := irNone;
  end
  else begin
    Result := irSkew;
  end;
end; { LineIntersectionPoint3D }

function IntersectionLine2Poly(const Line: TLine3D; const Points: PntArr; nPnts: aSInt; out IPoint: dcPoint3D; TooFar: TFloat = 3.84E+8): Boolean; { overload; }
var
  u, v, n, dir, w0: dcPoint;
  A, B, r: TFloat;
  Triangle: TTriangle3D;

begin
  if (nPnts < 3) then begin
    Result := False;
    Exit
  end;

  Result := True;

  Triangle := NewTriangle(Points[1], Points[2], Points[3]);

  u := SubtractPoints(Triangle[2], Triangle[1]);
  v := SubtractPoints(Triangle[3], Triangle[1]);
  n := CrossProduct(u, v);

  dir := SubtractPoints(Line[2].WorldPt, Line[1].WorldPt);
  w0 := SubtractPoints(Line[1].WorldPt, Triangle[2]);

  A := DotProduct(n, w0) * -cOne;
  B := DotProduct(n, dir);

  { Check if the line and the plane are parallel or near parallel }
  if (Abs(B) < 0.01 {0.001} {cTiny} {Epsilon}) then begin
    Result := False;
    Exit;
  end;

  r := A / B;

  iPoint.WorldPt := AddPoints(Line[1].WorldPt, MultiplyPointByScalar(dir, r));

  // iPoint.WorldPtX := Line[1].WorldPt.x + (r * dir.x);
  // iPoint.WorldPtY := Line[1].WorldPt.y + (r * dir.y);
  // iPoint.WorldPtZ := Line[1].WorldPt.z + (r * dir.z);

  Result := not TooFarAway(IPoint.WorldPt, cZeroPt, cFarAway);
end; { IntersectionLine2Poly }

function IntersectionLine2Plane(const Line: TLine3D; const PlanePoint, PlaneNormal: dcPoint; out IPoint: dcPoint): Boolean;
var
  A, B, r: TFloat;
  dir, w0: dcPoint;
begin
  dir := SubtractPoints(Line[2].WorldPt, Line[1].WorldPt);
  w0 := SubtractPoints(Line[1].WorldPt, PlanePoint);

  A := DotProduct(PlaneNormal, w0) * -cOne;
  B := DotProduct(PlaneNormal, dir);

  { Check if the line and the plane are parallel or near parallel }
  if (Abs(B) < Epsilon) then begin
    Result := False;
    Exit;
  end;

  r := A / B;

  IPoint := AddPoints(Line[1].WorldPt, MultiplyPointByScalar(dir, r));

  // IPoint.x := Line[1].WorldPt.x + (r * dir.x);
  // IPoint.y := Line[1].WorldPt.y + (r * dir.y);
  // IPoint.z := Line[1].WorldPt.z + (r * dir.z);

  Result := True;
end; { IntersectionLine2Plane }

function PlaneIntersection(n2, A2, n1, A1: dcPoint; out IntersectionLine: TLine3D): Boolean;
var
  Direction, w, Normal1Projected, Normal2Projected, v1, v2, v3: dcPoint;
  d1, d2, Length: TFloat;
begin
  Result := False;

  Direction := CrossProduct(n1, n2);

  if SameValue(Direction.x, cZero) and SameValue(Direction.y, cZero) and SameValue(Direction.z, cZero) then Exit;

  Normal1Projected.x := n1.x;
  Normal1Projected.y := n1.y;
  Normal1Projected.z := cZero;

  Normal2Projected.x := n2.x;
  Normal2Projected.y := n2.y;
  Normal2Projected.z := cZero;

  { Calculate the intersection dcPoint of the two planes }
  d1 := DotProduct(Normal1Projected, A1);
  d2 := DotProduct(Normal2Projected, A2);

  IntersectionLine[1].WorldPtX := (d2 * n1.z - d1 * n2.z) / Direction.z;
  IntersectionLine[1].WorldPtY := (d1 * n2.x - d2 * n1.x) / Direction.z;
  IntersectionLine[1].WorldPtZ := (n1.x * n2.y - n1.y * n2.x) / Direction.z;

  { Check if the intersection dcPoint lies on both planes }
  v1 := SubtractPoints(IntersectionLine[1].WorldPt, A1);
  v2 := SubtractPoints(IntersectionLine[1].WorldPt, A2);
  v3 := CrossProduct(n1, v1);

  if DotProduct(n2, v1) < cZero then Exit;
  if DotProduct(n2, v2) > cZero then Exit;
  if DotProduct(n2, v3) < cZero then Exit;

  { Calculate the direction vector of the intersection line }
  Length := Hypot(Direction.x, Direction.y, Direction.z);
  IntersectionLine[2].WorldPt := DivideVectorByScalar(Direction, Length);

  Result := True;
end; { PlaneIntersection }

(*
  function GetIntersectionPlane(const Ln1, Ln2: TLine3D): TTriangle3D;
  { Plane formed by triangle of lines and their intersection point }
  var
  Dir1, Dir2, Normal: dcPoint;
  PointOnPlane: dcPoint;
  D: TFloat;

  begin
  Dir1 := SubtractPoints(Ln1[2], Ln1[1]);
  Dir2 := SubtractPoints(Ln2[2], Ln2[1]);
  Normal := CrossProduct(Dir1, Dir2);
  NormalizeVector(Normal);
  PointOnPlane := Ln1[1];
  D := DotProduct(SubtractPoints(PointOnPlane, Ln2[1]), Normal);
  Result[1] := SubtractPoints(PointOnPlane, MultiplyPointByScalar(Normal, D));
  Result[2] := AddPoints(PointOnPlane, Dir1);
  Result[3] := AddPoints(PointOnPlane, Dir2);
  end; { GetIntersectionPlane }
*)
(*
  function GetIntersectionPlane(const Ln1, Ln2: TLine3D): TTriangle3D;
  var
  Dir1, Dir2, Normal: dcPoint;
  PointOnPlane: dcPoint;
  D: TFloat;
  begin
  Dir1 := SubtractPoints(Ln1[2], Ln1[1]);
  Dir2 := SubtractPoints(Ln2[2], Ln2[1]);
  Normal := CrossProduct(Dir1, Dir2);
  NormalizeVector(Normal);
  PointOnPlane := Ln1[1];
  D := DotProduct(SubtractPoints(PointOnPlane, Ln2[1]), Normal);
  Result[1] := AddPoints(Ln1[1], MultiplyPointByScalar(Dir1, D / DotProduct(Dir1, Normal)));
  D := DotProduct(SubtractPoints(PointOnPlane, Ln1[1]), Normal);
  Result[2] := AddPoints(Ln2[1], MultiplyPointByScalar(Dir2, D / DotProduct(Dir2, Normal)));
  Result[3] := AddPoints(Result[1], Result[2]);
  end; { GetIntersectionPlane }
*)
function GetIntersectionPlane(const Ln1, Ln2: TLine3D): TPlane3D;
var
  p1, p2: dcPoint;
  d1, d2: dcPoint;
  n: dcPoint;
  plane: TPlane3D;

begin
  // Get the two points on the lines.
  p1 := Ln1[1].WorldPt;
  p2 := Ln2[1].WorldPt;

  // Get the direction vectors of the lines.
  d1 := SubtractPoints(Ln1[2].WorldPt, p1);
  d2 := SubtractPoints(Ln2[2].WorldPt, p2);

  // Calculate the normal vector of the intersection plane.
  n := CrossProduct(d1, d2);

  // Check if the lines are parallel (cross product is zero vector)
  if (n.x = 0) and (n.y = cZero) and (n.z = cZero) then
       raise Exception.Create('The lines are parallel and do not define a unique plane.');

  // Normalize the normal vector.
  NormalizeVector(n);

  // Store the point and normal vector in the plane record.
  plane.Point := p1;
  plane.Normal := n;

  Result := plane;
end; { GetIntersectionPlane }

function GetEquationOfPlane(const Ln1, Ln2: TLine3D): string;
var
  p1, p2, p3: dcPoint;
  d1, d2: dcPoint;
  n: dcPoint;
  IsParallel: Boolean;
  D: TFloat;

  function AreParallel(d1, d2: dcPoint): Boolean;
  begin
    Result := (d1.x * d2.y - d1.y * d2.x) = cZero;
  end;

begin
  // Get the two points on the lines.
  p1 := Ln1[1].WorldPt;
  p2 := Ln2[1].WorldPt;

  // Get the direction vectors of the lines.
  d1 := SubtractPoints(Ln1[2].WorldPt, p1);
  d2 := SubtractPoints(Ln2[2].WorldPt, p2);

  // Check if the two lines are parallel.
  IsParallel := AreParallel(d1, d2);

  // If the two lines are parallel, then there is no equation for the plane.
  if IsParallel then
  begin
    Result := '';
    Exit;
  end;

  // Calculate the normal vector of the plane.
  n := CrossProduct(d1, d2);

  // Normalize the normal vector.
  NormalizeVector(n);

  // Calculate the distance from the origin to the plane.
  p3 := AddPoints(p1, n);
  D := DotProduct(n, p3);

  // Calculate the equation of the plane.
  Result := Format('%.2f*x + %.2f*y + %.2f*z + %.2f = 0', [n.x, n.y, n.z, -D]);
  LogDCAL(Result);
end; { GetEquationOfPlane }

function OffsetLine3D(const Line: TLine3D; Distance: TFloat; Direction: TDirection): TLine3D;
var
  OffsetVector: dcPoint;
begin
  case Direction of
    dirX: OffsetVector := dcPointNew(Distance, cZero, cZero);
    dirY: OffsetVector := dcPointNew(cZero, Distance, cZero);
    dirZ: OffsetVector := dcPointNew(cZero, cZero, Distance);
  end;

  Result[1].WorldPt := AddPoints(Line[1].WorldPt, OffsetVector);

  // Result[1].WorldPt.x := Line[1].WorldPt.x + OffsetVector.x;
  // Result[1].WorldPt.y := Line[1].WorldPt.y + OffsetVector.y;
  // Result[1].WorldPt.z := Line[1].WorldPt.z + OffsetVector.z;

  Result[2].WorldPt := AddPoints(Line[2].WorldPt, OffsetVector);

  // Result[2].WorldPt.x := Line[2].WorldPt.x + OffsetVector.x;
  // Result[2].WorldPt.y := Line[2].WorldPt.y + OffsetVector.y;
  // Result[2].WorldPt.z := Line[2].WorldPt.z + OffsetVector.z;
end; { OffsetLine3D }

function PerpendicularOffsetLine3D(const Line: TLine3D; Distance: TFloat; { const } PerpendicularDirection: dcPoint): TLine3D;
var
  NormalVector: dcPoint;
begin
  { Normalize the PerpendicularDirection vector }
  NormalizeVector(PerpendicularDirection);

  { Multiply the PerpendicularDirection by the offset distance }
  NormalVector := MultiplyPointByScalar(PerpendicularDirection, Distance);

  // NormalVector.x := PerpendicularDirection.x * Distance;
  // NormalVector.y := PerpendicularDirection.y * Distance;
  // NormalVector.z := PerpendicularDirection.z * Distance;

  { Apply the perpendicular offset }
  Result[1].WorldPt := AddPoints(Line[1].WorldPt, NormalVector);

  // Result[1].WorldPt.x := Line[1].WorldPt.x + NormalVector.x;
  // Result[1].WorldPt.y := Line[1].WorldPt.y + NormalVector.y;
  // Result[1].WorldPt.z := Line[1].WorldPt.z + NormalVector.z;

  Result[2].WorldPt := AddPoints(Line[2].WorldPt, NormalVector);

  // Result[2].WorldPt.x := Line[2].WorldPt.x + NormalVector.x;
  // Result[2].WorldPt.y := Line[2].WorldPt.y + NormalVector.y;
  // Result[2].WorldPt.z := Line[2].WorldPt.z + NormalVector.z;
end; { PerpendicularOffsetLine3D }

function PerpendicularVector(const Vec: dcPoint): dcPoint;
var
  AbsVec: dcPoint;
begin
  AbsVec := dcPointNew(Abs(Vec.x), Abs(Vec.y), Abs(Vec.z));

  if (AbsVec.x <= AbsVec.y) and (AbsVec.x <= AbsVec.z) then
       Result := CrossProduct(Vec, dcPointNew(cOne, cZero, cZero))
  else if AbsVec.y <= AbsVec.z then
       Result := CrossProduct(Vec, dcPointNew(cZero, cOne, cZero))
  else
       Result := CrossProduct(Vec, dcPointNew(cZero, cZero, cOne));

  NormalizeVector(Result);
end; { PerpendicularVector }

function IntersectionOffsetLines3D(const Line1, Line2: TLine3D; Distance: TFloat): TArray<TLine3D>;
var
  OffsetLine1, OffsetLine2: TArray<TLine3D>;
  IntersectionPoints: TArray<dcPoint3D>;
  LineA, LineB: TLine3D;
  i: Integer;
  PerpendicularDirection1, PerpendicularDirection2: dcPoint;
begin
  // Calculate perpendicular directions
  PerpendicularDirection1 := PerpendicularVector(SubtractPoints(Line1[2].WorldPt, Line1[1].WorldPt));
  PerpendicularDirection2 := PerpendicularVector(SubtractPoints(Line2[2].WorldPt, Line2[1].WorldPt));

  // Create two offset lines for each original line
  OffsetLine1 := TArray<TLine3D>.Create(
     PerpendicularOffsetLine3D(Line1, Distance, PerpendicularDirection1),
     PerpendicularOffsetLine3D(Line1, -Distance, PerpendicularDirection1)
     );
  OffsetLine2 := TArray<TLine3D>.Create(
     PerpendicularOffsetLine3D(Line2, Distance, PerpendicularDirection2),
     PerpendicularOffsetLine3D(Line2, -Distance, PerpendicularDirection2)
     );

  // Find the intersection points of the offset lines
  SetLength(IntersectionPoints, 4);
  i := 0;
  for LineA in OffsetLine1 do
    for LineB in OffsetLine2 do
    begin
      if VirtualLineIntersection3D(LineA, LineB, IntersectionPoints[i]) = irIntersect then
           Inc(i);
    end;

  if i < 3 then
  begin
    // Not enough intersection points found
    SetLength(Result, 0);
    Exit;
  end;

  // Connect the intersection points to form a triangle or a plane of intersection
  SetLength(Result, 3);
  for i := 0 to 2 do begin
    Result[i][1].WorldPt := IntersectionPoints[i].WorldPt;
    Result[i][2].WorldPt := IntersectionPoints[i + 1].WorldPt;
  end;
end; { IntersectionOffsetLine3D }

function FindHorizontalLine(Points: PntArr; nPnts: aSInt; StartOption: TStartOption; out Angle: TFloat; out StartPoint, EndPoint: dcPoint): Boolean;
var
  v1, v2, Normal, NormalProjected, Direction, Centroid: dcPoint;
  TargetZ, Dist, HighZ, LowZ: TFloat;
  i, StartIdx: Integer;
begin
  Result := False;
  if (nPnts < 3) then Exit;

  v1 := SubtractPoints(Points[2], Points[1]);
  v2 := SubtractPoints(Points[3], Points[1]);
  Normal := CrossProduct(v1, v2);

  NormalProjected.x := Normal.x;
  NormalProjected.y := Normal.y;
  NormalProjected.z := cZero;

  Angle := RadToDeg(ArcCos(DotProduct(Normal, NormalProjected) / (Hypot(Normal.x, Normal.y, Normal.z) * Hypot(NormalProjected.x, NormalProjected.y,
     NormalProjected.z))));

  HighZ := Points[1].z;
  LowZ := Points[1].z;
  StartIdx := 1;

  for i := 1 to nPnts do
  begin
    if Points[i].z > HighZ then
    begin
      HighZ := Points[i].z;
      StartIdx := i;
    end;

    if Points[i].z < LowZ then
    begin
      LowZ := Points[i].z;
    end;
  end;

  case StartOption of
    soHighest: TargetZ := HighZ;
    soLowest: TargetZ := LowZ;
  end;

  { Compute the centroid of the polygon }
  Centroid := dcPointNew();
  for i := 1 to nPnts do begin
    Centroid := AddPoints(Centroid, Points[i]);
  end;
  Centroid.x := Centroid.x / nPnts;
  Centroid.y := Centroid.y / nPnts;
  Centroid.z := Centroid.z / nPnts;

  Direction.x := Centroid.x - Points[StartIdx].x;
  Direction.y := Centroid.y - Points[StartIdx].y;
  Direction.z := cZero;

  Dist := Hypot(Direction.x, Direction.y);

  Direction.x := Direction.x / Dist;
  Direction.y := Direction.y / Dist;

  StartPoint := Points[StartIdx];
  EndPoint.x := StartPoint.x + Direction.x * Dist;
  EndPoint.y := StartPoint.y + Direction.y * Dist;
  EndPoint.z := TargetZ;

  Result := True;
end; { FindHorizontalLine }

function FindIntersectionLine(Points: PntArr; nPnts: aSInt; out IntersectionLine: TLine3D): Boolean;
var
  HighPoint, LowPoint: dcPoint;
  v1, v2, n1, n2: dcPoint;
  Angle: TFloat;
  p1, p2, p3: dcPoint;
  DummyEnd: dcPoint;

begin
  Result := False;

  if (nPnts < 3) then Exit;

  p1 := Points[1];
  p2 := Points[2];
  p3 := Points[3];

  if SameValue(p1.z, p2.z) and SameValue(p2.z, p3.z) then Exit;

  FindHorizontalLine(Points, nPnts, soHighest, Angle, HighPoint, DummyEnd);
  FindHorizontalLine(Points, nPnts, soLowest, Angle, LowPoint, DummyEnd);

  v1 := SubtractPoints(p2, p1);
  v2 := SubtractPoints(p3, p1);
  n1 := CrossProduct(v1, v2);

  n2.x := n1.x;
  n2.y := n1.y;
  n2.z := -n1.z;

  Result := PlaneIntersection(n1, HighPoint, n2, LowPoint, IntersectionLine);
end; { FindIntersectionLine }

function FindIntersectionLineBetweenPlanes(Points1, Points2: PntArr; nPnts1, nPnts2: aSInt; out IntersectionLine: TLine3D): Boolean;
var
  HighPoint1, LowPoint1, HighPoint2, LowPoint2: dcPoint;
  v1, v2, n1, n2: dcPoint;
  p1, p2, p3, q1, q2, q3: dcPoint;
  Angle: TFloat;
  DummyEnd: dcPoint;

begin
  Result := False;

  if (nPnts1 < 3) or (nPnts2 < 3) then Exit;

  p1 := Points1[1];
  p2 := Points1[2];
  p3 := Points1[3];

  q1 := Points2[1];
  q2 := Points2[2];
  q3 := Points2[3];

  FindHorizontalLine(Points1, nPnts1, soHighest, Angle, HighPoint1, DummyEnd);
  FindHorizontalLine(Points1, nPnts1, soLowest, Angle, LowPoint1, DummyEnd);

  FindHorizontalLine(Points2, nPnts2, soHighest, Angle, HighPoint2, DummyEnd);
  FindHorizontalLine(Points2, nPnts2, soLowest, Angle, LowPoint2, DummyEnd);

  v1 := SubtractPoints(p2, p1);
  v2 := SubtractPoints(p3, p1);
  n1 := CrossProduct(v1, v2);

  v1 := SubtractPoints(q2, q1);
  v2 := SubtractPoints(p3, q1);
  n2 := CrossProduct(v1, v2);

  Result := PlaneIntersection(n1, p1, n2, q1, IntersectionLine);
end; { FindIntersectionLineBetweenPlanes }

function IntersectionOfTwoPlanesLoHi(Poly1, Poly2: PntArr; nPnts1, nPnts2: aSInt;
   Adjacent1, Adjacent2: TPolyEdgeNeighbors; out IntPntAdj1, IntPntAdj2: TLine3D; out IntPoints: TArray<dcPoint>;
   out IntLine: TLine3D): Boolean; { overload; }
type
  TIntPoints = TArray<dcPoint>;

var
  PtsFound: Integer;

  procedure FindIntersectionPointsAndAddMarkers(var PtsFound: Integer; var IntPoints: TArray<dcPoint>);
  var
    i, j: Integer;
    IPoint: dcPoint3D;
    AdjLn1, AdjLn2: TLine3D;
    Dist: TFloat;

  begin
    { Calculate IntPntAdj1 and IntPntAdj2 }
    for i := 1 to 2 do begin
      AdjLn1[1].WorldPt := Poly1[Adjacent1[i][1]];
      AdjLn1[2].WorldPt := Poly1[Adjacent1[i][2]];
      if IntersectionLine2Poly(AdjLn1, Poly2, nPnts2, IPoint) then begin
        IntPntAdj1[i].WorldPt := IPoint.WorldPt;
        // LogDCAL('IntPntAdj1[' + IntToStr(i) + '] - x: ' + FloatToStr(IPoint.x) +
        // ' y: ' + FloatToStr(IPoint.y) +
        // ' z: ' + FloatToStr(IPoint.z))
      end;

      AdjLn2[1].WorldPt := Poly2[Adjacent2[i][1]];
      AdjLn2[2].WorldPt := Poly2[Adjacent2[i][2]];
      if IntersectionLine2Poly(AdjLn2, Poly1, nPnts1, IPoint) then begin
        IntPntAdj2[i].WorldPt := IPoint.WorldPt;
        // LogDCAL('IntPntAdj2[' + IntToStr(i) + '] - x: ' + FloatToStr(IPoint.x) +
        // ' y: ' + FloatToStr(IPoint.y) +
        // ' z: ' + FloatToStr(IPoint.z))
      end;
    end;

    { Check if IntPntAdj1 and IntPntAdj2 are close enough and set them to the same dcPoint if they are }
    for i := 1 to 2 do begin
      Dist := Distance3D(IntPntAdj1[i].WorldPt, IntPntAdj2[i].WorldPt);
      if (Dist < Epsilon) then begin
        IntPntAdj1[i].WorldPt := MidPoint(IntPntAdj1[i].WorldPt, IntPntAdj2[i].WorldPt);
        IntPntAdj2[i] := IntPntAdj1[i];
      end;
    end;

    { Find intersection points for all edges }
    for i := 1 to nPnts1 do begin
      // LogDCAL('i: ' + IntToStr(i));
      AdjLn1[1].WorldPt := Poly1[i];
      AdjLn1[2].WorldPt := Poly1[(i mod nPnts1) + 1];

      for j := 1 to nPnts2 do begin
        // LogDCAL('j: ' + IntToStr(j));
        AdjLn2[1].WorldPt := Poly2[j];
        AdjLn2[2].WorldPt := Poly2[(j mod nPnts2) + 1];

        { Check for intersection points with edges of Poly2 and Poly1 }
        if IntersectionLine2Poly(AdjLn2, Poly1, nPnts1, IPoint) then begin
          SetLength(IntPoints, PtsFound + 1);
          IntPoints[PtsFound] := IPoint.WorldPt;
          Inc(PtsFound);
          // LogDCAL('PtsFound: ' + IntToStr(PtsFound));
        end;
      end;
    end;
  end;

  procedure FindHiAndLoPoints(Pts: TIntPoints; out HiPt, LoPt: dcPoint3D);
  var
    i: Integer;
    MaxZ, MinZ: TFloat;
  begin
    MaxZ := Pts[1].z;
    MinZ := Pts[1].z;
    HiPt.WorldPt := Pts[1];
    LoPt.WorldPt := Pts[1];
    for i := 2 to 4 do begin
      if (Pts[i].z > MaxZ) then begin
        MaxZ := Pts[i].z;
        HiPt.WorldPt := Pts[i];
      end;
      if (Pts[i].z < MinZ) then begin
        MinZ := Pts[i].z;
        LoPt.WorldPt := Pts[i];
      end;
    end;
  end;

begin
  Result := False;
  PtsFound := 0;
  if (nPnts1 < 3) or (nPnts2 < 3) then Exit;

  FindIntersectionPointsAndAddMarkers(PtsFound, IntPoints);

  if (PtsFound < 2) then Exit;

  FindHiAndLoPoints(IntPoints, IntLine[1], IntLine[2]);

  Result := True;
end; { IntersectionOfTwoPlanesLoHi }

function Centroid3D(const Polygon: PntArr; nPnts: aSInt): dcPoint;
var
  Sum: dcPoint;
  i: Integer;

begin
  Sum := dcPointNew();
  for i := 1 to nPnts do
       Sum := AddPoints(Sum, Polygon[i]);

  Result := MultiplyPointByScalar(Sum, (cOne / nPnts));
end; { Centroid3D }

function CentroidOfTriangle(TT: TTriangle3D): dcPoint;
begin
  Result.x := (TT[1].x + TT[2].x + TT[3].x) / 3;
  Result.y := (TT[1].y + TT[2].y + TT[3].y) / 3;
  Result.z := (TT[1].z + TT[2].z + TT[3].z) / 3;
end; { CentroidOfTriangle }

function PolygonHorizontalAngle(const Polygon: PntArr; nPnts: aSInt; Perpendicular: Boolean = False): TFloat; { overload; }
var
  i, nextIdx: Integer;
  Normal, Edge1, Edge2: dcPoint;

begin
  { Compute the normal vector of the polygon }
  Normal := dcPointNew();
  for i := 1 to nPnts do begin
    nextIdx := (i mod nPnts) + 1;
    Edge1 := Polygon[i];
    Edge2 := Polygon[nextIdx];
    Normal := AddPoints(Normal, CrossProduct(Edge1, Edge2));
  end;

  { Project the normal vector onto the XY plane }
  Normal.z := cZero;

  { Calculate the angle between the projected normal and the X-axis }
  Result := ArcTan2(Normal.y, Normal.x);
  if not Perpendicular then
    { Add Pi/2 radians (90 degrees) to get the angle of the polygon's plane }
       Result := Result + Pi / 2.0;
end; { PolygonHorizontalAngle }

function DirectionDistanceLineHorizontal(Direction: TFloat; Distance: TFloat; Pt: dcPoint): TLine3D;
var
  dx, dy: TFloat;
begin
  dx := Distance * Cos(Direction);
  dy := Distance * Sin(Direction);

  Result[1].WorldPt := Pt;
  Result[2].WorldPtX := Pt.x + dx;
  Result[2].WorldPtY := Pt.y + dy;
  Result[2].WorldPtZ := Pt.z;
end; { DirectionDistanceLineHorizontal }

function ArePolygonEdgesCrossing(const Polygon: PntArr; nPnts: Integer): Boolean;
var
  i, j: Integer;
  Line1, Line2: TLine3D;
  IntersectionPoint: dcPoint;

begin
  Result := False;
  for i := 1 to nPnts do begin
    Line1[1].WorldPt := Polygon[i];
    Line1[2].WorldPt := Polygon[((i - 1) + 1) mod nPnts + 1];

    for j := i + 1 to nPnts do begin
      Line2[1].WorldPt := Polygon[j];
      Line2[2].WorldPt := Polygon[((j - 1) + 1) mod nPnts + 1];

      { Skip consecutive edges as they share a dcPoint and will always intersect }
      if (((i - 1) + 1) mod nPnts + 1) = j then
           Continue;

      { Skip edges that wrap around the last vertex to the first vertex }
      if (i = 1) and (j = nPnts) then
           Continue;

      if LineIntersectionPoint3D(Line1, Line2, IntersectionPoint) = irIntersect then begin
        Result := True;
        Exit;
      end;
    end;
  end;
end; { ArePolygonEdgesCrossing }

procedure ReversePolygonPoints(var Polygon: Entity);
var
  i: Integer;
  TempPoint: dcPoint;
begin
  for i := 1 to Polygon.plynpnt div 2 do begin
    TempPoint := Polygon.plypnt[i];
    Polygon.plypnt[i] := Polygon.plypnt[Polygon.plynpnt - i + 1];
    Polygon.plypnt[Polygon.plynpnt - i + 1] := TempPoint;
  end;
end; { ReversePolygonPoints }

procedure LineEndToKeep(Ln1: TLine3D; Pick1: dcPickPt; Intersection: dcPoint3D; out LE1, Opp1: dcPoint; Opposite: Boolean = False; IsOrtho: Boolean = True);
var
  Dv1: dcPoint;
  DP_Pick1: TFloat;
  //Ln1Pt1, Ln1Pt2, IntPt, Pck1: dcPoint;
  //Pk1: TPoint;
  //Params: TDrawParams;

  function DrawPtToDcPoint(Pt: TPoint): dcPoint;
  begin
    Result.x := Pt.x;
    Result.y := Pt.y;
    Result.z := cZero;
  end;

  function dcPointToDrawPoint(Pt: dcPoint): TPoint;
  begin
    Result.x := Round(Pt.x);
    Result.y := Round(Pt.y);
  end;

begin
  Pick1.WorldPtZ := cZero;

  if IsOrtho then begin
    { Use World Points }

    { Pick (i.e., GetPoint) is already in world x, y coordinates parallel to the Draw Form.
      So, no transformation is necessary in Ortho view. }

    { Direction Vector }
    Dv1 := SubtractPoints(Ln1[1].WorldPt, Ln1[2].WorldPt);
    DP_Pick1 := DotProduct(Dv1, SubtractPoints(Pick1.WorldPt, Intersection.WorldPt));
  end
  else begin
    { Use Draw Points - Projected World Points }
    //Ln1Pt1 := DrawPtToDcPoint(Ln1[1].DrawPt);
    //Ln1Pt2 := DrawPtToDcPoint(Ln1[2].DrawPt);
    //IntPt := DrawPtToDcPoint(Intersection.DrawPt);

    { Pick (i.e., GetPoint) is always in world x, y coordinates parallel to the Draw Form.
      So, a 3D transformation is not necessary, just do the 2D transformation. }
    //CalculateDrawCoordinate(Pick1.WorldPt, Pk1);
    //Pck1 := DrawPtToDcPoint(Pk1);

    { Direction Vector }
    //Dv1 := SubtractPoints(Ln1Pt1, Ln1Pt2);
    //DP_Pick1 := DotProduct(Dv1, SubtractPoints(Pck1, IntPt));

    Dv1 := SubtractPoints(Ln1[1].DrawPt, Ln1[2].DrawPt);
    DP_Pick1 := DotProduct(Dv1, SubtractPoints(Pick1.DrawPt, Intersection.DrawPt));
  end;

  // Params.Pt1 := dcPointToDrawPoint(Ln1Pt1);
  // Params.Pt2 := dcPointToDrawPoint(Ln1Pt2);
  // DrawOnCanvas(DrawOnCanvasProc, Params);

  // DrawTextAtDrawPt(Ln1[1].DrawPt, 'Pt', 9);

  // Params.Pt1 := dcPointToDrawPoint(Pck1);
  // Params.Pt2 := dcPointToDrawPoint(Pck1);
  // DrawOnCanvas(DrawOnCanvasProc, Params);

  // DrawTextAtDrawPt(Pk1, 'Pick', 9);

  { Direction Vector }
  // Dv1 := SubtractPoints(Ln1Pt1, Ln1Pt2);
  // DP_Pick1 := DotProduct(Dv1, SubtractPoints(Pck1, IntPt));

  if not Opposite then begin
    if (DP_Pick1 > cTiny) then begin
      LE1 := Ln1[1].WorldPt;
      Opp1 := Ln1[2].WorldPt;
    end
    else begin
      LE1 := Ln1[2].WorldPt;
      Opp1 := Ln1[1].WorldPt;
    end;
  end
  else begin
    if (DP_Pick1 > cTiny) then begin
      LE1 := Ln1[2].WorldPt;
      Opp1 := Ln1[1].WorldPt;
    end
    else begin
      LE1 := Ln1[1].WorldPt;
      Opp1 := Ln1[2].WorldPt;
    end;
  end;
end; { LineEndToKeep }

procedure LineEndsToKeep(Ln1, Ln2: TLine3D; Pick1, Pick2: dcPickPt; Intersection: dcPoint3D; out LE1, Opp1, LE2, Opp2: dcPoint; Opposite: Boolean = False;
   IsOrtho: Boolean = True);
{ Call LineEndToKeep twice }
begin
  LineEndToKeep(Ln1, Pick1, Intersection, LE1, Opp1, Opposite, IsOrtho);
  LineEndToKeep(Ln2, Pick2, Intersection, LE2, Opp2, Opposite, IsOrtho);
end; { LineEndsToKeep }

procedure ClosestEndPoint(Ln: TLine3D; TestPt: dcPoint; out ClosePt, FarPt: dcPoint; IgnoreZ: Boolean = False);
var
  Distance1, Distance2: TFloat;
begin
  if IgnoreZ then begin
    Ln[1].WorldPtZ := cZero;
    Ln[2].WorldPtZ := cZero;
    TestPt.z := cZero;
  end;

  Distance1 := Distance3D(Ln[1].WorldPt, TestPt);
  Distance2 := Distance3D(Ln[2].WorldPt, TestPt);

  if (Distance1 < Distance2) then begin
    ClosePt := Ln[1].WorldPt;
    FarPt := Ln[2].WorldPt;
  end
  else begin
    ClosePt := Ln[2].WorldPt;
    FarPt := Ln[1].WorldPt;
  end;
end; { ClosestEndPoint }

function ClosestPointOnLine(Ln: TLine3D; TestPt: dcPoint; IgnoreZ: Boolean = False): dcPoint;
var
  EdgeVector, PickVector: dcPoint;
  T: TFloat;

begin
  if IgnoreZ then begin
    Ln[1].WorldPtZ := cZero;
    Ln[2].WorldPtZ := cZero;
    TestPt.z := cZero;
  end;

  EdgeVector := SubtractPoints(Ln[2].WorldPt, Ln[1].WorldPt); { Line direction vector }
  PickVector := SubtractPoints(TestPt, Ln[1].WorldPt); { Vector from Ln[1] to TestPt }

  T := DotProduct(PickVector, EdgeVector) / DotProduct(EdgeVector, EdgeVector); { Project PickVector onto EdgeVector }

  if (T < cZero) then T := cZero; { Ensure T is within the line segment }
  if (T > cOne) then T := cOne; { Ensure T is within the line segment }

  Result := AddPoints(Ln[1].WorldPt, MultiplyPointByScalar(EdgeVector, T)); { Closest point on the line }
end; { ClosestPointOnEdge }

function GetIncludedAngle(const Ln1, Ln2: TLine3D): Double;
var
  d1, d2: dcPoint;
  dotProd, d1Magnitude, d2Magnitude, cosTheta: Double;

begin
  // Get the direction vectors of the lines.
  d1 := SubtractPoints(Ln1[2].WorldPt, Ln1[1].WorldPt);
  d2 := SubtractPoints(Ln2[2].WorldPt, Ln2[1].WorldPt);

  // Calculate the dot product of the direction vectors.
  dotProd := DotProduct(d1, d2);

  // Calculate the magnitudes of the direction vectors.
  d1Magnitude := Magnitude(d1);
  d2Magnitude := Magnitude(d2);

  // Check if either of the magnitudes is zero (invalid input).
  if (d1Magnitude = cZero) or (d2Magnitude = cZero) then
       raise Exception.Create('One or both of the input lines have zero length.');

  // Calculate the cosine of the angle between the direction vectors.
  cosTheta := dotProd / (d1Magnitude * d2Magnitude);

  // Calculate the angle in radians and convert it to degrees.
  Result := ArcCos(cosTheta) * (180 / Pi);
end; { GetIncludedAngle }

function FindIntersectionOrClosestPoint(const Ln1, Ln2: TLine3D): dcPoint;
var
  u, v, w0: dcPoint;
  A, B, C, D, e, f, sc, tc: Double;
  pointOnLine1, pointOnLine2: dcPoint;

begin
  // Get the direction vectors of the lines.
  u := SubtractPoints(Ln1[2].WorldPt, Ln1[1].WorldPt);
  v := SubtractPoints(Ln2[2].WorldPt, Ln2[1].WorldPt);

  // Calculate w0 = Ln1[1] - Ln2[1].
  w0 := SubtractPoints(Ln1[1].WorldPt, Ln2[1].WorldPt);

  // Calculate the coefficients a, b, c, d, e, and f.
  A := DotProduct(u, u);
  B := DotProduct(u, v);
  C := DotProduct(v, v);
  D := DotProduct(u, w0);
  e := DotProduct(v, w0);
  f := A * C - B * B;

  // Calculate sc and tc (the parameters of the points on the lines that are closest to each other).
  if (Abs(f) < 1E-9) then
  begin
    sc := cZero;
    if (B > C) then
         tc := D / B
    else
         tc := e / C;
  end
  else
  begin
    sc := (B * e - C * D) / f;
    tc := (A * e - B * D) / f;
  end;

  // Calculate the closest points on both lines.
  u := MultiplyPointByScalar(u, sc);
  v := MultiplyPointByScalar(v, tc);

  pointOnLine1 := AddPoints(Ln1[1].WorldPt, u);
  pointOnLine2 := AddPoints(Ln2[1].WorldPt, v);

  // Calculate the midpoint between the closest points.
  Result := AddPoints(pointOnLine1, pointOnLine2);
  Result := MultiplyPointByScalar(Result, 0.5);
end; { FindIntersectionOrClosestPoint }

function GetAngleBisectorLine(const Ln1, Ln2: TLine3D): TLine3D;
var
  d1, d2, bisectorDirection: dcPoint;

begin
  // Get the direction vectors of the lines.
  d1 := SubtractPoints(Ln1[2].WorldPt, Ln1[1].WorldPt);
  d2 := SubtractPoints(Ln2[2].WorldPt, Ln2[1].WorldPt);

  // Calculate the bisector direction vector.
  bisectorDirection := AddPoints(d1, d2);

  // Create the angle bisector line using the intersection point of the original lines (assuming they intersect) and the bisector direction vector.
  Result[1].WorldPt := Ln1[1].WorldPt; // Use the starting point of the first line, assuming it's the intersection point
  Result[2].WorldPt := AddPoints(Ln1[1].WorldPt, bisectorDirection);
end; { GetAngleBisectorLine }

procedure CalculateIntersectionPlane(const p1, v1, p2, v2: dcPoint; out PlanePoint, PlaneNormal: dcPoint);
var
  n, N_norm, P1_closest, P2_closest, w: dcPoint;
  M: dcPoint3D;
  proj1, proj2, lenN, A, B, D: TFloat;

begin
  // Calculate the normal vector (N) using cross product
  n := CrossProduct(v1, v2);
  lenN := Sqrt(DotProduct(n, n));

  // Normalize N
  N_norm.x := n.x / lenN;
  N_norm.y := n.y / lenN;
  N_norm.z := n.z / lenN;

  // Calculate the scalar projection
  w := SubtractPoints(p1, p2);
  A := DotProduct(v1, v1);
  B := DotProduct(v1, v2);
  D := DotProduct(v2, v2);
  proj1 := (DotProduct(w, v2) * B - DotProduct(w, v1) * D) / (A * D - B * B);
  proj2 := (DotProduct(w, v1) * A - DotProduct(w, v2) * B) / (A * D - B * B);

  // Find the closest points on both lines
  P1_closest := AddPoints(p1, MultiplyPointByScalar(v1, proj1));
  P2_closest := AddPoints(p2, MultiplyPointByScalar(v2, proj2));

  // Calculate the intersection point of the lines
  M := dcPoint3D.Create(dcPointNew());
  VirtualLineIntersection3D(p1, p2, v1, v2, M);
  // M.x := (P1_closest.x + P2_closest.x) / 2;
  // M.y := (P1_closest.y + P2_closest.y) / 2;
  // M.z := (P1_closest.z + P2_closest.z) / 2;

  PlanePoint := M.WorldPt;
  PlaneNormal := AddPoints(M.WorldPt, N_norm); // Add the intersection point to the normal vector
end; { CalculateIntersectionPlane }

function Hypot(const x, y: TFloat): TFloat; overload;
{ formula: Sqrt(X*X + Y*Y)
  implemented as:  |Y|*Sqrt(1+Sqr(X/Y)), |X| < |Y| for greater precision }
var
  Temp, TempX, TempY: TFloat;
begin
  TempX := Abs(x);
  TempY := Abs(y);
  if TempX > TempY then
  begin
    Temp := TempX;
    TempX := TempY;
    TempY := Temp;
  end;
  if TempX = cZero then
       Result := TempY
  else // TempY > TempX, TempX <> cZero, so TempY > cZero
       Result := TempY * Sqrt(1 + Sqr(TempX / TempY));
end; { Hypot }

function Hypot(const x, y, z: TFloat): TFloat; overload;
var
  Temp, TempX, TempY, TempZ: TFloat;
begin
  TempX := Abs(x);
  TempY := Abs(y);
  TempZ := Abs(z);
  if TempX > TempY then
  begin
    Temp := TempX;
    TempX := TempY;
    TempY := Temp;
  end;
  if TempY > TempZ then
  begin
    Temp := TempZ;
    TempZ := TempY;
    TempY := Temp;
  end;
  if TempZ = cZero then
       Result := cZero
  else
       Result := TempZ * Sqrt(1 + Sqr(TempX / TempZ) + Sqr(TempY / TempZ));
end; { Hypot }

function PolygonWasFixed(var Ent: Entity; FixIt: Boolean = True): Boolean;
{ Entity assumed to be Polygon for now }
var
  WasFixed: Boolean;
  NewPoints: PntArr;
  nPntsNew: Integer;
  OrigNormal, NewNormal: dcPoint;

begin
  Result := False;

  if not PointsAreCoplanar(Ent.plypnt, Ent.plynpnt) then begin
    // LogDCAL('Polygon might not be flat!');
    OrigNormal := SurfaceNormal(Ent.plypnt);
    NewNormal := OrigNormal;
    FixPolygonPoints(Ent.plypnt, Ent.plynpnt, NewNormal);
    // LogDCAL('OrigNormal: ' + FloatToStr(OrigNormal.x) + FloatToStr(OrigNormal.y) + FloatToStr(OrigNormal.z));
    // LogDCAL('NewNormal: ' + FloatToStr(NewNormal.x) + FloatToStr(NewNormal.y) + FloatToStr(NewNormal.z));
    // if not PointsAreCoplanar(Ent.plypnt, Ent.plynpnt) then begin
    // LogDCAL('Polygon STILL might not be flat!');
    // end;
    Result := True;
  end;

  if RemoveDuplicatePoints(Ent.plypnt, Ent.plynpnt, Epsilon, NewPoints, nPntsNew) then begin
    Ent.plypnt := NewPoints;
    Ent.plynpnt := nPntsNew;
    Result := True;
  end;

  if RemoveCollinearPoints(Ent.plypnt, Ent.plynpnt, NewPoints, nPntsNew) then begin
    Ent.plypnt := NewPoints;
    Ent.plynpnt := nPntsNew;
    Result := True;
  end;

  if Result and FixIt then begin
    Ent_Update(Ent); { Fix now so we can re-select it }
  end;
end; { EntWasFixed }

end.
