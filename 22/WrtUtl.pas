{ ---------------------------------------------------------------------------- }
{ }
{ DCAL Macro Include File 'WrtUtl' For DataCAD Version 3.6 }
{ }
{ Written by: }
{ The Programming Staff }
{ Software Development }
{ CADKEY, INC. }
{ }
{ Copyright C 1988 - 1990 CADKEY, INC. }
{ }
{ ---------------------------------------------------------------------------- }
{ }
{ This program may be copied, modified, distributed, and exchanged for }
{ personal use or demonstrations.  It may not be sold for profit. }
{ }
{ ---------------------------------------------------------------------------- }

unit WrtUtl;

interface

// #message 'Including file wrtutl.inc ...'

uses
  {DCAL for Delphi Units}
  UConstants, UDCLibrary, URecords, UInterfaces, UInterfacesRecords, UVariables;

{ RoofIt Units }
// rMenuChoice;

procedure cvpntst(pnt: point; var str: shortstring { str80 } );
procedure errStart;
procedure errShow;
procedure errStr(str: shortstring);
procedure errInt(i: integer);
procedure errRl(r: real);
procedure errDis(r: real);
procedure errAng(a: real);
procedure errpnt(pnt: point);
procedure msgStart;
procedure msgShow;
procedure msgStr(str: shortstring);
procedure msgInt(i: integer);
procedure msgRl(r: real);
procedure msgDis(r: real);
procedure msgAng(a: real);
procedure msgpnt(pnt: point);
procedure strStart;
procedure strShow(i: integer);
procedure strStr(str: shortstring);
procedure strInt(i: integer);
procedure strRl(r: real);
procedure strDis(r: real);
procedure strAng(a: real);
procedure strpnt(pnt: point);
procedure scrShow(row, col, { const } clr: integer);
procedure scrStr(row, col, { const } clr: integer; { const } str: shortstring { string } );
procedure scrInt(row, col, clr, { const } int: integer);
procedure scrDis(row, col, { const } clr: integer; { const } dis: real);
procedure scrRl(row, col, { const } clr: integer; { const } rl: real);
procedure scrAng(row, col, { const } clr: integer; { const } ang: real);
procedure scrPnt(row, col, { const } clr: integer; { const } pnt: point);

implementation

var
  err: shortstring { str80 };
  msg: shortstring { str80 };
  lblstr: shortstring { str80 };

procedure cvpntst(pnt: point; var str: shortstring { str80 } );

var
  tmp: shortstring { str80 };

begin
  str := '(';
  if pnt.x < 0.0 then begin
    strcat(str, '-');
  end;
  cvdisst(pnt.x, tmp);
  strcat(str, tmp);
  strcat(str, ', ');
  if pnt.y < 0.0 then begin
    strcat(str, '-');
  end;
  cvdisst(pnt.y, tmp);
  strcat(str, tmp);
  strcat(str, ', ');
  if pnt.z < 0.0 then begin
    strcat(str, '-');
  end;
  cvdisst(pnt.z, tmp);
  strcat(str, tmp);
  strcat(str, ')');
end; // cvpntst;

{ ---------------------------------------------------------------------------- }

procedure errStart;

begin
  err := '';
end; // errStart;

procedure errShow;

begin
  wrterr(err);
end; // errShow;

procedure errStr(str: shortstring);

begin
  strcat(err, str);
end; // errStr;

procedure errInt(i: integer);

var
  str: shortstring { str80 };

begin
  cvintst(i, str);
  errStr(str);
end; // errInt;

procedure errRl(r: real);

var
  str: shortstring { str80 };

begin
  cvrllst(r, str);
  errStr(str);
end; // errRl;

procedure errDis(r: real);

var
  str: shortstring { str80 };

begin
  cvdisst(r, str);
  errStr(str);
end; // errDis;

procedure errAng(a: real);

var
  str: shortstring { str80 };

begin
  cvangst(a, str);
  errStr(str);
end; // errAng;

procedure errpnt(pnt: point);

var
  str: shortstring { str80 };

begin
  cvpntst(pnt, str);
  errStr(str);
end; // errpnt;

{ ---------------------------------------------------------------------------- }

procedure msgStart;

begin
  msg := '';
end; // msgStart;

procedure msgShow;

begin
  wrtmsg(msg);
end; // msgShow;

procedure msgStr(str: shortstring);

begin
  strcat(msg, str);
end; // msgStr;

procedure msgInt(i: integer);

var
  str: shortstring { str80 };

begin
  cvintst(i, str);
  msgStr(str);
end; // msgInt;

procedure msgRl(r: real);

var
  str: shortstring { str80 };

begin
  cvrllst(r, str);
  msgStr(str);
end; // msgRl;

procedure msgDis(r: real);

var
  str: shortstring { str80 };

begin
  cvdisst(r, str);
  msgStr(str);
end; // msgDis;

procedure msgAng(a: real);

var
  str: shortstring { str80 };

begin
  cvangst(a, str);
  msgStr(str);
end; // msgAng;

procedure msgpnt(pnt: point);

var
  str: shortstring { str80 };

begin
  cvpntst(pnt, str);
  msgStr(str);
end; // msgpnt;

{ ---------------------------------------------------------------------------- }

procedure strStart;

begin
  lblstr := '';
end; // strStart;

procedure strShow(i: integer);

begin
  lblmsg(i, lblstr);
end; // strShow;

procedure strStr(str: shortstring);

begin
  strcat(lblstr, str);
end; // strStr;

procedure strInt(i: integer);

var
  str: shortstring { str80 };

begin
  cvintst(i, str);
  strStr(str);
end; // strInt;

procedure strRl(r: real);

var
  str: shortstring { str80 };

begin
  cvrllst(r, str);
  strStr(str);
end; // strRl;

procedure strDis(r: real);

var
  str: shortstring { str80 };

begin
  cvdisst(r, str);
  strStr(str);
end; // strDis;

procedure strAng(a: real);

var
  str: shortstring { str80 };

begin
  cvangst(a, str);
  strStr(str);
end; // strAng;

procedure strpnt(pnt: point);

var
  str: shortstring { str80 };

begin
  cvpntst(pnt, str);
  strStr(str);
end; // strpnt;

{ ---------------------------------------------------------------------------- }

procedure scrShow(row, col, { const } clr: integer);

begin
  printstr(lblstr, col, row, clr, 0, false);
end; // scrShow;

procedure scrStr(row, col, { const } clr: integer; { const } str: shortstring);

begin
  printstr(str, col, row, clr, 0, false);
end; // scrStr;

procedure scrInt(row, col, clr, { const } int: integer);

var
  str: shortstring { str80 };

begin
  cvintst(int, str);
  printstr(str, col, row, clr, 0, false);
end; // scrInt;

procedure scrDis(row, col, { const } clr: integer; { const } dis: real);

var
  str: shortstring { str80 };

begin
  cvdisst(dis, str);
  printstr(str, col, row, clr, 0, false);
end; // scrDis;

procedure scrRl(row, col, { const } clr: integer; { const } rl: real);

var
  str: shortstring { str80 };

begin
  cvrllst(rl, str);
  printstr(str, col, row, clr, 0, false);
end; // scrRl;

procedure scrAng(row, col, { const } clr: integer; { const } ang: real);

var
  str: shortstring { str80 };

begin
  cvangst(ang, str);
  printstr(str, col, row, clr, 0, false);
end; // scrAng;

procedure scrPnt(row, col, { const } clr: integer; { const } pnt: point);

var
  str: shortstring { str80 };

begin
  cvpntst(pnt, str);
  printstr(str, col, row, clr, 0, false);
end; // scrPnt;

// end; // wrtUtl.

end.
