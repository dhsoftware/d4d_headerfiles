unit ShortStrList;

interface

uses
{$IF CompilerVersion >= 23.0}
  System.Classes;
{$ELSE}
  Classes;
{$IFEND}

type
  TShortStringList = class
  private
    FList: TList;
    function Get(Index: Integer): ShortString;
    procedure Put(Index: Integer; const Value: ShortString);
    function GetCount: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(const Value: ShortString);
    procedure Clear;
    property Items[Index: Integer]: ShortString read Get write Put; default;
    property Count: Integer read GetCount;
  end;

implementation

constructor TShortStringList.Create;
begin
  FList := TList.Create;
end;

destructor TShortStringList.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

function TShortStringList.Get(Index: Integer): ShortString;
begin
  Result := PShortString(FList[Index])^;
end;

procedure TShortStringList.Put(Index: Integer; const Value: ShortString);
var
  P: PShortString;
begin
  New(P);
  P^ := Value;
  FList[Index] := P;
end;

function TShortStringList.GetCount: Integer;
begin
  Result := FList.Count;
end;

procedure TShortStringList.Add(const Value: ShortString);
var
  P: PShortString;
begin
  New(P);
  P^ := Value;
  FList.Add(P);
end;

procedure TShortStringList.Clear;
var
  i: Integer;
begin
  for i := 0 to FList.Count - 1 do
    Dispose(PShortString(FList[i]));
  FList.Clear;
end;

end.
