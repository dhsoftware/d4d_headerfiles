unit uDcalAddEntity;

{$IF CompilerVersion < 23.0}
{$LONGSTRINGS OFF} { <--- Ignored by the compiler in RAD Studio }
{$IFEND}

interface

uses
{$IF CompilerVersion >= 23.0}
  System.SysUtils,
  System.Math,
  System.Math.Vectors,
{$ELSE}
  SysUtils,
  Math,
{$IFEND}
  uConstants,
  uRecords,
  uDcMath,
  Trim3D_Main;

function CompareEntities(Ent1, Ent2: Entity): Boolean;
function IsDuplicateEntity(Ent: Entity): Boolean;

function AddMarkerFunc(Pt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity; { overload; }
function AddMarker(x, y, z: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity; overload;
procedure AddMarker(var Ent: Entity; Pt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
procedure AddMarker(Pt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;

procedure Add2DLine(p1, p2: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure Add3DLine(p1, p2: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
procedure Add3DLine(Ln: TLine3D; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
procedure AddHelpLine(p1, p2: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
function AddText(Pt: dcPoint; Clr: Byte; Str: ShortString; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity;
{ overload }
function AddCircle(CtrPt: dcPoint; Rad: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity;
procedure AddPolyline(Poly: PntArr; nPnts: Integer; Centroid: dcPoint; Mat: ModMat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False;
   DrawMode: Integer = DrMode_White);
procedure UpdatePolyline(var Ent: Entity; DrawMode: Integer = DrMode_White);
procedure AddPolygon(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);

procedure AddBezier(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddBSpline(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddMeshSurface(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddContours(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddRevSurface(CtrPt, EndPt: dcPoint; Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False;
   DrawMode: Integer = DrMode_White);
procedure AddSymbol(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddDimension(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddArc(CtrPt: dcPoint; Rad: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure Add3DArc(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddEllipse(CtrPt: dcPoint; RadX, RadY: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddBlock(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddSlab(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddCone(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddTrunCone(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddCylinder(CtrPt, EndPt: dcPoint; Len, Rad: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False;
   DrawMode: Integer = DrMode_White);
procedure AddSphere(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
procedure AddTorus(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);

procedure AddPolygonNodeNumbers(Ent: Entity; var NodeNums: TEntArr);
procedure ClearNodeNumbers(var NodeNums: TEntArr); overload;
procedure ClearNodeNumbers(l: pTrim3DL); overload;

implementation

uses
  Vcl.Graphics,
  uInterfaces,
  uInterfacesRecords,
  Clipper;

procedure CopyEntProperties(const EntToCopy: Entity; var EntToUpdate: Entity);
var
  i: Integer;
begin
  if (EntToCopy.EntType <> EntToUpdate.EntType) then Exit;

  if EntToCopy.EntType = EntPly then begin
    for i := 1 to MaxPoly do
         EntToUpdate.PlyPnt[i] := EntToCopy.PlyPnt[i];

    EntToUpdate.PlynPnt := EntToCopy.PlynPnt;

    EntToUpdate.Color := EntToCopy.Color;
  end;
end;

function CompareEntities(Ent1, Ent2: Entity): Boolean;
begin
  Result := False;
  if (Ent1.EntType <> Ent2.EntType) then Exit;

  case Ent1.EntType of
    EntMrk: begin
        if SamePoint(Ent1.mrkpnt, Ent2.mrkpnt, Epsilon) then
             Result := True;
      end;
    EntLin: begin
        if SamePoint(Ent1.linpt1, Ent2.linpt1, Epsilon) then
          if SamePoint(Ent1.linpt2, Ent2.linpt2, Epsilon) then
               Result := True;
      end;
    EntLn3: begin
        if SamePoint(Ent1.ln3pt1, Ent2.ln3pt1, Epsilon) then
          if SamePoint(Ent1.ln3pt2, Ent2.ln3pt2, Epsilon) then
               Result := True;
      end;
    EntHelpLn: begin
        if SamePoint(Ent1.helplnpt1, Ent2.helplnpt1, Epsilon) then
          if SamePoint(Ent1.helplnpt2, Ent2.helplnpt2, Epsilon) then
               Result := True;

        if not Result then
          if VirtualLineIntersection3D(Ent1.helplnpt1, Ent1.helplnpt2, Ent2.helplnpt1, Ent2.helplnpt2) = irCoincident then
               Result := True;
      end;
    EntCrc: begin
        // Do Something
      end;
  end; { case }
end; { CompareEntities }

function IsDuplicateEntity(Ent: Entity): Boolean;
var
  Mode: Mode_Type;
  Addr: lgl_addr;
  Existing: Entity;
  TempPt: dcPoint;

begin
  Result := False;

  mode_init(Mode);
  mode_enttype(Mode, Ent.EntType);
  Addr := ent_first(Mode);
  while ent_get(Existing, Addr) do begin
    Addr := ent_next(Existing, Mode);
    Result := CompareEntities(Ent, Existing);
  end;
end; { IsDuplicateEntity }

procedure SetCommonEntProperties(var Ent: Entity);
begin
  (*
    //Ent.enttype
    Ent.ltype := PGSV.lin
    Ent.Width
    Ent.spacing
    Ent.ovrshut
    Ent.attr
    Ent.color :=
    Ent.visLvl
    Ent.addr
    Ent.addrSpc
    Ent.Next
    Ent.prev
    Ent.lyr := PGSaveVar.currlayer;
    Ent.user0
    Ent.user1
    Ent.user2
    Ent.user3
    Ent.ss
    Ent.frstatr
    Ent.lastatr
    Ent.index
    Ent.WallIndex
    Ent.lastWallIndex
    Ent.last
    Ent.hatched
    Ent.o2cmaterialindex
    Ent.Matrix
    Ent.FixText
    Ent.HatchLocked
    Ent.NoClip
    Ent.Not_Snappable
    Ent.Non_Printing
    Ent.winlinetype :=
    Ent.winlinespacing :=
    Ent.from_chars
    Ent.ScaleDependent
    Ent.LockTextAngle
    Ent.Hidden
    Ent.FlipNormals
    Ent.For_Export
    Ent.KnockOut
    Ent.WallHatchOff
    Ent.WallFillOff
    Ent.NoWallLyrCheck
    Ent.symbolAlwaysFaceCamera
    Ent.Charset
  *)
end; { SetCommonEntProperties }

function AddMarkerFunc(Pt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity; { overload; }
begin
  if TooFarAway(Pt, cZeroPt) then Exit;

  Ent_Init(Result, EntMrk);
  Result.mrkpnt := Pt;
  // LogDCAL('z: ' + FloatToStr(Pt.z));
  Result.Non_Printing := 0;

  if not DrawOnly then begin
    // StopGroup;
    if AllowDupes then
         Ent_Add(Result)
    else
       if not IsDuplicateEntity(Result) then
         Ent_Add(Result);
  end;

  Result.Color := Clr;
  Result.MrkTyp := 1; { 1-square, 2-'x', 3-diamond, 4-dot, ... }
  Result.MrkSiz := 1;
  ent_update(Result);
  Ent_Draw(Result, DrawMode);
  // StopGroup;
end; { AddMarkerFunc }

function AddMarker(x, y, z: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity; overload;
begin
  Result := AddMarkerFunc(dcPointNew(x, y, z), Clr, AllowDupes, DrawOnly, DrawMode);
end; { AddMarker }

procedure AddMarker(var Ent: Entity; Pt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
begin
  Ent := AddMarkerFunc(Pt, Clr, AllowDupes, DrawOnly, DrawMode);
end; { AddMarker }

procedure AddMarker(Pt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
var
  Ent: Entity;
begin
  Ent := AddMarkerFunc(Pt, Clr, AllowDupes, DrawOnly, DrawMode);
end; { AddMarker }

procedure Add3DLine(p1, p2: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
var
  Ent: Entity;
begin
  if SamePoint(p1, p2, Epsilon) then Exit;

  Ent_Init(Ent, EntLn3);
  Ent.ln3pt1 := p1;
  Ent.ln3pt2 := p2;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Ent)
    else
       if not IsDuplicateEntity(Ent) then
         Ent_Add(Ent);
  end;

  Ent.Color := Clr;
  Ent.Width := 3;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end;

procedure Add3DLine(Ln: TLine3D; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White); overload;
begin
  Add3DLine(Ln[1].WorldPt, Ln[2].WorldPt, Clr, AllowDupes, DrawOnly, DrawMode);
end;

procedure Add2DLine(p1, p2: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
begin
  if SamePoint(p1, p2, Epsilon) then Exit;

  Ent_Init(Ent, EntLin);
  Ent.linpt1 := p1;
  Ent.linpt2 := p2;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Ent)
    else
       if not IsDuplicateEntity(Ent) then
         Ent_Add(Ent);
  end;

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end;

procedure AddHelpLine(p1, p2: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
begin
  p1.z := cZero;
  p2.z := cZero;
  if SamePoint(p1, p2, Epsilon) then Exit;

  Ent_Init(Ent, EntHelpLn);
  Ent.linpt1 := p1;
  Ent.linpt2 := p2;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Ent)
    else
       if not IsDuplicateEntity(Ent) then
         Ent_Add(Ent);
  end;

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end;

function AddText(Pt: dcPoint; Clr: Byte; Str: ShortString; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity;
begin
  if TooFarAway(Pt, cZeroPt) then Exit;

  Ent_Init(Result, EntTxt);
  Result.txtpnt := Pt;
  Result.txtstr := Str;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Result)
    else
       if not IsDuplicateEntity(Result) then
         Ent_Add(Result);
  end;

  Result.TxtFon := 'DataCAD';
  Result.TxtWinFont := font_ttf; { font_shx }
  Result.txttype.justification := TEXT_JUST_CENTER;
  Result.txttype.VerticalAlignment := TEXT_VALIGN_MIDDLE;
  Result.txtdisplayttf.dooutline := False;
  Result.txtdisplayttf.doFill := True;
  Result.txtdisplayttf.outlineclr := Clr;
  Result.txtdisplayttf.fillclr := Clr;
  Result.txtsiz := 384.0 * 1.5;
  Result.Color := Clr;
  Result.txtBase := Pt.z;
  Result.txtHite := Pt.z;

  ent_update(Result);
  Ent_Draw(Result, DrMode_White);
end; { AddText }

function TFillPropToByteArray(const FillProp: TFillProp): TBytes;
begin
  SetLength(Result, SizeOf(TFillProp));
  Move(FillProp, Result[0], SizeOf(TFillProp));
end;

function ByteArrayToTFillProp(const ByteArray: TBytes): TFillProp;
begin
  if Length(ByteArray) >= SizeOf(TFillProp) then
       Move(ByteArray[0], Result, SizeOf(TFillProp))
  else
       raise Exception.Create('Invalid byte array size for TFillProp conversion');
end;

procedure UpdatePolyline(var Ent: Entity; DrawMode: Integer = DrMode_White);
var
  FillAtr: Attrib;
  FillProp: TFillProp;
  FillPropBytes: TBytes;

begin
  FillProp.fillclr := 1;
  FillProp.PatternClr := 1;
  FillProp.FillPattern := 0;
  FillProp.doBMP := False;
  FillProp.doEntFillClr := True;
  FillProp.doEntPatternClr := False;
  FillProp.imgFixAspect := True;
  FillProp.ImageName := '';
  FillProp.FillOpacity := 128;
  FillProp.ImageOpacity := 255;
  FillProp.ImgOpaMethod := 0;
  FillProp.FillClrIdx := 15;
  FillProp.PattClrIdx := 1;
  FillProp.TransClr := 15;
  FillProp.TransClrIdx := 1;
  FillProp.TransClrTol := 0;

  FillPropBytes := TFillPropToByteArray(FillProp);

  FillAtr := InitSolidFillAttributeNew(RGBToDCADRGB($660066), FillProp, 1, FillPropBytes);
  atr_add2ent(Ent, FillAtr);

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { UpdatePolyline }

procedure AddPolyline(Poly: PntArr; nPnts: Integer; Centroid: dcPoint; Mat: ModMat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False;
   DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  pvert: PolyVert;
  FillAtr: Attrib;
  FillProp: TFillProp;
  FillPropBytes: TBytes;
  TransMat: ModMat;

  i: Integer;

begin
  Ent_Init(Ent, EntPln);

  for i := 1 to nPnts do begin
    polyvert_init(pvert);
    pvert.Pnt.x := Poly[i].x;
    pvert.Pnt.y := Poly[i].y;
    polyvert_add(pvert, Ent.plnfrst, Ent.plnlast);
  end;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Ent)
    else
       if not IsDuplicateEntity(Ent) then
         Ent_Add(Ent);
  end;

  Ent.Color := Clr;

  // EntPLine.plnhite :=
  Ent.plnclose := True;
  // EntPLine.plnCover :=

  FillProp.fillclr := 1;
  FillProp.PatternClr := 1;
  FillProp.FillPattern := 0;
  FillProp.doBMP := False;
  FillProp.doEntFillClr := False;
  FillProp.doEntPatternClr := False;
  FillProp.imgFixAspect := True;
  FillProp.ImageName := '';
  FillProp.FillOpacity := 128;
  FillProp.ImageOpacity := 255;
  FillProp.ImgOpaMethod := 0;
  FillProp.FillClrIdx := 1;
  FillProp.PattClrIdx := 1;
  FillProp.TransClr := 1;
  FillProp.TransClrIdx := 1;
  FillProp.TransClrTol := 0;

  FillPropBytes := TFillPropToByteArray(FillProp);

  FillAtr := InitSolidFillAttributeNew(1, FillProp, 1, FillPropBytes);
  atr_add2ent(Ent, FillAtr);

  Mat := TransposeMatrix(Mat);
  Ent.Matrix := CopyMat(Mat);

  TransMat := TranslationMatrix(Centroid.x, Centroid.y, Centroid.z);
  Ent.Matrix := MatrixMultiply(Ent.Matrix, TransMat);

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddPolyline }

procedure AddPolygon(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  pvert: PolyVert;
  i: Integer;

begin
  Ent_Init(Ent, EntPly);
  Ent.PlynPnt := nPnts;
  for i := 1 to nPnts do begin
    Ent.PlyPnt[i] := Poly[i];
    Ent.plyisln[i] := True;
  end;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Ent)
    else
       if not IsDuplicateEntity(Ent) then
         Ent_Add(Ent);
  end;

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddPolygon }

procedure AddBezier(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  i: Integer;

begin
  Ent_Init(Ent, EntBez);

  for i := 1 to 16 do begin
    Ent.bezpnt[i] := Poly[i]; { point array }
  end;

  Ent.beznpnt := 16;
  Ent.bezpnt[1] := dcPointNew();
  Ent.bezordr := 4; { not used }
  Ent.bezbase := PGSaveVar.basez;
  Ent.bezhite := PGSaveVar.hitez;

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddBezier }

procedure AddBSpline(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;
  i: Integer;

begin
  Ent_Init(Ent, EntBsp);

  for i := 1 to 16 do begin
    Ent.bsppnt[i] := Poly[i];
  end;

  Ent.bspnpnt := 16;
  Ent.bsppnt[1] := dcPointNew();
  Ent.bspordr := 3; { not used }
  Ent.bspbase := PGSaveVar.basez;
  Ent.bsphite := PGSaveVar.hitez;

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddBSpline }

procedure AddMeshSurface(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;
  i, j: Integer;

begin
  Ent_Init(Ent, EntSrf);

  for i := 1 to 4 do begin
    for j := 1 to 4 do begin
      Ent.srfpnt[i, j] := Poly[i];
    end;
  end;

  Ent.srfpnt[1, 1] := dcPointNew();
  Ent.srfsdiv := 36;
  Ent.srftdiv := 36;

  SetIdent(Ent.Matrix);
  // Axis[1] := CtrPt;
  // Axis[2] := RadPt;
  Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddMeshSurface }

procedure AddContours(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;
  i: Integer;

begin
  Ent_Init(Ent, EntCnt);

  for i := 1 to 16 do begin
    Ent.ckpnt[i] := Poly[i]; { point array }
  end;

  Ent.cntNpnt := 16;
  Ent.ckpnt[1] := dcPointNew();
  Ent.cntType := 1; { 0-natural, 1-cyclic, 2-tangent }
  Ent.cntdivs := PGSv.contdivs;
  Ent.cntstiff := PGSv.contstiff;
  Ent.cntTanPnt1 := dcPointNew();
  Ent.cntTanpnt2 := dcPointNew();

  // PGSv.contcyc
  // PGSv.contindx
  // PGSv.contmode
  // PGSv.contfixz
  // PGSv.contzsor

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddContours }

procedure AddRevSurface(CtrPt, EndPt: dcPoint; Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False;
   DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;
  i: Integer;
  pvert: PolyVert;

begin
  Ent_Init(Ent, EntRev);

  for i := 1 to nPnts do begin
    polyvert_init(pvert);
    pvert.Pnt.x := Poly[i].x;
    pvert.Pnt.y := Poly[i].y;
    polyvert_add(pvert, Ent.plnfrst, Ent.plnlast);
  end;

  Ent.revbang := PGSv.revbang;
  Ent.reveang := PGSv.reveang;
  Ent.revdiv1 := 36;
  Ent.revdiv2 := 36;
  // Ent.revfrst :=
  // Ent.revlast :=
  Ent.revtype := PGSv.revtype;
  // PGSv.revsector

  SetIdent(Ent.Matrix);
  Axis[1].WorldPt := CtrPt;
  Axis[2].WorldPt := EndPt;
  Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddRevSurface }

procedure AddSymbol(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;

begin
  Ent_Init(Ent, EntSym);
  (*
    Ent.symname
    Ent.symnameaddr
    Ent.symattrib_addr
    Ent.symstrt
    Ent.SymGTVLink

    //PGSv.symlevel
    //PGSv.Sym_Layer_Control
    //PGSv.FastSymSnap

    PGSaveVar.symbinc
    PGSaveVar.sym_enl
    PGSaveVar.symbperp2ply
    PGSaveVar.sym_exp
    PGSaveVar.symbz2ply
    PGSaveVar.symname_addr
    PGSaveVar.symfact
    PGSaveVar.symRotate
    PGSaveVar.sym_exp_currlyr
    PGSaveVar.sym_exp_oldAttrib
    PGSaveVar.SymZOffset
    PGSaveVar.symexp_currlyr
    PGSaveVar.symuseplt
    PGSaveVar.symscale
    PGSaveVar.syminview
    PGSaveVar.SymAttrAllCaps
    PGSaveVar.SymKeepGroup
    PGSaveVar.SymbolExplode2DOnly
    PGSaveVar.SymbolExplode3DOnly
    PGSaveVar.SymCreateLeaveEnts
    PGSaveVar.SymbolsRememberGTV
    PGSaveVar.SymExplodeOnOnly
    PGSaveVar.symname
    PGSaveVar.sympath1
    PGSaveVar.SymbolFolder
    PGSaveVar.SymBrowserFName
  *)

  SetIdent(Ent.Matrix);
  // Axis[1] := CtrPt;
  // Axis[2] := EndPt;
  Ent.Matrix := LineToMatrix(Axis);
  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddSymbol }

procedure AddDimension(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
begin
  Ent_Init(Ent, EntDim);

  Ent.dimpt1 := dcPointNew();
  Ent.dimpt1.z := PGSaveVar.basez;
  Ent.dimpt2 := dcPointNew();
  Ent.dimpt1.z := PGSaveVar.basez;
  // dimang := angle(Ent.dimpt1, Ent.dimpt2);
  // dimang := dimang + halfpi;
  // Ent.dimpt3.x := Ent.dimpt1.x + 384.0 * Cos(dimang);
  // Ent.dimpt3.y := Ent.dimpt1.y + 384.0 * Sin(dimang);
  Ent.dimpt1.z := PGSaveVar.basez;
  // ent.dimtxtpt :=
  Ent.dimexo1 := PGSaveVar.dimexo1;
  Ent.dimexo2 := PGSaveVar.dimexo2;
  Ent.dimexe := PGSaveVar.dimexe;
  Ent.dimdli := PGSaveVar.dimdli;
  // ent.dimang :=
  // ent.dimdis :=
  Ent.dimangl := PGSaveVar.dimangl;
  Ent.dimovr := PGSaveVar.dimmovr;
  // ent.dimfudge :=
  Ent.dimtxtsize := PGSaveVar.dimtxtsz;
  Ent.dimarrsize := PGSaveVar.dimasz;
  Ent.dimarratio := PGSaveVar.arratio;
  Ent.dimtxtaspect := PGSaveVar.dmaspct;
  // ent.dimtxtang :=
  Ent.dimBase := PGSaveVar.basez;
  Ent.dimHite := PGSaveVar.hitez;
  Ent.dimTxtslant := PGSaveVar.dmslant;
  // ent.diminc :=
  // ent.dimtm := PGSavevar.dimtm;
  Ent.dimtxtofs := PGSaveVar.dmtxtoff;
  // ent.dimfrstpt :=
  Ent.dimtxtclr := PGSaveVar.dimtxtclr;
  Ent.dimticclr := PGSaveVar.dimticclr;
  Ent.dimtolp := PGSaveVar.dimtp;
  Ent.dimtolm := PGSaveVar.dimtm;
  Ent.dimtol := PGSaveVar.dimtol;
  Ent.dimlim := PGSaveVar.dimlim;
  // ent.dimnolftmrk :=
  // ent.dimnorhtmrk :=
  Ent.dimdinstd := PGSaveVar.dinstd;
  Ent.dimFilledArrow := PGSaveVar.FilledArrow;
  // ent.DimWinfont := PGSavevar.dmwinfont;
  // ent.dimNlpts :=
  Ent.dimtxtweight := PGSaveVar.dmtxtwt;
  Ent.dimtictype := PGSaveVar.dimtic;
  Ent.dimtype := PGSaveVar.dmlntyp; { linear dimensioning type } { 0 h 1 v 2 a 3 r }
  // PGSavevar.dimtype; { linear, angular, dia, rad }
  Ent.dimticweight := PGSaveVar.dmticwt;
  Ent.dimfont := PGSaveVar.dimfont;
  Ent.dimdispttf := PGSaveVar.DispTTF;
  Ent.dimldr := False;
  Ent.dimtih := PGSaveVar.dimtih;
  Ent.dimtoh := PGSaveVar.dimtoh;
  Ent.dimse1 := PGSaveVar.dimse1;
  Ent.dimse2 := PGSaveVar.dimse2;
  Ent.dimman := False;
  Ent.dimassoc := PGSaveVar.dimautorot;
  Ent.dimtad := PGSaveVar.dimtad;
  Ent.dimsmallfrac := PGSaveVar.dimsmallfrac;
  Ent.dimnounits := PGSaveVar.dimnounits;
  Ent.dimstakfrac := PGSaveVar.dimstakfrac;
  Ent.dimnosingle := PGSaveVar.dimnosingle;
  // ent.dimldrpnts :=
  // Ent_Add(Ent);
  // ent.color := RandomRange(1, 255);
  // ent.ltype :=
  // ent.spacing :=
  // ent.Width := RandomRange(1, 4);
  // ent.ovrshut :=
  // ent.winlinetype := Random(26); { 27 }
  // ent.winlinespacing :=

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddDimension }

function AddCircle(CtrPt: dcPoint; Rad: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White): Entity;
begin
  Ent_Init(Result, EntCrc);
  Result.CrcCent := CtrPt;
  Result.CrcRad := Rad;
  Result.CrcBase := CtrPt.z; // PGSaveVar.BaseZ;
  Result.CrcHite := CtrPt.z; // PGSaveVar.HiteZ;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Result)
    else
       if not IsDuplicateEntity(Result) then
         Ent_Add(Result);
  end;

  Result.Color := Clr;
  ent_update(Result);
  Ent_Draw(Result, DrawMode);
end; { AddCircle }

procedure AddArc(CtrPt: dcPoint; Rad: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
begin
  Ent_Init(Ent, EntArc);

  Ent.ArcCent := CtrPt;
  Ent.ArcRad := Rad;
  Ent.ArcBAng := cZero;
  Ent.ArcEAng := Pi * 2.0;
  Ent.ArcBase := PGSaveVar.basez;
  Ent.ArcHite := PGSaveVar.hitez;

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddArc }

procedure Add3DArc(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;

begin
  Ent_Init(Ent, EntAr3);

  Ent.ar3div := 36;
  Ent.ar3rad := Distance3D(CtrPt, RadPt);
  Ent.ar3bang := 0.0;
  Ent.ar3eang := Pi * 2.0;
  // Ent.ar3close :=

  SetIdent(Ent.Matrix);
  Axis[1].WorldPt := CtrPt;
  Axis[2].WorldPt := RadPt;
  Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { Add3DArc }

procedure AddEllipse(CtrPt: dcPoint; RadX, RadY: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
begin
  Ent_Init(Ent, EntEll);

  Ent.ellcent := dcPointNew();
  // Ent.ellrad.x :=
  // Ent.ellrad.y :=
  Ent.ellbang := 0.0;
  Ent.elleang := Pi * 2.0;
  Ent.ellang := 0.0;
  Ent.ellBase := PGSaveVar.basez;
  Ent.ellHite := PGSaveVar.hitez;

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddEllipse }

procedure AddBlock(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  // Axis: TLine3D;

begin
  Ent_Init(Ent, EntBlk);

  Ent.blkpnt[1] := dcPointNew();
  Ent.blkpnt[2] := dcPointNew();
  Ent.blkpnt[3] := dcPointNew();
  Ent.blkpnt[4] := dcPointNew();

  // SetIdent(Ent.Matrix);
  // Axis[1] := CtrPt;
  // Axis[2] := EndPt;
  // Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddBlock }

procedure AddSlab(Poly: PntArr; nPnts: Integer; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  // Axis: TLine3D;

  FillAtr: Attrib;
  FillProp: TFillProp;

begin
  Ent_Init(Ent, EntSlb);

  Ent.slbnpnt := nPnts;
  Ent.slbpnt := Poly;
  Ent.slbthick.z := PGSv.slabthick;
  Ent.slbisln[1] := True;
  // Ent.slbfrstvoid
  // Ent.slblastvoid
  // Ent.slab

  // PGSv.slabref

  // SetIdent(Ent.Matrix);
  // Axis[1] := CtrPt;
  // Axis[2] := EndPt;
  // Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddSlab }

procedure AddCone(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;

begin
  Ent_Init(Ent, EntCon);

  Ent.concent := CtrPt;
  Ent.conrad := Distance3D(CtrPt, RadPt);
  Ent.condiv := 36;
  Ent.conbang := 0.0;
  Ent.coneang := Pi * 2.0;
  // Ent.conclose

  // PGSv.coneang

  // SetIdent(Ent.Matrix);
  Axis[1].WorldPt := CtrPt;
  Axis[2].WorldPt := RadPt;
  // Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddCone }

procedure AddTrunCone(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;

begin
  Ent_Init(Ent, EntTrn);

  Ent.trncent := CtrPt;
  Ent.trnrad1 := Distance3D(CtrPt, RadPt);
  Ent.trnrad2 := Ent.trnrad1 / 2.0;
  Ent.trnbang := 0.0;
  Ent.trneang := Pi * 2.0;
  // Ent.trnclose

  // SetIdent(Ent.Matrix);
  Axis[1].WorldPt := CtrPt;
  Axis[2].WorldPt := RadPt;
  // Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddTrunCone }

procedure AddCylinder(CtrPt, EndPt: dcPoint; Len, Rad: TFloat; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False;
   DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;
  Lyr: Layer;
  LyrName: ShortString;

begin
  Ent_Init(Ent, EntCyl);

  Ent.cyldiv := 36;
  Ent.cylrad := Rad; // PGSv.cylrad;
  Ent.cyllen := Len;
  Ent.cylbang := PGSv.cylbang;
  Ent.cyleang := PGSv.cyleang;
  Ent.cylclose := PGSv.cylsidclo;

  SetIdent(Ent.Matrix);
  Axis[1].WorldPt := CtrPt;
  Axis[2].WorldPt := EndPt;
  Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;

  if not DrawOnly then begin
    if AllowDupes then
         Ent_Add(Ent)
    else
       if not IsDuplicateEntity(Ent) then
         Ent_Add(Ent);
  end;

  ent_move(Ent, CtrPt.x, CtrPt.y, CtrPt.z);

  // ent_update(Ent);
  // Ent_Draw(Ent, DrawMode);
  (*
    LyrName := 'Cylinder';
    if lyr_find(LyrName, Lyr) then begin
    Ent.Lyr := Lyr;
    end
    else if lyr_create(LyrName, Lyr) then begin
    if lyr_find(LyrName, Lyr) then begin
    setlayername(Lyr, LyrName);
    Ent.Lyr := Lyr;
    lyr_seton(Lyr, True, False);
    end;
    end;
  *)
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);

  // ent_explode(Ent, Ent.Lyr, 1);
end; { AddCylinder }

procedure AddSphere(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;

begin
  Ent_Init(Ent, EntDom);

  Ent.domrad := Distance3D(CtrPt, MidPoint(CtrPt, RadPt));
  Ent.domdiv1 := PGSv.circldiv1;
  Ent.domdiv2 := PGSv.circldiv2;
  Ent.dombang1 := 0.0; // PGSv.dombang;
  Ent.domeang1 := Pi * 2.0; // PGSv.domeang;
  Ent.dombang2 := 0.0; // PGSv.dombang;
  Ent.domeang2 := Pi * 2; // PGSv.domeang;
  // Ent.domclose :=

  // SetIdent(Ent.Matrix);
  Axis[1].WorldPt := MidPoint(CtrPt, RadPt);
  Axis[2].WorldPt := RadPt;
  // Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddSphere }

procedure AddTorus(CtrPt, RadPt: dcPoint; Clr: Byte; AllowDupes: Boolean = True; DrawOnly: Boolean = False; DrawMode: Integer = DrMode_White);
var
  Ent: Entity;
  Axis: TLine3D;

begin
  Ent_Init(Ent, EntTor);

  Ent.tordiv1 := PGSv.circldiv1;
  Ent.tordiv2 := PGSv.circldiv2;
  Ent.torrad1 := Distance3D(CtrPt, MidPoint(CtrPt, RadPt));
  Ent.torrad2 := PGSv.torradius;
  Ent.torbang1 := 0.0; // PGSv.torbang;
  Ent.torenag1 := Pi * 2.0; // PGSv.toreang;
  Ent.torbang2 := 0.0;
  Ent.toreang2 := Pi * 2.0;
  // Ent.torclose :=

  // SetIdent(Ent.Matrix);
  Axis[1].WorldPt := CtrPt;
  Axis[2].WorldPt := RadPt;
  // Ent.Matrix := LineToMatrix(Axis);

  Ent.Color := Clr;
  ent_update(Ent);
  Ent_Draw(Ent, DrawMode);
end; { AddTorus }

procedure Inflate(var Poly: PntArr; var nPnts: Integer); overload;
{ Ent assumed to be polygon }
var
  Subj, Sol: TPathsD;
  TmpPnt: Point;
  i: Integer;

begin
  SetLength(Subj, 1);
  SetLength(Subj[0], nPnts);
  for i := 0 to Length(Subj[0]) - 1 do begin
    Subj[0][i].x := Poly[i + 1].x;
    Subj[0][i].y := Poly[i + 1].y
  end;
  Sol := InflatePaths(Subj, 384.0, { jtSquare } jtRound { jtMiter } , etPolygon, 2, 1, 1.0);
  for i := 0 to Length(Sol[0]) - 1 do begin
    Poly[i + 1].x := Sol[0][i].x;
    Poly[i + 1].y := Sol[0][i].y;
    Poly[i + 1].z := 0.0;
    if ((i + 1) = MaxPoly) then begin
      nPnts := MaxPoly;
      Break;
    end
    else begin
      nPnts := i + 1;
    end;
  end;
end; { Inflate }

procedure AddPolygonNodeNumbers(Ent: Entity; var NodeNums: TEntArr);
{ Ent assumed to be polygon }
var
  Subj, Sol: TPathsD;
  i { ,  j, np } : Integer;

  // Params: TDrawParams;
  // Cnv: TCanvas;

  RotPnts: PntArr;
  RotPntCnt: Integer;
  Centroid: dcPoint;
  RotMat: ModMat;

begin
  if (Ent.EntType <> EntPly) then Exit;

  { Rotate coplanar points to be parallel with XY plane and center them at the origin }
  RotPntCnt := Ent.PlynPnt;
  RotPnts := RotatePolyToXYPlane(Ent.PlyPnt, RotPntCnt, Centroid, RotMat);

  { Test - Add polygon using these points }
  //AddPolygon(RotPnts, RotPntCnt, 4, True, False);

  { Inflate/Shrink polyline boundary }
  SetLength(Subj, 1);
  SetLength(Subj[0], RotPntCnt);
  for i := 0 to Length(Subj[0]) - 1 do begin
    Subj[0][i].x := RotPnts[i + 1].x;
    Subj[0][i].y := RotPnts[i + 1].y;
    // Subj[0][i].Z := Trunc(Ent.plypnt[i + 1].z); { <--- Original Z values }
  end;
  Sol := InflatePaths(Subj, -384.0, jtMiter, etPolygon, 7);

  { Update RotPnts with 'Sol'ution values }
  if (Length(Sol) > 0) and (Length(Sol[0]) > 0) then begin
    for i := 0 to Length(Sol[0]) - 1 do begin
      RotPnts[i + 1].x := Sol[0][i].x;
      RotPnts[i + 1].y := Sol[0][i].y;
      // np := FindNearPointInArray(TmpPnt, Ent.PlyPnt, Ent.PlynPnt, True); { <--- Match to original points }
      RotPnts[i + 1].z := cZero; // Ent.PlyPnt[np].z;
    end;
    { The number of points may be different for the offset boundary! }
    RotPntCnt := Length(Sol[0]);
    SetLength(NodeNums, Length(Sol[0]) { * 3 } );
  end;

  { Test - Add polygon using these points }
  //AddPolygon(RotPnts, RotPntCnt, 4, True, False);

  { Rotate points back from xy plane }
  RotPnts := RotatePoints(RotPnts, RotPntCnt, TransposeMatrix(RotMat));

  { Move points back from origin }
  for i := 1 to RotPntCnt do
       RotPnts[i] := MultiplyMatPoint(TranslationMatrix(Centroid.x, Centroid.y, Centroid.z), RotPnts[i]);

  { Test - Add polygon using these pooints }
  //AddPolygon(RotPnts, RotPntCnt, 4, True, False);

  { Add temporary node numbers using text entities }
  // j := 0;
  for i := 0 to RotPntCnt - 1 do begin
    (*
      NodeNums[j] := AddMarkerFunc(RotPnts[i + 1], 15, True, True);
      Inc(j);
      NodeNums[j] := AddCircle(RotPnts[i + 1], 384, 14, True, True);
      Inc(j);
      NodeNums[j] := AddText(RotPnts[i + 1], 15, IntToStr(i + 1), True, True);
      Inc(j);
    *)
    { Draw Text entities at offset points }
    NodeNums[i] := AddText(RotPnts[i + 1], 15, IntToStr(i + 1), True, True);

    { Draw text at offset points projected to Draw Form }
    // DrawTextAtDrawPoint(WorldPointToDrawPoint(RotPnts[i + 1]), IntToStr(i + 1), 15);

    { Use DrawOnCanvasProc to draw text at offset points projected to Draw Form }
    // Params.Pt1 := WorldPointToDrawPoint(RotPnts[i + 1]);
    // Params.Pt2 := WorldPointToDrawPoint(RotPnts[i + 1]);

    // Cnv := TCanvas.Create;

    // if Assigned(DrawOnCanvas) then
    // DrawOnCanvas(DrawOnCanvasProc, Params);
  end;
end; { AddPolygonNodeNumbers }

procedure ClearNodeNumbers(var NodeNums: TEntArr); overload;
var
  i: Integer;
begin
  for i := 0 to Length(NodeNums) - 1 do begin
    Ent_Draw(NodeNums[i], DrMode_Black);
  end;
end; { ClearNodeNumbers }

procedure ClearNodeNumbers(l: pTrim3DL); overload;
begin
  ClearNodeNumbers(l.NodeNums1);
  ClearNodeNumbers(l.NodeNums2);
end; { ClearNodeNumbers }

end.
