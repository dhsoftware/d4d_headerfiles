unit UDCLibrary;


interface

uses
{$IF CompilerVersion >= 23.0}
  System.SysUtils,
{$ELSE}
  SysUtils,
{$IFEND}
  UConstants,
  UInterfaces,
  UInterfacesRecords,
  URecords;
function linecolor: asint;

procedure strassign(var tostr: shortstring; fromstr: shortstring);

procedure printstr(str: string; row, col, clr, cursor: integer; inverse: boolean);

procedure Beep;
function wall_search: boolean;

function file_exist(fname: string): boolean;
function file_del(fname: string): boolean;

function lyr_undo(): layer;

function atan(ang: afloat): afloat;

implementation

procedure strassign(var tostr: shortstring; fromstr: shortstring);
begin
  tostr := fromstr;
end;

procedure printstr(str: string; row, col, clr, cursor: integer; inverse: boolean);
begin
  // Do Something
end;

function linecolor: asint;
begin
  Result := currentstate.color;
end;

procedure Beep;
begin
  // Do Something
end;

function wall_search: boolean;
begin
  Result := True;
end;

function file_exist(fname: string): boolean;
begin
  Result := fileexists(fname);
end;

function file_del(fname: string): boolean;
begin
  Result := DeleteFile(fname);
end;

function lyr_undo(): layer;
begin
  // result := delplyr;
end;

function atan(ang: afloat): afloat;
begin
  Result := arctan(ang);
end;

end.
