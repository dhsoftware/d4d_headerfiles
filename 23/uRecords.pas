unit uRecords;

{$IF CompilerVersion < 23.0}
{$LONGSTRINGS OFF} { <--- Ignored by the compiler in RAD Studio }
{$IFEND}

interface

uses
{$IF CompilerVersion >= 23.0}
  WinApi.Windows, Vcl.OleCtrls,
{$ELSE}
  Windows,
{$IFEND}
  uConstants;

type
  aSInt = Integer;
  paSInt = ^aSInt;

  { uDgType }
  aFloat = Double;
  pafloat = ^aFloat;

  TFloat = Extended;

  { uDgType }
  WantType = aSInt;
  Action = (aFirst, aLast, aAgain, alSize);
  CrtStat = (Res_Normal, Res_Error, Res_Escape);

  { uDgType }
  AdrMem = Pointer;
  AdsMem = Pointer;
  AdsFunc = Pointer;

  { uDgType }
  str80 = string[81];
  pstr80 = ^str80;
  str8 = string[9];
  str16 = string[17];
  str132 = string[133];
  str255 = string[255];
  pstr255 = ^str255;
  pstr132 = ^str132;
  bset256 = array [0 .. 15] of aSInt;
  keystr = ShortString;

  { uDcDDfn }
  fontstr = string[80];
  lyr_name_8 = string[lyr_name_max];
  symstr = string[sym_len];

  { uDcVars }
  set_name = string[8];

  { uDgType }
  lgl_addr = packed record
    page, ofs: aSInt;
  end;

  entaddr = lgl_addr;
  symaddr = lgl_addr;
  atraddr = lgl_addr;
  pvtaddr = lgl_addr;
  viewaddr = lgl_addr;
  lyraddr = lgl_addr;
  plgl_addr = ^lgl_addr;

  Point = packed record
    case aSInt of
      0: (x, y, z: aFloat);
      1: (v: array [0 .. 2] of aFloat); { vertex }
  end;

  dcPoint = Point;
  pdcPoint = ^dcPoint;

  // CursPos, { Coordinate of cursor over Draw Form in pixels }
  // ScreenPt, { Coordinate of cursor over Desktop in pixels }
  // DrawPt: TPoint; { Coordinate of cursor in Draw Form units }
  // WorldPt: dcPoint; { 3D World coordinate }
  // PickPt: dcPickPt; { 2D World coordinate relative to current View Projection }

{$IF CompilerVersion >= 23.0}
  dcPoint3D = record
{$ELSE}
  dcPoint3D = class
{$IFEND}
  private
    fWorldPt: dcPoint;
    fDrawPt: TPoint; { Cache calculated DrawPt }
    procedure SetWorldPt(const Val: dcPoint);
    procedure SetWorldPtX(const Val: aFloat);
    procedure SetWorldPtY(const Val: aFloat);
    procedure SetWorldPtZ(const Val: aFloat);
  public
    property WorldPt: dcPoint read fWorldPt write SetWorldPt; { 1/32" }
    property DrawPt: TPoint read fDrawPt; { 0.01" }
    property WorldPtX: aFloat read fWorldPt.x write SetWorldPtX;
    property WorldPtY: aFloat read fWorldPt.y write SetWorldPtY;
    property WorldPtZ: aFloat read fWorldPt.z write SetWorldPtZ;
    function SameValue(Val, TestVal: TFloat; Epsilon: TFloat): Boolean;
    function SamePoint(Pt, TestPt: dcPoint; Epsilon: TFloat; IgnoreZ: Boolean = False): Boolean;
    constructor Create(Pt: dcPoint);
  end;

{$IF CompilerVersion >= 23.0}
  dcPickPt = record
{$ELSE}
  dcPickPt = class
{$IFEND}
  private
    fWorldPt: dcPoint;
    fDrawPt: TPoint; { Cache calculated DrawPt }
    procedure SetWorldPt(const Val: dcPoint);
    procedure SetWorldPtX(const Val: aFloat);
    procedure SetWorldPtY(const Val: aFloat);
    procedure SetWorldPtZ(const Val: aFloat);
  public
    property WorldPt: dcPoint read fWorldPt write SetWorldPt; { 1/32" }
    property DrawPt: TPoint read fDrawPt; { 0.01" }
    property WorldPtX: aFloat read fWorldPt.x write SetWorldPtX;
    property WorldPtY: aFloat read fWorldPt.y write SetWorldPtY;
    property WorldPtZ: aFloat read fWorldPt.z write SetWorldPtZ;
    function SameValue(Val, TestVal: TFloat; Epsilon: TFloat): Boolean;
    function SamePoint(Pt, TestPt: dcPoint; Epsilon: TFloat; IgnoreZ: Boolean = False): Boolean;
    constructor Create(Pt: dcPoint);
  end;

  Point2D = record
    x, y: aFloat;
  end;

  { -1 = Color by Entity, 0 .. 255 = Palette Index, >= DCADRGBBlack = Custom }
  //TDCColor = -1 .. $FFFFFF + DCADRGBBlack;
  TDCColor = type Longint;

  { -1 = Color by Entity, 0 .. 255 = Palette Index }
  TDCPalIdx = type Integer; { -1 .. 255 }

  timestamp = Longint;

  DxfModeType = set of (Dxf_Unused07, Dxf_Unused06, Dxf_Unused05, Dxf_Unused04, Dxf_Unused03, Dxf_Unused02,
     Dxf_Automatic, Dxf_Move, Dxf_Unused15, Dxf_Unused14, Dxf_Unused13, Dxf_Unused12, Dxf_Unused11, Dxf_Unused10,
     Dxf_Unused09, Dxf_Unused08);

  TxtMaskType = set of (TxtM_sText, TxtM_sCaseSens, TxtM_sFont, TxtM_sSize, TxtM_Unused03, TxtM_Unused02, TxtM_Unused01,
     TxtM_Unused00, TxtM_Unused15, TxtM_Unused14, TxtM_Unused13, TxtM_Unused12, TxtM_Unused11, TxtM_Unused10,
     TxtM_Unused09, TxtM_Unused08);

  Mask_Ent_Arry = array [1 .. MaxMaskEnts] of Boolean;

  { uDgType }
  PSelectMaskData = ^TSelectMaskData;

  TSelectMaskData = record
    UseMask: Boolean;
    Ents: Mask_Ent_Arry;
    SearchText: Str80;

    TextIsMasked, doEnts, doWgt, doLines, doClrs: boolean;

    Weight, {Custom,} LineType{, Color}: aSInt;
    Custom, Color: TDCPalIdx;

    HatchIsMasked: Boolean;
    SearchPattern: Str255;

    IsHatched: Boolean;
    IsFilled: Boolean;

    //SymbolIsMasked: Boolean;
    //SearchSymbolName: Str255; { Mask }
    //XrefIsMasked: Boolean;
    //SearchXrefName: Str255; { Mask }
  end;

  TClrBtnStat = set of (cbtn_normal, cbtn_on, cbtn_off, cbtn_lyractive, cbtn_lyrlocked);

  TDCMenuBtnInfo = packed record
    name: ShortString; // KeyStr; Cannot use longstring because this record
    // is sometimes part of state record and thus there is no finalization to
    // the string. Hence short string
    Toggle: TClrBtnStat;
    lblfilelblnum: Integer;
    Hint: ShortString;
  end;

  keyrec = packed record
    CSToolbarId: aSInt;
    WrtLvl: string[20];
    DCMenuBtns: array [1 .. 20] of TDCMenuBtnInfo;
  end;

  handletype = smallint;
  Dta = array [0 .. 79] of byte;

  fileType = packed record
    // dpsv - dont think any reason to change this record.
    handle: handletype;
    error: smallint;
    nextchar: char;
    dummy: Boolean;
    nextvalid: Boolean;
    istext: Boolean;
    recordlen: smallint;
    buf: array [0 .. bufSize - 1] of char;
    ptrBuf: smallint; { next char to read in buffer }
    numBuf: smallint; { num chars in buffer }
    mode: smallint;
  end;

  pFileType = ^fileType;

  RTextType = packed record
    backwards: Boolean;
    upsidedown: Boolean;
    justification: TEXT_JUST_LEFT .. TEXT_JUST_FIT;
    VerticalAlignment: TEXT_VALIGN_BASELINE .. TEXT_VALIGN_TOP;
  end;

  vec4 = packed array [1 .. 4] of aFloat;
  modmat = packed array [1 .. 4] of vec4;
  pntmat = array [1 .. 4] of array [1 .. 4] of dcPoint; { Points for bezier surface }
  pntarr = array [1 .. maxpoly] of dcPoint;
  pnt4arr = array [1 .. 4] of dcPoint;
  polyarr = pntarr;
  ppolyarr = ^polyarr;
  boolarr = array [1 .. maxpoly] of Boolean;
  bezarr = array [1 .. max_spline] of dcPoint;

  layer = lyraddr; { This is what old DCAL means }

  Rlayer = packed record
    addr: lyraddr; { logical address of this layer description }
    addrSpc: AdsMem;
    nxt: lyraddr;
    prev: lyraddr;
    frstatr, lastatr: atraddr;
    chg: Boolean; { true if changed }
    on: Boolean; { = true if plane is on }
    num: aSInt; { layer number }
    cnt: Longint; { number of lines on this layer }
    color: aSInt;
    name: lyr_name_8; { contains the plane name }
    firstln: entaddr; { first line on this level }
    lastln: entaddr; { last line in link }
    gridorg: dcPoint; { grid origin }
    showdivs: dcPoint; { size of displayed grid (1/32's of an inch) }
    shw1divs: dcPoint; { larger display grid }
    snapdivs: dcPoint;
    snapgrid: Boolean; { true if snapping to grid }
    showgrid: Boolean; { true if showgrid is displayed }
    shw1grid: Boolean;
    stupid03: Boolean;
    numdivs: aSInt; { number of angular divisions }
    gridang: aFloat;
    onoff: aSInt;
    group: aSInt; { group number for this layer }
    min: dcPoint;
    max: dcPoint;
    refpnt: dcPoint; { reference dcPoint }
    lastindex: Longint; { last index in layer }
    materialindex: word;
  end;

  polyvert = packed record
    addrSpc: AdsMem;
    addr: pvtaddr;
    Next: pvtaddr;
    prev: pvtaddr;
    shape: aSInt;
    pnt: dcPoint;
    bulge: aFloat;
    nextpnt: dcPoint; { Next dcPoint in list }
    last: Boolean; { True if this is the last in list }
    info: byte;
  end;

  TDisplayTTF = packed record
    dooutline: Boolean;
    dofill: Boolean;
    OutlineClr: aSInt; { Range 1..255 }
    FillClr: Integer; { Range 1..255, Add 1000 Offset to fixed RGB }
  end;

  TFontType = (font_shx, font_ttf);

  TDCTimeStamp = packed record
    Date, Time: Integer;
  end;

  pentity = ^entity;

  entity = packed record
    enttype: byte; // aSInt;
    ltype: aSInt; // For embedded linetype to work due to parameter passing
    Width: byte; // aSInt;
    spacing: aFloat;
    ovrshut: aFloat;
    attr: byte; // aSInt;
    color: aSInt;
    visLvl: byte; // aSInt;    { visibilty level (in symbols) }
    addr: entaddr; { pointer to this entity }
    addrSpc: AdsMem;
    Next: entaddr; { pointer to the next entity }
    prev: entaddr; { pointer to the previous entity }
    lyr: lyraddr; { layer this line is on }
    user0: aSInt;
    user1: aSInt;
    user2: aSInt;
    user3: aSInt;
    ss: aSInt; { selection sets that this is a member of (bitmap) }
    frstatr: atraddr; { pointers to the list of }
    lastatr: atraddr; { attributes for this entity }
    index: Longint; { global index - number }
    WallIndex: aSInt; { index within a wall }
    lastWallIndex: aSInt; { Maximal absolute wall-index }
    last: Boolean; { true if this is the last entity in a shape }
    hatched: Boolean; { true if entity is ass. hatched }
    o2cmaterialindex: word;

    Matrix: modmat;
    FixText: byte; { For on-the-fly FixText in Symbols/XRefs }
    HatchLocked: byte;
    NoClip: byte;
    Not_Snappable: byte;
    Non_Printing: byte;
    winlinetype: byte;
    winlinespacing: aFloat;
    from_chars: byte;
    // ScaleIndependent : byte;
    ScaleDependent: byte;
    LockTextAngle: byte;
    Hidden: byte;
    FlipNormals: byte;
    For_Export: byte;
    KnockOut: byte;
    WallHatchOff,
       WallFillOff: byte;
    NoWallLyrCheck: byte;
    symbolAlwaysFaceCamera: byte; { Always face camera option for symbols.
      It is designed to be used with 'billboard'
      type symbols such as trees }
    Charset: byte;

    unusedbytes: array [1 .. 8] of byte; { Unused bytes always in the end for Undo/Redo }

    case aSInt of
      entlin: (linpt1, linpt2: dcPoint);
      entln3: (ln3pt1, ln3pt2: dcPoint);
      entmrk: (mrkpnt, mrkvec: dcPoint;
           mrktyp, mrksiz: aSInt
        ); { in pixels }
      entarc: (arccent: dcPoint;
           arcrad: aFloat;
           arcbang: aFloat;
           arceang: aFloat;
           arcbase: aFloat;
           archite: aFloat);
      entcrc: (crccent: dcPoint;
           crcrad: aFloat;
           crcbang: aFloat;
           crceang: aFloat;
           crcbase: aFloat;
           crchite: aFloat);
      entbez: (bezordr, beznpnt: aSInt;
           bezpnt: bezarr;
           bezbase, bezhite: aFloat
        );
      entbsp: (bspordr, bspnpnt: aSInt;
           bsppnt: bezarr;
           bspbase, bsphite: aFloat
        );
      entell: (ellcent: dcPoint;
           ellrad: dcPoint; { a and b, like radii }
           ellbang: aFloat;
           elleang: aFloat;
           ellang: aFloat; { angle of rotation }
           ellBase, ellHite: aFloat
        );
      entTxt: (txtpnt: dcPoint; { dcPoint around where text will be justified }
           txtpnt2: dcPoint; { Second dcPoint for fit/aligned text }
           txttype: RTextType;
           txtsiz: aFloat;
           txtang: aFloat;
           txtslant: aFloat;
           txtaspect: aFloat; { aspect ratio }
           txtBase: aFloat;
           txtHite: aFloat;
           txtfon: fontstr;
           txtdisplayttf: TDisplayTTF;
           TxtWinfont: TFontType;
           fontNum: aSInt;
           txtunderline, txtoverline: Boolean;
           txtstr: str255); { set in dcdata }
      entDim: ( { ** Record 1 ** }
           dimpt1: dcPoint; { first dimensioned dcPoint }
           dimpt2: dcPoint; { second dimensioned dcPoint }
           dimpt3: dcPoint; { third dcPoint entered }

           { ** Record 2 ** }
           dimtxtpt: dcPoint; { text position }
           dimexo1: aFloat; { extension line offset for first dcPoint }
           dimexo2: aFloat; { extension line offset for second dcPoint }
           dimexe: aFloat; { extension line extension }
           dimdli: aFloat; { dimension line increment }
           dimang: aFloat; { used only in rotated }

           { ** Record 3 ** }
           dimdis: aFloat; { last associative distance }
           dimangl: aFloat; { NOT USED }
           dimovr: aFloat; { dimension line extension amount }
           dimfudge: aFloat; { NOT USED }
           dimtxtsize: aFloat; { text size }
           dimarrsize: aFloat; { arrow size }
           dimarratio: aFloat; { arrow ratio }

           { ** Record 4 ** }
           dimtxtaspect: aFloat; { text aspect ratio }
           dimtxtang: aFloat; { text angle relative to dimension line }
           dimBase: aFloat;
           dimHite: aFloat;
           dimTxtslant: aFloat; { text slant amount }
           diminc: aSInt; { used for changing baseline dims }
           dimtm: aSInt; { lock scale }
           dimtxtofs: aFloat; { text offset distance }

           { ** Record 5 ** }
           dimfrstpt: dcPoint; { used with dmpt2 for dis }
           dimtxtclr: aSInt;
           dimticclr: aSInt;
           dimtolp: aFloat; { plus tolerance }
           dimtolm: aFloat; { minus tolerance }
           dimtol: Boolean; { tolerances }
           dimlim: Boolean; { limits }
           dimnolftmrk: Boolean; { supress left arrow }
           dimnorhtmrk: Boolean; { supress right arrow }
           dimdinstd: Boolean; { DIN standard for Soft-Tech }
           dimFilledArrow: Boolean;
           DimWinfont: TFontType;
           sigdigits: aSInt;

           { ** Record 6 ** }
           dimNlpts: aSInt; { number of leader points }
           dimtxtweight: aSInt; { text weight }
           dimtictype: aSInt; { arrow type }
           dimtype: aSInt; { dimensioning style }
           dimticweight: aSInt; { tic mark width }

           dimfont: fontstr;
           dimdispttf: TDisplayTTF;

           dimldr: Boolean; { has a leader been added }
           dimtih: Boolean; { text inside horiz }
           dimtoh: Boolean; { text outside horiz }
           dimse1: Boolean; { suppress extension line 1 }
           dimse2: Boolean; { suppress extension line 2 }
           dimman: Boolean; { text placed manually }
           dimassoc: Boolean; { used for savevar.dimautorot for soft-tech }
           dimtad: Boolean; { text above dimension line }
           dimsmallfrac: aFloat;
           dimnounits: Boolean;
           dimstakfrac: Boolean;
           dimnosingle: Boolean;

           { ** Records 7, 8 and 9 ** }
           dimldrpnts: array [1 .. ldrpts] of dcPoint;
           { leader points }
        );

      entxref: (xrefname: symstr; { Not currently used, but might use this space later }
           xrefnameaddr: lgl_addr;
           xrefSpc: aSInt; { Do Not Cast to an AddrSpcType }
           GTVLink: byte;
           ShowHatch: byte;
           ShowFill: byte;
        );

      entsym: (symname: symstr;
           symnameaddr: lgl_addr;
           symattrib_addr: lgl_addr; { do not modify this variable directly }
           symstrt: lgl_addr; { start of the symbol header }
           NewFactor: byte;
           SymGTVLink: byte;
        );

      enthol: (holnpnt: aSInt; { Number of points }
           holthick: dcPoint; { Slab thickness }
           holslab: Boolean; { True if closed }
           holconvex: Boolean; { True if convex }
           holpnt: pntarr; { Array of the points }
           holisln: boolarr; { Visible }
           holfrstvoid, hollastvoid: entaddr
        );
      entSlb: (slbnpnt: aSInt; { Number of points }
           slbthick: dcPoint; { Slab thickness }
           slab: Boolean; { True if closed }
           convex: Boolean; { True if convex }
           slbpnt: pntarr; { Array of the points }
           slbisln: boolarr; { Visible }
           slbfrstvoid, slblastvoid: entaddr
        );
      entPly: (plynpnt: aSInt; { Number of points }
           plythick: dcPoint; { Slab thickness }
           plyslab: Boolean; { True if closed }
           plyconvex: Boolean; { True if convex }
           plypnt: pntarr; { Array of the points }
           plyisln: boolarr; { Visible }
           plyfrstvoid, plylastvoid: entaddr
        ); { list of holes }
      entpln: (plnfrst: pvtaddr;
           plnlast: pvtaddr;
           plnclose: Boolean;
           plnunused: Boolean;
           plbase: aFloat;
           plnhite: aFloat;
           plnVoids: Boolean; { do we have voids? }
           plnCover: Boolean; { do we have a cover? }
           plnFrstvoid: entaddr; { List of Pline-Holes }
           plinLastvoid: entaddr;
           plinType: aSInt);

      entar3: (ar3div: aSInt;
           ar3rad: aFloat;
           ar3bang, ar3eang: aFloat;
           ar3close: Boolean);

      entcon: (condiv: aSInt;
           conrad: aFloat;
           concent: dcPoint;
           conbang, coneang: aFloat;
           conclose: Boolean);

      entcyl: (cyldiv: aSInt;
           cylrad, cyllen: aFloat;
           cylbang, cyleang: aFloat;
           cylclose: Boolean);

      enttrn: (trndiv: aSInt;
           trncent: dcPoint;
           trnrad1, trnrad2: aFloat;
           trnbang, trneang: aFloat;
           trnclose: Boolean);

      entdom: (domdiv1, domdiv2: aSInt;
           domrad: aFloat;
           dombang1, dombang2, domeang1, domeang2: aFloat;
           domclose: Boolean);

      enttor: (tordiv1, tordiv2: aSInt;
           torrad1, torrad2: aFloat;
           torbang1, torbang2, torenag1, toreang2: aFloat;
           torclose: Boolean);

      entsrf: (srfpnt: pntmat;
           srfsdiv, srftdiv: aSInt
        ); // to be release later.

      entblk: (blkpnt: pnt4arr);

      entcnt: (ckpnt: pntarr;
           cntTanPnt1: dcPoint;
           cntTanpnt2: dcPoint;
           cntNpnt, cntType, cntdivs: aSInt;
           cntstiff: aFloat);

      entrev: (revbang, reveang: aFloat;
           revdiv1, revdiv2: aSInt;
           revfrst, revlast: lgl_addr;
           revtype: aSInt);
      enthelpln: (helplnpt1, helplnpt2: dcPoint);
      entBlkAttDef: (Atttxtpnt: dcPoint;
           Atttxtpnt2: dcPoint;
           atttxttype: RTextType;
           Atttxtsiz: aFloat;
           Attang: aFloat;
           Attslant: aFloat;
           Attaspect: aFloat; { aspect ratio }
           AttChrBase: aFloat;
           AttchrHite: aFloat;
           Attfont: fontstr;
           attdispttf: TDisplayTTF;
           AttWinfont: TFontType;
           AttfontNum: aSInt;
           AttFlags: aSInt;
           AttUniqueTagNum: Longint; { Could be Used only with insert }
           AttTag, Attprompt, AttDefault: lgl_addr;
           attunderline, attoverline: Boolean;
           // Use symbol data as the last parameter for this entity type. This
           // variable is not stored in database and even undo/redo system need
           // not know about this. This carries insert data when drawing.
           attsymboldataerror: Boolean;
           AttSymbolData: lgl_addr);
      DCNullObject, DCPlainTextObject, DCRTFTextObject, DCXLSObject, DCWallObject, DCDoorObject, DCWindowObject,
         DCCutOutObject, DCUnusedObject1, DCUnusedObject2, DCUnusedObject3, DCUnusedObject4, DCUnusedObject5,
         DCUnusedObject6, DCUnusedObject7, DCUnusedObject8: (UnUsedType: aSInt;
           GroupId: TDCTimeStamp;
           SuperData: lgl_addr;
           oobj: TObject { KEEP THIS ONE LAST!! (so undo.pas doesn't need to be changed } );
  end;

  ssPtrType = packed record
    ptr: lgl_addr;
    ofs: aSInt;
  end;

  mode_type = packed record
    { Adding variables to this record should change sp_mode_type in usprecords.pas
      and make changes for DCAL as needed then }
    addrSpc: AdsMem;
    srch_mode: byte; { 0 = line, 1 = shape, 2 = area, 3 = all }
    lyr_mode: byte; { 0 = current layer, 1 = all on layers, 2 = all layers
      3 = use entity in entaddr }
    greedy_box: byte; { bit0 = box; bit1=greedy }

    entaddr: lgl_addr; { start with this entity if lyr_mode = 3 }
    ssNum: aSInt; { selection set number to use }
    ssPtr: ssPtrType; { used when reading from a  selection set (lyr_mode = 4) }
    pnt1, { min dcPoint for inside test }
    pnt2: dcPoint; { max dcPoint for inside test }
    lyr_last: lgl_addr; { last entity on this layer }
    one_lyr: lgl_addr; { the only layer to search, used in lyr_curr and 1lyr }
    lyr_stop: Boolean; { stop at last entity on every layer ? }
    some_ent: Boolean; { only select some entities }
    ents: array [firstent .. lastent] of Boolean;
    quickcheck: Boolean;
    from_dcal: Boolean;
    SelectMask: Pointer; // PSelectMaskData;
    shape: Boolean; { stop if end of shape is found }
    one_ent: Boolean;
    ignore: Boolean;
    fence: Boolean;
    atr: Boolean; { check for an attribute ? }
    atrName: string[13]; { attribute to check for }
    npnt: aSInt;
    fmin, fmax: dcPoint;
    ppnt: ppolyarr; { .ofs portion of lgl_addr }
    from_select_all: Boolean;
  end;

  state = packed record { default type things for lines }
    { Max size of record is 64 bytes }
    color: aSInt; { 4 }
    linetype: aSInt; { 8 }
    Width: aSInt; { 12 }
    attr: aSInt; { 16 }
    factor: aFloat; { 24 }
    ovrshut: aFloat; { 32 }
    winlinetype: byte; { 33 }
    winlinefactor: aFloat; { 41 }
    o2cmaterialindex: word; { 43 }
    visLvl: byte; { 44 } { 0 = all views; 1 = 3D Only; 2 = 2D Only. Used in smart entities }
    unused: array [1 .. 20] of byte; { Makes 64 bytes }
  end;

  pstate = ^state;

  sym_type = packed record
    addr: symaddr;
    addrSpc: AdsMem;
    name: symstr; { file name for this symbol }
    symnameaddr: lgl_addr; { address where we will store the symbol name }
    frstent, { first entity in this symbol }
    lastent, { last entity in this symbol }
    frstatr, { first database info }
    lastatr, o2cmaterials: lgl_addr; { last database info }
    min, max: dcPoint; { extents }
    num: aSInt; { number of instances of this symbol at last call to sym_count }
    refFlag: Boolean; { set by sym_ref }
    sym_fill1: Boolean;
    cnt: Longint; { number of entities in this symbol }
    serialno: aSInt; { set by sym_ref }
    KnockOut: byte;
    case aSInt of
      1: (Next: symaddr; { next symbol header }
           prev: symaddr;); { previous symbol header }
      2: (frstsym: symaddr; { first symbol used in symbol }
           lastsym: symaddr;); { last symbol used in symbol }
  end;

  symbol = sym_type;
  psym_type = ^sym_type;

  atrName = string[atr_name_len + 1];

  db_article = packed record
    db_name: str8;
    db_extension: str8;
    db_type: aSInt;
    art_type: aSInt;
    art_name: str80;
  end;

  AreaPerimType = packed record
    txtpnt: dcPoint;
    txttype: RTextType;
    txtsiz, ang, slant, aspect: aFloat;
    txtfont: fontstr;
    dispttf: TDisplayTTF;
    TxtWinfont: TFontType;
    fontNum: aSInt;
    txtunderline, txtoverline, ShowBoundary, AtCentroid, DoArea, DoPerim: Boolean;
  end;

  TAttribXData = packed record
    { This size of record not to exceed 56 bytes }
    case byte of
      0: (Data: array [0 .. 55] of byte);
      1: (positn: dcPoint;
           txtsize: aFloat;
           txtang: aFloat;
           txtslant: aFloat;
           txtaspect: aFloat;
        );
      2: (xrefhilitecolor: aSInt;
           xrefhiliteltype: aSInt;
           xrefhilitespacing: aFloat;
        );
      3: (arrotype: aSInt;
           ArrowTextSize: aFloat;
           ArroWeight: aSInt;
           ArroColor: aSInt;
           ArroSize: aFloat;
           ArroRatio: aFloat;
           FilledArrow: Boolean;
           scale: aFloat;
           AtHead: Boolean;
           unused: array [0 .. 9] of byte;
        );
  end;

  attrib = packed record
    atrtype: aSInt;
    addr: atraddr;
    addrSpc: AdsMem;
    Next: atraddr;
    prev: atraddr;
    name: atrName;
    Visible: Boolean; { either data56 or visible is true but not both }
    data56: Boolean; { either data56 or visible is true but not both }
    SPBvisible: Boolean; // Solid pattern bitmap fill boundary visible
    VarDataSize: byte; // max space for str255 is 255
    txtcolor: aSInt;
    xdata: TAttribXData;
    case aSInt of
      atr_str: (str: str80);
      atr_int: (int: aSInt);
      atr_rl: (rl: aFloat);
      atr_dis: (dis: aFloat);
      atr_ang: (ang: aFloat);
      atr_pnt: (pnt: dcPoint);
      atr_addr: (lgladdr: lgl_addr);
      atr_db: (db: db_article);
      atr_str255: (shstr: str255);
      atr_AreaPerim: (AreaPerim: AreaPerimType);
  end;

  scanLineType = packed record
    { describes a hatch scan line }
    ang: aFloat;
    org: dcPoint;
    delta: dcPoint;
    numdash: byte;
    dash: array [0 .. maxDash - 1] of aFloat;
    dashDraw: array [0 .. maxDash - 1] of Boolean;
    { Do not change the alignments of these fields because they match
      ScanlineAttrib record, which is not defined anymore }
    isfirst: Boolean;
  end;

  HatchDefAttrib = packed record
    islandtype: byte;
    hatchmode: byte;
    outlinevisible: Boolean;
    namelen: byte; { 4 }
    ang: aFloat; { 12 }
    scale: aFloat; { 20 }
    org: dcPoint; { 44 }
    name: str255;
  end;

  VNameStr = str80;

  view_type = packed record
    addr: lgl_addr; { Address of stored view }
    addrSpc: AdsMem; { Current drawing file }
    Next: lgl_addr; { Next view in list if any }
    prev: lgl_addr; { Previous view in list if any }
    viewmode: aSInt; { Type of projection }
    vmat: modmat; { Viewing xform matrix }
    cmat: modmat; { Post-clipping xform matrix }
    emat: modmat; { Editing matrix }
    imat: modmat; { Inverse of editing matrix }
    prspct: Boolean; { True if perspective mode }
    clipon: Boolean; { True if clipping cubes on }
    name_old: string[9]; { Short Name }
    clpmin: dcPoint; { Clipping cube minimum }
    clpmax: dcPoint; { Clipping cube maximum }
    xr: aFloat; { Window/viewport x position }
    yr: aFloat; { Window/viewport y position }
    scale: aFloat; { Window/viewport scale }
    scalei: aSInt; { View drawing scale }
    perscent: dcPoint; { Perspective center of view }
    perseye: dcPoint; { Perspective eyepoint }
    persdis: aFloat; { Perspective distance }
    togglelyr: Boolean; { True of toggling of layers on }
    frstlyr: lgl_addr; { Address of first entry in layer list }
    lastlyr: lgl_addr; { Address of last entry in layer list }
    currlyr: lgl_addr; { Address of current layer for this view }
    flag1: byte; { 0 = GoTo View, 1 = MSP Detail }
    flag2: byte; { MSP Sheet Number (0 + 1) through (255 + 1) }
    coneang: aFloat; { Cone of Vision }
    frstatr, { Address First Attribute }
    lastatr: lgl_addr; { Address Last Attribute }
    clipNoZ: Boolean; { Ignore Z }
    name: VNameStr; { Long Name }
    DoZoom: Boolean; { Zoom Extents }
  end;

  list_type = packed record
    addrSpc: AdsMem;
    addr, Next, prev: lgl_addr;
    Data: array [1 .. 6] of lgl_addr;
  end;

  TBrowseType = (sym_none, sym_folder, sym_drawing, sym_template);

  TFillProp = packed record
    FillClr,
    PatternClr: TDCColor;
    FillPattern: SmallInt; { only using 6 currently }
    doBMP,
    doEntFillClr,
    doEntPatternClr,
    imgFixAspect: Boolean;
    { The following are not included in legacy attribute string }
    { New SPB-Fill }
    ImageName: str255;
    FillOpacity: byte;
    ImageOpacity: byte;
    ImgOpaMethod: byte; { 0 = From Image, 1 = Custom, 2 = Ignore }
    FillClrIdx: byte; { 0 = Custom or Index }
    PattClrIdx: byte; { 0 = Custom or Index }
    TransClr: TDCColor; // Integer;
    TransClrIdx: byte; { 0 = Custom or Index }
    TransClrTol: byte; { 0 - 255 }
  end;

  Ro2cView = packed record // 164 out of 256 Bytes Max
    Xrot, Zrot, Zoom, VRPx, VRPy, VRPz, EyeX, EyeY, EyeZ, ViewAngle: single;
    KeepSpinning, Walkthrough, Filter, isParallel: wordbool;
    Lights: integer;
    ViewName: Str80;
    BackColor: integer;
    BackPicMode: TOleEnum;
    BackPicOffsetX, BackPicOffsetY: integer;
    AtrBackPicURL: atrname;
    SchemeName: Str80;
  end;

  LineTypeDefinition = packed record
    lineId: aSInt;
    numSegs: word;
    endCorrection: word;
    factor: aFloat;
    name: str255; { Making it string and using FillChar will set Name field to null and
      can cause crash, because strings are handled as pointers }
    delta: array [0 .. maxpoly - 1] of Point2D;
  end;

  LineTypeDefinitionArray = array [0 .. MaxLns - 1] of LineTypeDefinition;

  TBpSavedSettings = packed record
    IgnoreChecked: Boolean; { ASV and File In-Use }
    fPdfLayersOptionIndex: integer; { 0: None, 1: Drawing, 2: MSP }
    SuppressChecked: Boolean; { Skip Screen Redraws }
    LogToFile: Boolean; { BatchPlot.log }
    NumCopiesValue: integer; { In memory only, not in drawing or SET file }
    fPlotToFileFormatRadio: integer; { 0: Device, 1: PDF, 2: PLT }
    fPlotToFile: Boolean; { True: File (PDF or PLT) False: Device }
    fPdfMergeChecked: Boolean; { Merge PDFs }
    fAlphaSort: integer; { 0: Original, 1: Alpha Sort - Not Used }
    fReverseSort: Boolean; { Reverse Sort }
    fIncludeQuick: Boolean; { Include Quick Layout }
    fIncludeGoToViews: Boolean; { Include GoTo Views }
    fIncludeMSPSheets: Boolean; { Include MSP Sheets }
    fPdfMergeFileName: Str80; { Merged PDFs Filename }
    fCollate: Boolean; { Print 1, 2, 3..., 1, 2, 3... }
    Unused: array [109 .. 255] of Byte; { 255 Bytes Max. }
  end;

  pipe_type = packed record
    Critical_DoNot_Change_Use: AdrMem; // FnDrawLines: TDrawLnX;
    Data: AdrMem; { address of user data }
    SnapData: AdrMem; { Only for snap near data pointer }
    pclip_Delphi_Class: AdrMem; // pclip: Tgpc_polygon_Object;{see declaration}
    doXrefxclip: Boolean;
    doCliprectangle: Boolean;
    { USAGE }
    { lin pnt ply }
    { x   x   x } viewmode: aSInt; { current viewing projection }
    { x   x   x } Symlevel: aSInt; { symbol nesting level }
    { x   x   x } Modlevel: aSInt; { modeling matrix concat level }
    { x   x   x } Maxsymnest: aSInt; { max level of symbol nesting }
    { x   -   - } Drag: aSInt; { dragging case, 0 if none }
    { x   x   x } Primdivs: aSInt; { primary circle divsions }
    { x   x   x } Scnddivs: aSInt; { secondary circle divisions }
    { x   x   x } Mindivs: aSInt; { minimum circle divisions }
    { x   x   x } maxpoly: aSInt; { max circle divs and polygon sides }
    { x   -   - } Boxsymsize: aSInt; { min size of sym boxes in pixels }
    { x   -   - } Boxtxtsize: aSInt; { min size of text boxes in pixels }
    { x   -   - } TestClr: bset256; { plotter pen color comparison value }
    { x   x   x } Brk: aSInt; { nonzero of broken using end or del keys }
    { x   -   - } Txtboxclr: aSInt; { Color of text boxes }
    Dropmeshflag: Boolean;
    XrefHilite: Boolean;
    XrefColor: aSInt;
    XrefLType: aSInt;
    // Unusedi        : ARRAY [0..1] OF aSInt;
    { x   x   x } doGeom: Boolean; { do entity line (pnt) geometry }
    { x   x   x } Chkbrk: Boolean; { check for brkprss }
    { x   -   x } doisln: Boolean; { check isln flags polygon/slabs }
    { x   x   - } doMark: Boolean; { draw node markers }
    { x   x   x } doSym: Boolean; { generate symbol geometry }
    { x   x   - } doSymins: Boolean; { generate symbol insertion points }
    { x   -   - } doBezgrd: Boolean; { generate bezsurf grid }
    { x   x   - } doBezpnt: Boolean; { generate bezsurf control markers }
    { x   x   - } doAtr: Boolean; { generate visible attributes }
    { x   -   - } doLinwgt: Boolean; { generate line weights }
    { x   -   - } doOvrshut: Boolean; { generate line overshoots }
    { x   -   - } doLintype: Boolean; { generate line types }
    { x   -   - } doUsrline: Boolean; { generate user line types }
    { x   x   x } doClip: Boolean; { true if 3d clipping cubes on }
    { x   x   - } doMidmrk: Boolean; { generate mid line points (snap) }
    { x   -   - } doBoxtext: Boolean; { generate boxes only for text }
    { x   x   - } doBoxsyms: Boolean; { generate boxes only for symbols }
    { x   -   - } doSlabref: Boolean; { generate slab reference face markers }
    { x   x   x } doDivover: Boolean; { use prim/scnd divs, not ent.xxxdivs }
    { x   -   - } doBezgeom: Boolean; { generate bezsurf geometry }
    { x   x   x } do3dgeom: Boolean; { generate geom for 3d circular entities }
    { x   x   - } doCornpnt: Boolean; { generate 3d circ surf corner points }
    { x   x   - } doDimension: Boolean; { draw associative dimensions }
    { x   x   x } doHatch: Boolean; { draw hatch lines }
    { x   -   - } doCheckClr: Boolean; { check pen color during plotting }
    { x   -   - } ToPlotter: Boolean; { plotter in use }
    { x   -   - } ToClipboard: Boolean; { send to windows clipboard }
    { x   -   - } Clipboardpenwidth: aSInt; { pen width to use when drawing to clipboard }
    { x   -   - } Clipboardmultistroke: Boolean; { do multi-weight lines get multi-stroked on clipboard? }
    { -   -   x } doHide: Boolean; { true if constructing HLR database }
    { -   -   x } doArcgon2: Boolean; { true close 2D arc in HLR }
    { -   -   x } doEllgon: Boolean; { true close ellipses in HLR }
    { x   -   - } Usepipefast: Boolean; { true use pipe_fast if applicable }
    { x   x   - } doDrawpnt: Boolean; { used only for draw_pnt }
    { x   -   - } do2doptim: Boolean; { Optimize 2d line and arc in ortho }
    { x   -   - } doHilite: Boolean; { True if hilighting this entity }
    { -   x   - } doBulgepnt: Boolean; { True if dcPoint around bulge are created }
    { x   x   - } doDimmpts: Boolean; { draw dimm markers }
    do2dcrvctr: Boolean;
    For_Snap: Boolean;
    { x   x   x } Undo_Record: Boolean; { allow undo buffer to record draw event }
    // DoPlotStamp    : boolean;  { Text to be searched for plot stamp tokens?}
    { Never used because PlotStamp class takes care of it }
    // unusedb2       : boolean; replaced with undo_record see up
    InXref: Boolean;
    Fill_Loop_First_Pt: Boolean; // First dcPoint on 'loop' of filled polygon/polyline or void in same

    { -   -   - } XrefFactor: aFloat;
    { -   -   - } XrefSpacing: aFloat;
    { x   x   x } CrcFact: aFloat; { 2d arc, circle, ellipse factor }
    { x   x   x } Pix: aFloat; { size of a pixel }
    { x   x   x } StepSize: aFloat; { size of a line width step (pixel) }
    { x   x   - } TextLf: aFloat; { size of a line feed for text }
    { x   x   x } Hither: aFloat; { distance to hither clipping plane }

    { x   x   x } Windmin, { lower left extents of clipping window }
    { x   x   x } Windmax: dcPoint; { upper right extents of clipping window }
    { x   x   x } Vportmin, { lower left of view port }
    { x   x   x } Vportmax: dcPoint; { upper right of view port }
    { x   x   x } Clipmin, { lower left of 3d clipping cube }
    { x   x   x } Clipmax: dcPoint; { upper right of 3d clippint cube }

    { x   x   x } Postmat: modmat { mat4x4 }; { post clipping cube tran matrix }
    For_Layout: Boolean;
    Morethan4txtpoints: Boolean;
    GlobalOvershoot: Boolean;
    Overshoot: aFloat;
    Entitychkbrk: Boolean;
    EntityBrkCount: longword;
    doShowHatchOrigin: Boolean;
    FixText: byte;
    clipNoZ: Boolean;
    Multistrokepens: Boolean;
    Snap_type: aSInt;
    Forcsg: Boolean;
    Pvert_hidden: Boolean;
    Allow4ptpolygon: Boolean; { allow4 pt polygons for hlr and o2c }
    For_MSPBind: Boolean;
    ScaleIndOvershoot: Boolean;
    Is_Chars: Boolean;
    Symbol_Draw_Fill: Boolean;
    Check_Symbol_Draw: Boolean;
    LockTextAngle: byte;
    ForHide: Boolean;
    InSymb: Boolean;
    For_Browser: Boolean;
    Full3d: Boolean;
    ForceUndraw: Boolean;
    For_Export: Boolean;
    For_saveas: Boolean;
    For_XClip: Boolean;
    Plyr: plgl_addr { player };
    ScaleDependent: byte;
    Scal: dcPoint;
    Tran: dcPoint;
    KnockOut: byte;
    SymNestLevel: aSInt;
    XrefNestLevel: aSInt;
    WallHatchOff,
       WallFillOff: Boolean;
    // Bug #01918
    SymbolFactor: aFloat;
    NewFactor: byte;
    TopLevelNotScaleDependent: Boolean;
    ParentSym: entity;
    For_Select: Boolean;
    doNesting: Boolean;
  end;

  EntOutProc = procedure(Ent: entity; var p: pipe_type; var xForm: modmat); stdcall;

  TBytes = array of byte;

implementation

uses
  uInterfaces;

constructor dcPoint3D.Create(Pt: dcPoint);
begin
  SetWorldPt(Pt);
end; { dcPoint3D.Create }

procedure dcPoint3D.SetWorldPt(const Val: dcPoint);
begin
  if not Self.SamePoint(Val, fWorldPt, 1.0E-12) then begin
    fWorldPt := Val;
    fDrawPt := WorldPointToDrawPoint(fWorldPt);
  end;
end; { dcPoint3D.SetWorldPt }

procedure dcPoint3D.SetWorldPtX(const Val: aFloat);
begin
  if not Self.SameValue(fWorldPt.x, Val, 1.0E-12) then begin
    fWorldPt.x := Val;
    fDrawPt := WorldPointToDrawPoint(fWorldPt);
  end;
end; { dcPoint3D.SetWorldPtX }

procedure dcPoint3D.SetWorldPtY(const Val: aFloat);
begin
  if not Self.SameValue(fWorldPt.y, Val, 1.0E-12) then begin
    fWorldPt.y := Val;
    fDrawPt := WorldPointToDrawPoint(fWorldPt);
  end;
end; { dcPoint3D.SetWorldPtY }

procedure dcPoint3D.SetWorldPtZ(const Val: aFloat);
begin
  if not Self.SameValue(fWorldPt.z, Val, 1.0E-12) then begin
    fWorldPt.z := Val;
    fDrawPt := WorldPointToDrawPoint(fWorldPt);
  end;
end; { dcPoint3D.SetWorldPtZ }

function dcPoint3D.SameValue(Val, TestVal: TFloat; Epsilon: TFloat): Boolean;
begin
  Result := (Abs(Val - TestVal) <= Epsilon);
end; { dcPoint3D.SameValue }

function dcPoint3D.SamePoint(Pt, TestPt: dcPoint; Epsilon: TFloat; IgnoreZ: Boolean = False): Boolean;
begin
  if IgnoreZ then begin
    Pt.z := 0.0 { cZero };
    TestPt.z := 0.0 { cZero };
  end;
  Result := (Abs(Pt.x - TestPt.x) <= Epsilon);
  if Result then Result := (Abs(Pt.y - TestPt.y) <= Epsilon);
  if Result then Result := (Abs(Pt.z - TestPt.z) <= Epsilon);
end; { dcPoint3D.SamePoint }

constructor dcPickPt.Create(Pt: dcPoint);
{ World Coordinate of Cursor or dcPoint returned from GetPoint (not GetPoint3) }
begin
  SetWorldPt(Pt);
end; { dcPoint3D.Create }

procedure dcPickPt.SetWorldPt(const Val: dcPoint);
begin
  if not Self.SamePoint(Val, fWorldPt, 1.0E-12) then begin
    fWorldPt := Val;
    CalculateDrawCoordinate(fWorldPt, fDrawPt);
  end;
end; { dcPoint3D.SetWorldPt }

procedure dcPickPt.SetWorldPtX(const Val: aFloat);
begin
  if not Self.SameValue(fWorldPt.x, Val, 1.0E-12) then begin
    fWorldPt.x := Val;
    CalculateDrawCoordinate(fWorldPt, fDrawPt);
  end;
end; { dcPoint3D.SetWorldPtX }

procedure dcPickPt.SetWorldPtY(const Val: aFloat);
begin
  if not Self.SameValue(fWorldPt.y, Val, 1.0E-12) then begin
    fWorldPt.y := Val;
    CalculateDrawCoordinate(fWorldPt, fDrawPt);
  end;
end; { dcPoint3D.SetWorldPtY }

procedure dcPickPt.SetWorldPtZ(const Val: aFloat);
begin
  if not Self.SameValue(fWorldPt.z, Val, 1.0E-12) then begin
    fWorldPt.z := Val;
    CalculateDrawCoordinate(fWorldPt, fDrawPt);
  end;
end; { dcPoint3D.SetWorldPtZ }

function dcPickPt.SameValue(Val, TestVal: TFloat; Epsilon: TFloat): Boolean;
begin
  Result := (Abs(Val - TestVal) <= Epsilon);
end; { dcPoint3D.SameValue }

function dcPickPt.SamePoint(Pt, TestPt: dcPoint; Epsilon: TFloat; IgnoreZ: Boolean = False): Boolean;
begin
  if IgnoreZ then begin
    Pt.z := 0.0 { cZero };
    TestPt.z := 0.0 { cZero };
  end;
  Result := (Abs(Pt.x - TestPt.x) <= Epsilon);
  if Result then Result := (Abs(Pt.y - TestPt.y) <= Epsilon);
  if Result then Result := (Abs(Pt.z - TestPt.z) <= Epsilon);
end; { dcPoint3D.SamePoint }

end.
