unit UConstants;

interface

const
   sw_size = 8;
   x = 0;
   y = 1;
   z = 2;
   Xdone = 0;
   MaxLabelChar = 12;
   drmode_white = 1; { draw mode for draw }
   drmode_black = 0; { draw mode for erase }
   drmode_flip = -1; { draw mode for XOR }
   drmode_null = -2; { no output }
   hilite_ss = 10; { selection set used for hilited items }
   snap_ss = 11; { selection set used for object snap }
   mouse_Ctrl_right_click_ss = 14;
   bufSize = 128;
   maxMaskents = 36;

   clrwhite = 1;
   clrred = 2;
   clrgrn = 3;
   clrblue = 4;
   clrcyan = 5;
   clrmgta = 6;
   clrbrown = 7;
   clrltgray = 8;
   clrdkgray = 9;
   clrltred = 10;
   clrltgrn = 11;
   clrltblue = 12;
   clrltcyan = 13;
   clrltmgta = 14;
   clryellow = 15;

   linked = 0;
   sngle = 1;
   area = 2;
   fenced = 3;

   lyr_name_max = 8;
   sym_len = 255; { length of a symbol name }
   ssMax = 16; { number of selection sets }
   max_spline = 20; { max control points in a spline curve }
   charlen = 255; { max string entity length }
   maxpoly = 256; { number of points in a polygon }
   maxaddr = 36; { number of addresses in a property }

   ldrpts = 9;

   ltype_solid = 1; { line types }
   ltype_dotted = 2;
   ltype_dashed = 3;
   ltype_dotdash = 4;

   lyr_curr = 0;
   lyr_on = 1;
   lyr_all = 2;

   entlin = 1;
   entcrc = 2;
   entbez = 3;
   entarc = 4;
   entmrk = 5;
   entell = 6;
   enttxt = 7;
   entln3 = 8;
   entbsp = 9;
   entdim = 10;
   entsym = 11;
   entxref = 12;
   entar3 = 13;
   entply = 14;
   entblkattdef = 15;
   entcyl = 16;
   entcon = 17;
   entcnt = 18;
   enttor = 19;
   entdom = 20;
   entsrf = 21;
   entblk = 22;
   entslb = 23;
   enttrn = 24;
   entpln = 25;
   entrev = 26;
   UnusedEntity1 = 27;
   enthol = 28;
   entshl = 29;
   SuperEntFirst = 30; // Polymorphic Entitiy Object
   DCNullObject = SuperEntFirst;
   DCPlainTextObject = SuperEntFirst + 1;
   DCRTFTextObject = SuperEntFirst + 2;
   DCXLSObject = SuperEntFirst + 3;
   DCWallObject = SuperEntFirst + 4;
   DCDoorObject = SuperEntFirst + 5;
   DCWindowObject = SuperEntFirst + 6;
   DCCutOutObject = SuperEntFirst + 7;
   DCUnusedObject1 = SuperEntFirst + 8;
   DCUnusedObject2 = SuperEntFirst + 9;
   DCUnusedObject3 = SuperEntFirst + 10;
   DCUnusedObject4 = SuperEntFirst + 11;
   DCUnusedObject5 = SuperEntFirst + 12;
   DCUnusedObject6 = SuperEntFirst + 13;
   DCUnusedObject7 = SuperEntFirst + 14;
   DCUnusedObject8 = SuperEntFirst + 15;
   SuperEntLast = DCUnusedObject8;

   firstent = entlin;
   lastent = SuperEntLast;

   // TEXT_GEN_BACKWARD=2;
   // TEXT_GEN_UPSIDEDOWN=4;
   TEXT_JUST_LEFT = 0;
   TEXT_JUST_CENTER = 1;
   TEXT_JUST_RIGHT = 2;
   TEXT_JUST_ALIGNED = 3;
   TEXT_JUST_MIDDLE = 4;
   TEXT_JUST_FIT = 5;
   TEXT_VALIGN_BASELINE = 0;
   TEXT_VALIGN_BOTTOM = 1;
   TEXT_VALIGN_MIDDLE = 2;
   TEXT_VALIGN_TOP = 3;

   f1 = 256 + 59;
   f2 = 256 + 60;
   f3 = 256 + 61;
   f4 = 256 + 62;
   f5 = 256 + 63;
   f6 = 256 + 64;
   f7 = 256 + 65;
   f8 = 256 + 66;
   f9 = 256 + 67;
   f0 = 256 + 68;
   s1 = 256 + 84;
   s2 = 256 + 85;
   s3 = 256 + 86;
   s4 = 256 + 87;
   s5 = 256 + 88;
   s6 = 256 + 89;
   s7 = 256 + 90;
   s8 = 256 + 91;
   s9 = 256 + 92;
   s0 = 256 + 93;

   ex = 20;
   exitkey = 233;

   zero = 0.0;
   one = 1.0;
   two = 2.0;
   three = 3.0;
   four = 4.0;
   ten = 10.0;
   thirtytwo = 32.0;
   onehalf = 0.5;
   threeqtrs = 0.75;
   hundred = 100.0;
   onefoot = 384.0;
   metricconv = 0.79375; { convert 1/32's to mm }
   ratio = 1.7;
   ratio6 = 0.6;
   ratio8 = 0.8;
   isoangle = 0.955316618; { 54.7356 degrees }
   abszero = 1.0E-12;
   sqrzero = abszero * abszero;
   sp_pi = 3.14159265359;
   pi = 3.1415926535897932384626433832795;
   pi2 = two * pi;
   twopi = pi2;
   halfpi = pi * onehalf;
   pi32 = pi * three * onehalf;
   // sqrt2     = sqrt(two);
   // sqrt3     = sqrt(three);
   dzero = zero;
   dabszero = abszero * abszero;
   dsqrzero = dabszero;
   // ln_ten    = ln(ten);

   pathcad = 0; { }
   pathtpl = 1; { *.tpl }
   pathsym = 2; { *.dsf, *.sm3 }
   pathfrm = 3; { *.frm }
   pathmcr = 4; { *.dcx }
   pathchr = 5; { *.chr, *.shx }
   pathsup = 6; { dcadwin - .lbl .msg .dca .mcr }
   pathdrv = 7; { }
   pathlyr = 8; { *.dlf, *.lyr }
   pathout = 9; { *.txt, *.plt }
   pathtmp = 10; { Temporary files }
   pathswp = 11; { Swap files }
   pathdef = 12; { dcadwin - .dis .scl .ang .dec }
   pathdxf = 13; { DXF files }
   pathdfd = 14; { default drawing }
   pathdls = 15; { }
   pathnet = 16; { }
   pathprj = 17; { }
   pathfontpof = 18; { }
   pathhatchpof = 19; { }
   pathlinepof = 20; { }
   pathPOF = 21; { }
   pathINI = 22; { *.pal }
   pathLPF = 23; { }
   pathCOL = 24; { }
   PathASC = 25; { }
   PathJNL = 26; { }
   PathSession = 27;
   PathBmp = 28;
   Pathhelp = 29;
   Pathinstall = 30; { Where DataCAD is installed }
   Pathlangg = 31;
   Pathgray = 32;
   Pathshaderbmp = 33;
   Patho2c = 34;
   Patho2cmaterial = 35;
   Patho2ctexture = 36;
   Pathsymins = 37;
   Pathxref = 38;
   Pathpen = 39;
   PathSymBrowserUnused = 40;
   Pathestdb = 41;
   Pathestext = 42;
   Pathtag = 43;
   Pathdbf = 44;
   PathInsertMetafile = 45;
   PathSaveAsPDF = 46;
   PathRecovery = 47;
   Patho2cimporttexture = 48;
   Pathlinedef = 49;
   Pathhatchdef = 50;
   Pathdatabases = 51;
   PathSketchUp = 52;
   Pathtxtstyles = 54;
   Pathxls = 55;
   Pathwallstyles = 56;
   Pathdoorstyles = 57;
   Pathwindowstyles = 58;
   Patho2cmaterialBasepath = 59;
   PathSmartDoorSymbol = 60;
   PathSymbolBasePath = 61;
   Patho2cTextureBasePath = 62;
   PathStylesBasePath = 63;
   PathXRefBasePath = 64;
   Pathclipboardfiles = 65;
   PathSmartWindowSymbol = 66;
   PathPlotterSettings = 67;
   PathDictionary = 68;
   PathRGB = 69;
   PathDimStyle = 70;
   PathMsgLblMnu = 71;
   PathSunShader =72;

   atr_name_len = 12;
   atr_int = 0;
   atr_rl = 1;
   atr_str = 2;
   atr_dis = 3;
   atr_ang = 4;
   atr_pnt = 5;
   atr_addr = 6; { Should not be used }
   atr_db = 7;
   atr_str255 = 8;
   atr_AreaPerim = 9;

   vmode_orth = 0;
   vmode_para = 1;
   vmode_oblq = 2;
   vmode_pers = 3;
   vmode_edit = 4;
   vmode_all = 5;
   oblqplan = 0;
   oblqelev = 1;

   pv_vert = 0;
   pv_bulge = 1;

   fl_ok = 0;

   htype_normal = 1;
   htype_outer = 2;
   htype_ignore = 3;
   MaxDash = 6;

   wall_none = 0;
   wall_clip = 1;
   wall_cap = 2;
   wall_on = 1;
   wall_off = 0;

   osnap_nearest = 1;
   osnap_endpoint = 2;
   osnap_midpoint = 4;
   osnap_center = 8;
   osnap_quad = 32;
   osnap_intsect = 64;
   osnap_perp = 256;
   osnap_tan = 512;
   osnap_quick = 1024;
   osnap_npoint = 2048;
   osnap_virtualsnap = 4096;

   XDcalStateBegin = 8100;
   XDcalStateEnd = 8200;

implementation

end.
