unit UInterfacesRecords;

interface

uses uRecords;

type
  answerAArg = record
    answer: aSInt; { out }
    { no = 0, 1 = yes, 2 = abort }
  end;

  viewArg = packed record
    num: aSInt; { in }
  end;

  getscaleArg = packed record
    pint: ^aSInt; { pinteger }   { out }
    getout: boolean; { out }
  end;

  globescArg = packed record
    key: aSInt; { in }
  end;

  getPlinArg = packed record
    msg: aSInt; { in }   { message to print, 0 if none }
    drawit: boolean; { in }   { draw while creating and when leaving }
    doclosd: boolean; { in }   { give "Closed" option? }

    init: pboolean; { in out }  { initialize "frst" and "last"? }
    isclosd: pboolean; { in out }  { returns true if created closed polyline }
    frst, last: plgl_addr; { in out }

    result: crtstat; { out }  { normal : got a valid polyline }
    { escape : Fn key f6-s6 was pressed }
    { error  : error in getting last vertex }
    dogesc: boolean;
    dovoid: boolean;
    key: aSInt; { out }
    fkey: aSInt; { out }
    vmode: aSInt; { in }   { allowable view mode }
    Pmsg: ^str80;
    contsrch: boolean;
    docover: pboolean;
    dorect: pbyte;
    EnableCurvesOption: boolean;
    addr: lgl_addr;
    chk_addr: boolean;
    reset_undo: boolean;
  end;

  getcharArg = packed record
    key: aSInt; { out }
  end;

  getpointArg = packed record
    result: crtstat; { out }
    curs: point; { out }
    key: aSInt; { out - if result = escape, the key that was pressed }
    fkey: aSInt; { out, the function key number }
    from3d: boolean; { in, were we called from 3d }
    curs3d: boolean; { in }
    snapped: boolean;
    dogesc: boolean;
    viewmode: aSInt;
    curs3: point;
    PickingSymbols: boolean;
    returnongesc: boolean;
  end;

  DgetdisArg = packed record
    pdis: paFloat; { in out }
    unusedlen: aSInt; { in }
    result: crtstat; { out }
    key: aSInt; { out, set if resut = escape }
  end;

  getdisArg = packed record
    pdis: paFloat; { in out }
    msg: aSInt; { in }
    unusedlen: aSInt; { in }
    toabs: boolean; { in }
  end;

  DgetintArg = packed record
    pint: ^aSInt; // pinteger;   { in out }
    len: aSInt; { in }
    result: crtstat; { out }
    key: aSInt; { out, set if result = escape }
  end;

  getintArg = packed record
    pint: paSInt; { in out }
    len: aSInt; { in }
    msg: aSInt; { in }
    toabs: boolean; { in }
  end;

  DgetangArg = packed record
    pang: paFloat; { in out }
    len: aSInt; { in }
    result: crtstat; { out }
    key: aSInt; { out, set if resut = escape }
    rel: boolean; { in }
  end;

  getangArg = packed record
    pang: paFloat; { in out }
    msg: aSInt; { in }
    len: aSInt; { in }
    rel: boolean; { in }
  end;

  getclrArg = packed record
    pclr: paSInt; { in out }
    msg: aSInt; { in }
    CanReturnZero: boolean;
  end;

  getescArg = packed record
    msgs: boolean; { in }
    key: aSInt; { out }
  end;

  DgetstrArg = packed record
    pstr: Pointer; // pstr132;    { in out }
    len: aSInt; { in }
    result: crtstat; { out }
    key: aSInt; { out, set if result = escape }
  end;

  { getstring }
  getstrArg = packed record
    pstr: Pointer; // pstr132;    { in out }
    len: aSInt; { in }
  end;

  DgetrealArg = packed record
    prl: paFloat; { in out }
    len: aSInt; { in }
    result: crtstat; { out }
    key: aSInt; { out, set if result = escape }
  end;

  getrealArg = packed record
    prl: paFloat; { in out }
    len: aSInt; { in }
    msg: aSInt; { in }
    toabs: boolean; { in }
  end;

  gettxtsizArg = packed record
    psiz: paFloat; { in out }
    msg: aSInt; { in }
    useplot: boolean; { in }
  end;

  getflname1Arg = packed record
    MaxStringLen: aSInt;
    NameTooLongErr: boolean;
    pstr: ^str255; { in out }
    path: ^str255; { in out }
    suffix: str255; { in }
    prompt: aSInt; { in }
    names: boolean; { in }
    chkit: boolean; { in }
    entire: ^str255; { out }
    addext: boolean; { in }
    result: crtstat; { out }
    key: aSInt; { out }
    lbl: aSInt; { in }
    FilterIndex: aSInt; { out } // Bill D'Amico 3/5/99
  end;

  TArrSuffix = array [1 .. 10] of aSInt;

  getflnameArg = packed record
    { IMPORTANT!!! - The presumption here is that both pstr and entire
      point to string variables of the same size, and that both of those
      string variables can hold at least MaxStringLen characters (not counting
      the first length byte)
    }
    MaxStringLen: aSInt;
    NameTooLongErr: boolean;
    pstr: Pointer; // ^str80;        { in out }
    path: aSInt; { in out }
    suffix: aSInt; { in }
    prompt: aSInt; { in }
    names: boolean; { in }
    chkit: boolean; { in }
    entire: Pointer; // ^str80;        { out }
    addext: boolean; { in }
    result: crtstat; { out }
    key: aSInt; { out }
    lbl: aSInt; { in }
    FilterIndex: aSInt; { out } // Bill D'Amico 3/5/99
    numsuffix: aSInt; { in }
    ArrSuffix: TArrSuffix;
    { in } // Devinder for multiple values in dialog box drop down
  end;

  TArcArg = record
    result: crtstat; { out }
    key: aSInt; { out }
    init: pboolean; { in out }
    state: aSInt; { in out }
    center: point; { in out ~^ }
    begang, { in out }
    endang, { in out }
    radius: aFloat; { in out  ~p }
    msg: str80; { in      ~useless }
  end;

  getpolyArg = packed record
    result: crtstat; { out }
    init: pboolean; { in out }
    key: aSInt; { out }
    msgno: aSInt; { in }
    msg: str80; { in }
    npnts: aSInt; { in out }
    pnt: polyarr; { out }
  end;

  getgarcArg = record
    result: crtstat; { out }
    key: aSInt; { out }
    shape: aSInt; { in }
    init: pboolean; { in out }
    state: aSInt; { in out }
    pnt1, { in out }
    pnt2, { in out }
    pnt3, center: point; { in out }
    begang, { in out }
    endang, { in out }
    radius: aFloat; { in out }
    msg: str80; { in }
    menu: boolean end;

    CircOptions = record DoPolyline: boolean;
    pDoPlineCover, pDoPlineClosed: pboolean;
  end;

  CircMenuArg = record
    CircOpts: CircOptions;
  end;

  TextMenuArg = packed record
    blkattrib: boolean;
  end;

  MoveDragArg = packed record
    ForceAndCopy: boolean;
    msg1: aSInt;
    msg2: aSInt;
  end;

  SelSetsMenuArg = record
    ForClipboardSelect: boolean;
  end;

  TLyrArg = packed record
    lyraddr: lgl_addr;
  end;

  TFLyrArg = packed record
    lyraddr: lgl_addr; { out }
    offset: aSInt; { in out }
    key: aSInt; { out }
    result: aSInt; { out }
  end;

  TShadeArg = packed record
    lyr: lgl_addr;
    view: view_type;
    mode: mode_type;
    LightType: aSInt;
    EdgeType: aSInt;
    BackFacing: boolean;
    GouraudOn: boolean;
    retval: boolean;
  end;

  TPolyArg = packed record
    result: crtstat; { out }
    init: boolean; { in out }
    key: aSInt; { out }
    msgno: aSInt;
    msg: str80; { in }
    npnts: aSInt; { in out }
    pnt: polyarr; { out }
  end;

  TCutOutArg = packed record
    pt1, pt2, pt3, pt4: point;
    addr1, addr2, addr3, addr4, addr5, addr6: lgl_addr;
    docut: boolean;
    result: boolean;
  end;

  To2cMaterial = packed record
    DiffColor, ReflColor, AmbRefl, DifRefl, SpcRefl, Opacity, RefIndex, hilitesize: integer;
    Flags: longint;
    TexScale: aFloat;
    ObjSmooth, Unused: boolean;
    MaterialId: Word;
    IsTex, SelfLit, BothFace, IsTexMask, IsTexCol, IsTexRefl: boolean;
    TexName: str255;
    MaterialName, MaterialPath: str255;
    addr, Next, Prev: lgl_addr;
  end;

implementation

end.
