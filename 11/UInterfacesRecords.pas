unit UInterfacesRecords;

interface

uses URecords;

Type        
   answerAArg = RECORD
      answer: aSInt;    { out }
      { no = 0, 1 = yes, 2 = abort }
   END;
   viewArg = PACKED RECORD
      num: aSInt;    { in }
   END;
   getscaleArg = PACKED RECORD
      pint: ^aSInt;{pinteger}   { out }
      getout: boolean;    { out }
   END;
   globescArg = PACKED RECORD
      key: aSInt;    { in }
   END;
   getPlinArg = PACKED RECORD
      msg: aSInt;  { in }   { message to print, 0 if none }
      drawit: boolean;  { in }   { draw while creating and when leaving }
      doclosd: boolean;  { in }   { give "Closed" option? }

      init: pboolean; { in out }  { initialize "frst" and "last"? }
      isclosd: pboolean; { in out }  { returns true if created closed polyline }
      frst,
      last: plgl_addr;{ in out }

      Result: crtstat;  { out }  { normal : got a valid polyline }
      { escape : Fn key f6-s6 was pressed }
      { error  : error in getting last vertex }
      dogesc: boolean;
      dovoid: boolean;
      key: aSInt;  { out }
      fkey: aSInt;  { out }
      vmode: aSInt;  { in }   { allowable view mode }
      Pmsg: ^str80;
      contsrch: Boolean;
      docover: pBoolean;
      dorect: pboolean;
      EnableCurvesOption: boolean;
      addr: lgl_addr;
      chk_addr: boolean;
   END;

   getcharArg = PACKED RECORD
      key: aSInt;    { out }
   END;

   getpointArg = PACKED RECORD
      Result: crtstat;    { out }
      curs: point;      { out }
      key: aSInt;    { out - if result = escape, the key that was pressed }
      fkey: aSInt;    { out, the function key number }
      from3d: boolean;    { in, were we called from 3d }
      curs3d: boolean;    { in }
      snapped: boolean;
      dogesc: boolean;
      viewmode: aSInt;
      curs3: point;
      PickingSymbols: boolean;
   END; 
   DgetdisArg = PACKED RECORD
      pdis: paFloat;     { in out }
      len: aSInt;    { in }
      Result: crtstat;    { out }
      key: aSInt;    { out, set if resut = escape }
   END;
   getdisArg = PACKED RECORD
      pdis: paFloat;     { in out }
      msg: aSInt;    { in }
      len: aSInt;    { in }
      toabs: boolean;    { in }
   END; 
   DgetintArg = PACKED RECORD
      pint: ^aSInt;//pinteger;   { in out }
      len: aSInt;    { in }
      Result: crtstat;    { out }
      key: aSInt;    { out, set if result = escape }
   END;
   getintArg = PACKED RECORD
      pint: paSInt;   { in out }
      len: aSInt;    { in }
      msg: aSInt;    { in }
      toabs: boolean;    { in }
   END;
   DgetangArg = PACKED RECORD
      pang: paFloat;     { in out }
      len: aSInt;    { in }
      Result: crtstat;    { out }
      key: aSInt;    { out, set if resut = escape }
      rel: boolean;    { in }
   END;
   getangArg = PACKED RECORD
      pang: paFloat;     { in out }
      msg: aSInt;    { in }
      len: aSInt;    { in }
      rel: boolean;    { in }
   END;
   getclrArg = PACKED RECORD
      pclr: paSInt;      { in out }
      msg: aSInt;       { in }
      CanReturnZero: boolean;
   END;
   getescArg = PACKED RECORD
      msgs: boolean;    { in }
      key: aSInt;    { out }
   END;
   DgetstrArg = PACKED RECORD
      pstr: Pointer;//pstr132;    { in out }
      len: aSInt;    { in }
      Result: crtstat;    { out }
      key: aSInt;    { out, set if result = escape }
   END;
   { getstring }
   getstrArg = PACKED RECORD
      pstr: Pointer;//pstr132;    { in out }
      len: aSInt;    { in }
   END;
   DgetrealArg = PACKED RECORD
      prl: paFloat;     { in out }
      len: aSInt;    { in }
      Result: crtstat;    { out }
      key: aSInt;    { out, set if result = escape }
   END;
   getrealArg = PACKED RECORD
      prl: paFloat;     { in out }
      len: aSInt;    { in }
      msg: aSInt;    { in }
      toabs: boolean;    { in }
   END;
   gettxtsizArg = PACKED RECORD
      psiz: paFloat;     { in out }
      msg: aSInt;    { in }
      useplot: boolean;    { in }
   END;
   getflname1Arg = packed RECORD
      MaxStringLen: aSint;
      NameTooLongErr: boolean;
      pstr: ^str255;        { in out }
      path: ^str255;        { in out }
      suffix: str255;         { in }
      prompt: aSInt;       { in }
      names: boolean;       { in }
      chkit: boolean;       { in }
      entire: ^str255;        { out }   
      addext: boolean;       { in }
      Result: crtstat;       { out }
      key: aSInt;       { out }
      lbl: aSInt;       { in }
      FilterIndex: aSInt;     {out} // Bill D'Amico 3/5/99
   END;
   TArrSuffix = Array[1..10] of aSInt;
   getflnameArg = packed RECORD
            { IMPORTANT!!! - The presumption here is that both pstr and entire
              point to string variables of the same size, and that both of those
              string variables can hold at least MaxStringLen characters (not counting
              the first length byte)
            }
      MaxStringLen: aSint;
      NameTooLongErr: boolean;
      pstr: pointer; //^str80;        { in out }
      path: aSInt;        { in out }
      suffix: aSInt;         { in }
      prompt: aSInt;       { in }
      names: boolean;       { in }
      chkit: boolean;       { in }
      entire: pointer; //^str80;        { out }
      addext: boolean;       { in }
      Result: crtstat;       { out }
      key: aSInt;       { out }
      lbl: aSInt;       { in }
      FilterIndex: aSInt;     {out} // Bill D'Amico 3/5/99
      numsuffix: aSInt; {in}
      ArrSuffix: TArrSuffix;
      { in } // Devinder for multiple values in dialog box drop down
   END;
   TArcArg = RECORD
      Result: crtstat;     { out }
      key: aSInt;     { out }
      init: pboolean;    { in out }
      state: aSInt;     { in out }
      center: ^point;       { in out }
      begang,                 { in out }
      endang,                 { in out }
      radius: paFloat;       { in out }
      msg: str80;       { in }
   END;
   getpolyArg = packed RECORD
      Result: crtstat;     { out }
      init: pboolean;      { in out }
      key: aSInt;     { out }
      msgno: aSInt;     { in }
      msg: str80;       { in }
      npnts: aSInt;     { in out }
      pnt: polyarr;     { out }
   END;

   getgarcArg = RECORD
      Result: crtstat;     { out }
      key: aSInt;     { out }
      shape: aSInt;     { in  }
      init: pboolean;      { in out }
      state: aSInt;     { in out }
      pnt1,                   { in out }
      pnt2,                   { in out }
      pnt3,
      center: point;       { in out }
      begang,                 { in out }
      endang,                 { in out }
      radius: aFloat;       { in out }
      msg: str80;       { in }
      menu: boolean
   END;

    CircOptions = RECORD
         DoPolyline : boolean;
         pDoPlineCover,
         pDoPlineClosed  : pboolean;
     END;

   CircMenuArg = RECORD
         CircOpts : CircOptions;
     END;
   TextMenuArg = PACKED RECORD
         blkattrib : Boolean;
      END;
   MoveDragArg = packed record
    ForceAndCopy : boolean;
    msg1         : aSInt;
    msg2         : aSInt;
   end;
  SelSetsMenuArg = RECORD
   ForClipboardSelect : boolean;
  END;
    TLyrArg = PACKED RECORD
      lyraddr : lgl_addr;
   END;

     TFLyrArg = PACKED RECORD
                  lyraddr : lgl_addr;{out}
                  offset  : asint;{in out}
                  key     : asint;{out}
                  result  : asint;{out}
            END;
    TShadeArg = Packed Record
		     lyr        : lgl_addr;
           view       : view_type;
		     mode       : mode_type;
		     LightType  : aSInt;
		     EdgeType   : aSInt;
		     BackFacing : BOOLEAN;
		     GouraudOn  : BOOLEAN;
		     retval     : boolean;
   End;
   TPolyArg = Packed Record
		     result   : crtstat;     { out }
		     init     : boolean;      { in out }
		     key      : aSInt;     { out }
		     msg      : str80;       { in }
		     npnts    : aSInt;     { in out }
		     pnt      : polyarr;     { out }
      End;
   TCutOutArg = PACKED RECORD
		  pt1,
		  pt2,
		  pt3,
		  pt4 : point;
        addr1,
        addr2,
        addr3,
        addr4,
        addr5,
        addr6 : lgl_addr;
        docut : boolean;
        result : boolean;
   End;

implementation

end.
