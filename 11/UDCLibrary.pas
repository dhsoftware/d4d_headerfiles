unit UDCLibrary;

interface

Uses Sysutils, UConstants, URecords, UInterfacesRecords, UInterfaces;

function linecolor: asint;

procedure strassign(var tostr: shortstring; fromstr: shortstring);

PROCEDURE printstr(str: string; row, col, clr, cursor: integer; inverse: boolean);

PROCEDURE Beep;
function wall_search: boolean;

function file_exist(fname: string): boolean;
function file_del(fname: string): boolean;

function lyr_undo: layer;

function atan(ang: afloat): afloat;

implementation

procedure strassign(var tostr: shortstring; fromstr: shortstring);
begin
   tostr := fromstr;
end;

PROCEDURE printstr(str: string; row, col, clr, cursor: integer; inverse: boolean);
begin
end;

function linecolor: asint;
begin
   Result := currentstate.color;
end;

PROCEDURE Beep;
BEGIN
END;

function wall_search: boolean;
begin
   Result := True;
end;

function file_exist(fname: string): boolean;
begin
   Result := fileexists(fname);
end;

function file_del(fname: string): boolean;
begin
   Result := DeleteFile(fname);
end;

function lyr_undo: layer;
begin
   //result := delplyr;
end;
                        
function atan(ang: afloat): afloat;
begin
   Result := arctan(ang);
end;

end.
