unit UVariables;

interface

uses URecords;

type
  PtrGSaveVar = ^savevar;

  savevar = packed record
    headerversion: asInt;
    strtype: asInt; { 0 = point, 1 = box }
    dimse1: boolean; { suppress extension line 1 }
    dimse2: boolean; { suppress extension line 2 }
    dimtih: boolean; { text inside horizontal }
    dimtoh: boolean; { text outside horizontal }
    dimtad: boolean; { text above dimension line }
    dimtxtauto: boolean; { auto text placement }
    dimtol: boolean; { tolerance }
    stakfrac: boolean;
    dimasz: aFloat; { arrow size }
    boxclr: asInt; { color of text off boxes }
    boxsiz: asInt; { minimum size of text before boxes are drawn }
    dimcen: aFloat; { center mark size }
    dimexo1: aFloat; { extension line offset for first point }
    dimexo2: aFloat; { extension line offset for second point }
    dimexe: aFloat; { extension line extension }
    dimdli: aFloat; { dimension line increment }
    dimtp: aFloat; { plus tolerance }
    dimtm: aFloat; { minus tolerance }
    dmlntyp: asInt; { linear dimensioning type }
    dimtype: asInt; { linear, angular, dia, rad }
    dimangl: aFloat; { only used in rotated }
    dimtic: asInt; { 0 = arrows, 1 = tics, 2 = dots }
    srch: boolean; { search on all layers ? }
    XrefDragCtr: boolean;
    aline: asInt; { text alignment type }
    gridclr: asInt; { dots }
    grd1clr: asInt; { crosses }
    grid1sz: asInt; { size of crosses }
    htchorg: point; { hatch origin }
    drwsnap: boolean; { snapping point for windows & doors }
    mirtext: boolean; { if true, fix text when mirroring }
    allorgs: boolean; { if true, set all grid origins together }
    ofsdyn: boolean;
    symbinc: aFloat; { z symbol increment }
    curssiz: asInt; { current size of small cursor }
    doortyp: asInt; { type of door being drawn }
    chamfra: aFloat; { first chamfer distance }
    chamfrb: aFloat; { second chamfer distance }
    wallcln: asInt; { wall cleanup 0=none, 1="T" intersection, }
    { 2=wall cap }
    ovrdraw: boolean; { Draw lines past endpoint ? }
    LastXrefDyn: boolean;
    inpstyl: asInt; { style of inputting: }
    { 0 = relative polar }
    { 1 = absolute polar }
    { 2 = relative rect }
    { 3 = absolute rect }
    dimmovr: aFloat; { dimension line extension amount }
    plincover: boolean;
    showref: boolean; { show layer reference marks during refresh }
    sym_enl: point;
    jambthk: aFloat;
    select: asInt; { line, shape, area }
    dimtxtsz: aFloat;
    dimstrdis: aFloat;
    arrtype: asInt; { indicates type of arrow }
    arratio: aFloat; { arrow aspect ratio : L/W }
    filrad: aFloat;
    filcut: boolean;
    plinrect: boolean;
    doorang: aFloat;
    wallwidth: aFloat;
    doorwidth: aFloat;
    windwidth: aFloat;
    drawmarks: boolean;
    multilayer: boolean;
    smallgrid: asInt;
    scroll: aFloat; { % of screen to scroll }
    numdivs: asInt;
    hatchmode: asInt;
    hatchsc: aFloat;
    hatchang: aFloat;
    findhatch: boolean; { if false, ignore hatch things on searches }
    showall: boolean;
    basez: aFloat;
    hitez: aFloat;
    scalefacx: aFloat;
    scalefacy: aFloat;
    scalefacz: aFloat;
    numsides: asInt; { number of sides in polygons }
    temprot: aFloat;
    arrowsize: aFloat;
    userlines: boolean;
    autocalc: boolean;
    snapangs: asInt; { number of snap angles when doing tangents }
    jambin: boolean;
    jamb: boolean;
    jambwth: aFloat;
    rotang: aFloat;
    cutout: boolean;
    popmess: boolean; { pbm 2/12/01 for ctrl-right click menu }
    outsill: aFloat;
    insill: aFloat;
    glssthk: aFloat; { thickness of glass }
    tmpstrx: aFloat; { template stretch in x }
    tmpstry: aFloat; { template stretch in y }
    tmpstrz: aFloat;
    sidewnd: boolean; { true if defining both sides of windows }
    InsXClip: boolean;
    swngtype: asInt; { ver 7.5 - GKReddy }
    texton: boolean;
    curvdts: boolean; { draw dots on beziers, splines ? }
    showwgt: boolean;
    fillon: boolean;
    lytsize: asInt; { layout size of screen, 50 = 50 % } // unused
    txtang: aFloat;
    txtsiz: aFloat;
    txtwgt: asInt;
    txtslnt: aFloat;
    txtaspt: aFloat;
    swngcolr: asInt;
    orthmode: boolean;
    refresh: boolean;
    constref: boolean;
    bitmapon: boolean;
    refpnt: point;
    missdis { fuzzy } : asInt;
    scaletype: asInt;
    phase: asInt;
    service: asInt;
    dept: asInt;
    lf_fact: aFloat;
    noposttime: longint; { time that has not been posted }
    totime: longint; { total time spent in drawing }
    rate: asInt;
    delay: asInt;
    centline: asInt; { 0=sides, 1=center of wall, 2=ctr of cavity }
    noisy: boolean;
    bigcurs: boolean;
    f_delay: asInt; { delay in minutes }
    lyrview: boolean;
    FilledArrow: boolean;
    numscales: asInt;
    wall: asInt;

    osnap: packed record
      val: asInt;
      { bit encoded object snap mode, see notes 5-jan-6 }
      num: asInt; { number of points for n-point snap }
    end;

    saveview: array [1 .. 10] of packed record
      xr: aFloat;
      yr: aFloat;
      scalei: asInt;
      desc: string[11];
      active: asInt;
    end;

    LastXref: packed record
      ang: aFloat; { Rotation angle }
      scl: aFloat; { Enlargement factor }
    end;

    angstyl: asInt; { readout of angles }
    nounits: boolean;
    typemode: boolean; { pbm tags }
    mindimm: aFloat; { minimum distance to dimension }
    automis: aFloat; { auto dimension miss distance }
    autosty: asInt; { style of auto dimm last used }
    dmnopts: boolean; { ignore points in auto dimm }
    fixddis: boolean; { use savevar.dimdli for third point in auto dimm }
    lyoutx, lyouty: asInt; { layout sheet divisions }
    autolyr: boolean; { auto dimension all layers? }
    assoc: boolean;
    ptsonly: boolean;
    fromdim: boolean;
    dimmon: boolean;
    autodim: boolean;
    doorhgt, sillhgt, headhgt: aFloat;

    plt: packed record
      scalei: asInt;
      xr: aFloat; { center of drawing }
      yr: aFloat;
      penspeed: asInt; // Not used dpsv??
      penwidth: asInt; // dpsv??
      papersize: asInt;
      color: boolean;
      pensort: boolean; // dpsv??
    end;

    dynam: boolean;
    symbperp2ply: boolean;
    { if true then symbinc is ignored and symbols are 'dropped' only polygons }
    currlayer: lgl_addr;
    sym_exp: boolean; { symbol explode }
    symbz2ply: boolean;
    { if true then symbinc is ignored and symbols are 'dropped' only polygons }
    symname_addr: lgl_addr;
    usrfact: boolean; { userline factor ON/OFF - Unused }
    hatchon: boolean;
    ts_create: timestamp; { time this file was created }
    ts_update: timestamp; { time this file was last saved }
    arrowgt: asInt;
    arroclr: asInt;

    pltlim: packed record
      x, y: aFloat;
    end;

    dmticwt: asInt; // *****       { width of tic marks }
    dim2lns: asInt; { used in exploding dimensions }
    maxdrag: asInt; { maximun number of lines to drag }
    boxsym: asInt; { minimum size of symbols before boxes are drawn }
    plyVert: boolean; { true if polygons start on vertex }
    plyInsd: boolean; { true if polygons inscribed }
    plyDiam: boolean; { true if polygons by diameter }
    plyCntr: boolean; { true if adding polygon centers }
    aperture: boolean;
    autoInc: boolean;
    matchwall: boolean;
    matchdoor: boolean;
    loc_error: asInt; { Symbols Local Error }
    dinstd: boolean;
    dmtxtoff: aFloat; { dimension text offset }
    dmtxtwt: asInt; { dimension text weight }
    showneg: boolean; { show neg. numbers in dist readout? }
    plyrect: boolean;

    goodies: packed record
      srch: boolean;
      lyr: lyraddr;
    end;

    copflag: boolean;
    dmslant: aFloat; // *****
    dmaspct: aFloat;
    inwall: boolean;
    showz: boolean;

    rotplot: packed record
      on1: boolean;
      ang: aFloat;
      cent: point;
    end;

    ssactive: asInt;
    showins: boolean;
    crcfact: aFloat; { factor for circle precision }
    doacres: boolean;
    frmwinfont: boolean;
    txtWinFont: boolean;
    dmwinfont: boolean;
    lastdist: point; { last distance used in copy, move or stretch }
    snapsrch: boolean; { layer search used for object snap }
    symfact: aFloat; { symbol line spacing enlargement factor }
    atrDraw: boolean; { draw visible attributes ? }
    autopath: boolean;
    { automatic creation & setting of path for sym files }
    frstAtr: lgl_addr; { system attributes }
    lastAtr: lgl_addr;
    disSync: boolean;
    dxfFact: aFloat; // dpsv??
    symRotate: boolean; { rotate symbols on insertion }
    multimov: boolean; { used in move/drag for multiple selection }
    noSingle: boolean; { use fixed decimal for distance readout }
    sigdigits: asInt; { number of decimal places }
    line_enlg: aFloat; { line spacing enlargement factor }
    dimmpts: boolean; { show assoc. dimm. control points }
    txtuseplt: boolean; { Should text sizes use current plot scale? }
    dynamtxt: boolean; { Text being entered dynamically? }
    LastEnlPt: point; { last point used for center of enlargement }
    dimtxtclr: asInt; { dimensioning text color }
    dimticclr: asInt; { dimensioning tic color }
    dimlim: boolean; { use dimension limit? }
    persviewoption: byte; { look Ud3pers for details }
    dimangtp: aFloat; { plus tolerance amount for angular dimensions }
    dimangtm: aFloat; { minus tolerance amount for angular dimensions }
    LastRotPt: point; { last point used for center of rotation }
    StartAng: aFloat; { Angle that defines zero for the user interface }
    ClkWise: boolean; { Angles increase clockwise? }
    crvpts: boolean; { Display center points of 2d curves ? }
    regen_order: asInt; { 0 = active first, 1 = active last, 2 = in order }
    layout_exts: boolean; { Display extents only for plot layout }
    dl_fact: asInt; { viewport to NDC size factor }
    copy_ctr: boolean; { Display rectangular array counter? }
    dimautorot: boolean;
    { Use automatic rotation when dim text doesn't fit? }

    freehandtyp: asInt;
    plottime: longint; { total time for plotting }
    hidetime: longint; { total time for hidung }
    perstime: longint; { total time for personal outtime }

    offsdis: aFloat; { distance for offset/geo }

    globpara: point;
    globpers: point;
    goldcut: boolean;
    // serial2sym  : boolean; dpsv removed
    dmdirect: boolean;
    // matrixplot  : boolean;        { Print on MatrixPlotter? (TSR)} dpsv removed

    TextMask: TxtMaskType;
    srchwinfont: boolean;
    txtsizMask: aFloat;

    // numplots    : aSInt;       { Number of plots to be made } // dpsv removed
    SelGreedy: boolean; { Makes select-fence or select-box greedy }
    // PixSclState : aSInt;       { 0:StdScl; 1:PixScl; 2:PixScl&PixAspct }  // dpsv removed
    // calibrAng   : aFloat; //dpsv removed
    // calibrCent  : point; // dpsv removed
    refmarks: boolean;
    cutmarks: boolean;
    LyrFilterOn: boolean; { An/Aus-Schalter f�r Filter }
    lastver: asInt;
    walltype: asInt; { 0=none, 1=2LnWalls, 2=3LnWalls, 3=4LnWalls }

    hilite: packed record
      on1: boolean; { true if hilite outside wall line }
      outside: boolean; { used for centline only }
      color: asInt; { color of hilite }
      Width: asInt; { width of hilite }
      linetype: asInt; { linetype of hilite }
    end;

    cwall: packed record
      ext_width: aFloat; { Exterior wall width }
      int_width: aFloat; { Interior wall width }
      cav_width: aFloat; { Cavity thickness }
      color: asInt; { Color for cavity lines }
      lnwidth: asInt; { Line Width for cavity lines }
      lntype: asInt; { Line type for cavity lines }
    end;

    cntrline: packed record
      color: asInt; { Color for cavity lines }
      lnwidth: asInt; { Line Width for cavity lines }
      lntype: asInt; { Line type for cavity lines }
    end;

    multilayout: boolean;
    currsheet: asInt;
    PaletteData: lgl_addr;
    strings_stored_location: lgl_addr;
    Areasigdigits: asInt; { number of decimal places }
    DrawHatchPlin: boolean;
    sym_exp_currlyr: boolean;
    sym_exp_oldAttrib: boolean;
    txttype: RTextType;
    ZbyLayer: boolean;
    MoveAutoRefresh: boolean;
    SymZOffset: asInt;
    OffsPerp: boolean;
    First_LineDef, Last_LineDef: lgl_addr;
    XRefNoNested: boolean;
    GlobalOvershoot: boolean;
    First_HatchDef, Last_HatchDef: lgl_addr;
    StandardLineTypesFactor: array [1 .. 4] of aFloat;
    OvershootHatch: boolean;
    LockHatch: byte;
    RotSelBox: boolean;
    OpenRFM: boolean;
    ShowHatchOrigin: boolean;
    smallfrac: aFloat;
    Trim1LineByEnt: boolean;
    XRefZOffset: asInt;
    XRefz2ply: boolean;
    xrefinc: aFloat;
    ClipBZOffset: asInt;
    ClipBz2ply: boolean;
    ClipBinc: aFloat;
    symexp_currlyr: byte;
    xref_multi: boolean;
    KeepGroups: boolean;
    frstmaterial: lgl_addr;
    lastmaterial: lgl_addr;
    frstwinline: lgl_addr;
    dimscaletype: asInt;
    dimnounits: boolean;
    dimstakfrac: boolean;
    dimsmallfrac: aFloat;
    dimnosingle: boolean;
    dimsigdigits: asInt;
    smartwalls: boolean;
    symuseplt: boolean; { Should symbol enlargement use current plot scale? }
    symscale: aFloat;
    syminview: boolean;
    smartdatastorage: lgl_addr;
    rotate_quick: boolean;
    rotate_by_center: boolean;
    rotate_to_absolute: boolean;
    doorjambin: boolean;
    Nested_Xref_Hilite: boolean;
    XRef_Name: boolean;
    Layer_Menu_Offset: asInt;
    bindmaterial2ent_read, bindmaterial2ent_write: boolean;
    ClipRotate: boolean;
    SnapOffset: boolean;
    SnapOffsetDis: aFloat;
    rottext: boolean; { if true, fix text when rotating }
    SnapOnScreen: boolean;
    OffLayersOn: boolean;
    { Turn On destination layers that are currently off for Clipboard paste }
    enlarge_by_center: boolean;
    AnyLayer: boolean;
    BindMaterial: boolean;
    DoorSideByStrike, DoorCenterByStrike, HideNodeText: boolean;
    ClipBoardEnl: point;
    isSingleDL: boolean;
    ScaleIndOvershoot: boolean;
    SymAttrAllCaps: boolean;
    DynamicSymFlip: boolean;
    AreaPerimSelect: boolean;
    perimsign, areasign: asInt;
    ShowAxis: boolean;
    ShowZero: boolean;
    createvoid: boolean;
    MaterialData: lgl_addr;
    SymKeepGroup, PasteKeepGroup, ExplodeKeepGroup: boolean;
    BrowseType: TBrowseType;
    BrowserAutoDivisions: boolean;
    BrowserPerDrawing: boolean;
    BrowserIso: boolean;
    SymbolExplode2DOnly: boolean;
    SymbolExplode3DOnly: boolean;
    ViewDependency: boolean;
    CalibrateByPoints: boolean;
    AutoDimJambs: boolean;
    LockTextAngle: boolean;
    PasteSymbolByCenter: boolean;
    ClipBoardCtrRefPnt: boolean;
    LockTextSize: boolean;
    { ScaleIndependentText : boolean; }
    XrefsRememberGTV: boolean;
    DetailLinked2GTV: boolean;
    ArrowsAtHead: boolean;
    SymCreateLeaveEnts: boolean;
    lFunktionFirst: asInt;
    ToLayerColor: boolean;
    Arrows1stOrtho: boolean;
    SmartArrows: boolean;
    AreaNoFloat: boolean;
    MTextPTextCrosshair: boolean;
    HideNodeMark: boolean;
    MSPLayout_Show_Ents: boolean;
    LockSymbolSize: boolean;
    LockHatchScale: boolean;
    KnockOutText: boolean;
    NoPenSort: boolean;
    KnockOutBorderText: boolean;
    ShowKnockOuts: boolean;
    ShowKnockOutBorders: boolean;
    WallHatchOn: boolean;
    WallFillOn: boolean;
    KnockOutsDrawLast: boolean;
    NodeTextKnockOut: boolean;
    Nested_HatchFill: boolean;
    XRefWallHatchOff: boolean;
    XRefWallFillOff: boolean;
    TxtKnockOutEnlX: aFloat;
    TxtKnockOutEnlY: aFloat;
    DimKnockOutEnlX: aFloat;
    DimKnockOutEnlY: aFloat;
    wysiwyg: boolean;
    Mirror2DByEnt: boolean;
    PLinClosed: boolean;
    KnockOutCurves: boolean;
    KnockOutBorderCurves: boolean;
    KnockOutPolys: boolean;
    KnockOutBorderPolys: boolean;
    TextAlignByEntity: boolean;
    BrowserDivisionsX, BrowserDivisionsY: asInt;
    Suppress_XClips: boolean;
    Suppress_SClips: boolean;
    Text_Underline: boolean;
    Text_Overline: boolean;
    AssocArea: boolean;
    ShowAreaOutline: boolean;
    DimPrefixOn: boolean;
    DimSuffixOn: boolean;
    DimSuppressDim: boolean;
    SaveSymMulti: boolean;
    InPlotMenu: boolean;
    FitArray: boolean;
    GTVExtents: boolean;
    AtCenter: boolean;
    ToLayerDoLineType: boolean;
    ToLayerDoLineSpacing: boolean;
    ToLayerLineType: asInt;
    ToLayerLineSpacing: aFloat;
    LineTypeByLayer: boolean;
    SpacingByLayer: boolean;
    EllipseType: asInt;
    EllipseArcs: boolean;
    NumEllArcs: asInt;
    Charset: byte;
    HideHelplines: boolean;
    HelplineColor: asInt;
    DrawHelplinesFirst: boolean;
    UseRL: boolean;
    RL: aFloat;
    HelplineWinLType: byte;
    HelplineWinLTypeFactor: aFloat;
    AS1100_Comma_Separator: boolean;
    SelfXRefNesting: boolean;
    OpenSymLyrMgr : boolean;
    Rise  : afloat;
    Pitch : afloat;
    KeepTop : boolean;
    DoGrade : boolean;
    Grade : aFloat;
    ByAngle : boolean; 
    InclinesByPitch : boolean;
    InclinesByAngle : boolean;
    InclinesByGrade : boolean;
    Run : afloat; 
    SymbolsRememberGTV : boolean; 
    FatCursor : Boolean;
    CursorThickness : aSint;
    BrowserNoShowNested : boolean;
    SymExplodeOnOnly : boolean;
    PDFtoImageDPI : byte;  // 1=100; 2=200; 3=300; 4=400; 5=500; 6=600;
    PDFtoImageType : byte; // 1=.BMP; 2=.GIF; 3=.JPG; 4=.PNG; 5=.TIFF
    LockPDFSize : boolean;
    AlphaBlendAEC: Boolean;
    AlphaBlendAECValue: Byte;
    NoTxtCurs : boolean;

    { Delphi DCAL Record - UVariables.pas }
    // Add any new lgl_addrs to TGlobalVarAddrs record as well and update Get/Put procs
    // Uses these bytes
    UnusedDataBytes: array [1 .. 755] of byte;

    LastDataByteBeforeStringData: byte;

    // Only strings go below this section.
    frmname, fname, { current (or last) file name }
    fname_all: symstr; // string[81];   { entire path name of current file }
    hatchstr: str255;
    sectionhatchstr: str255;
    empinits: string[7];
    projnum: string[9];
    projname: string[9];
    password: string[11];
    dcxName: string[255];
    setname: array [0 .. 7] of set_name;
    fontname: fontstr; { current font file }
    dimfont: fontstr; { current dimensioning font file }
    formfont: fontstr; { current forms font file }
    searchFont: fontstr;
    symname: string[80]; { last symbol name used }
    sympath1: string[81];
    lastsymins: string[9];
    dimscript: string[81]; { dim text description for actual vals }
    searchText: string[81];
    LyrFilter: lyr_name_8; { AnzeigeFilter f�r Folienauswahl }
    arttype: string[9]; { Valid article type }
    artname: string[17]; { Active article name }
    artdbname: string[17]; { Current data-file of article }
    arttypelbl: string[9]; { label of article type }
    activewallstylename, activedoorstylename, activewindowstylename, rendergroundmaterialini, renderbackimage: str255;
    SymbolFolder: str255;
    TemplateFile: str255;
    ArrowSymbol: str255;
    DimPrefix: str80;
    DimSuffix: str80;

    // Not saved/restored strings
    SymBrowserFName: symstr;
    DispTTF: TDisplayTTF;
    DoCapsIniMessage: boolean;
    DisplayShaderLights: boolean;
  end;

  FrstViewType = array [0 .. 3] of boolean;
  ViewAddrType = packed array [0 .. 3] of lgl_addr;

  PtrGsv = ^svtype;

  svtype = packed record
    hlrcrop, { Crop hlr images to screen }
    no_penaway: boolean; { used to supress putting pen away when finished plotting }
    viewmode: asInt; { Current viewing mode. }
    circldiv1, { Primary circle divisions }
    circldiv2: asInt; { Secondary circle divisions }
    hideon, { Hidden line removal in progress }
    quicksrch, { Quick search of curved surfaces control points }
    shelon, { Solid modeler enabled }
    drawmarks, { Drawing of markers active }
    divover, { Override circle divisions }
    bezgrdon, { Bezier surface grids on }
    bezpnton, { Bezier surface control pnts on }
    modelon, { Modelling level >= 1 }
    targon: boolean; { Target currently in use }
    LayoutMarks: boolean;
    mindivs: asInt; { Minimum number of circle divisions }
    frstview: FrstViewType;
    modlevel: asInt; { Tracks the concat level of xform }
    relzero: aFloat; { Relative value of zero epsilon }
    zerofact, { Variable constants }
    sqrzero: aFloat; { sv.relzero squared (HLR) }
    contcyc: boolean; { Cyclic flag for contours }
    fixcone: boolean; { Fixed cone of vision for persp }
    srchtype: asInt; { 1 = line, 2 = shape, 3 = area, 4 = fence }
    quicksnap: boolean; { True if quick snapping on in 3D }
    slabthick: aFloat; { Current slab thickness }
    shelclosed, { True if circular shapes are closed }
    cylsidclo: boolean; { True if close sides of cylinders }
    contstiff: aFloat; { Contour curve stiffness factor }
    cylrad, { Current cylinder radius (horiz) }
    cylbang, { Cylinder beginning radius (horiz) }
    cyleang: aFloat; { Cylinder ending radius (horiz) }
    hplymode: asInt; { Input mode for horizontal polygons }
    contindx: aFloat; { Index used for contours }
    dombang, { Beginning angle of dome }
    domeang: aFloat; { Ending angle of dome }
    torradius: aFloat; { Current torus roll radius }
    torbang, { Current torus roll beginning angle }
    toreang: aFloat; { Current torus roll ending angle }
    autohite: boolean; { Automatic height set in skew }
    roofstyle: asInt; { Currently set roof input mode }
    edgemode: asInt; { Input mode for get edge }
    plnemode: asInt; { Input mode for get plane }
    userz1, { Current value of userz1 }
    userz2: aFloat; { Current value of userz2 }
    sdiv, { Current sdivs for bez surfs }
    tdiv: asInt; { Current tdivs for bez surfs }
    rotmode: asInt; { 3D rotation axis }
    rotang: point; { 3D rotatioin angles }
    rotctr: point; { Center of rotation }
    enlfact: point; { 3D enlargment factor }
    expltype: asInt; { Explode mode: 1 lines, 2 pgons }
    circledgs: boolean; { Only edges of objects in HLR }
    addtodwg, { Add lines from HLR to layer }
    slabref: boolean; { Show reference side for a slab }
    orthoflip: boolean; { Flip ortho in parallel view }
    rotangle: aFloat; { Parallel view rotation view }
    markmode: asInt; { Input mode for markers }
    vplymode: asInt; { Input mode for vertical polygons }
    persdis: aFloat; { Scaling fraction / percent }
    targ: point; { x, y : ratio, z : radius }
    oblqang: aFloat; { Oblique view angle }
    oblqfact: aFloat; { Oblique view scale factor }
    zflag: asInt; { 0 = z-base, 1 = z-height }
    oblqtype: asInt; { 0 = plan, 1 = elevation }
    planang, viewang: aFloat;
    perscent: point; { Center of perspective view }
    perseye: point; { Perspective eye point }
    hidehead: lgl_addr; { Head of HLR database in hidSpc }
    pierceon, { Piercing on in hlr module }
    arctogon2, { 2D arcs converted to polygons }
    arctogon3, { 3D arcs converted to polygons }
    ellptogon, { Ellipses converted to polys }
    drawhidden: boolean; { Draw hidden lines as hidestyle }
    hidefact: aFloat; { Line factor for hidden lines. }
    hidetype, { Style of hidden lines. }
    hidecolor: asInt; { Color of hidden lines. }
    memerr, { true if memory allocation error }
    broken: boolean; { true if user broke out }
    numents, totents, numtgons, tottgons: longint;
    clipon: boolean; { True if clipping cubes is on }
    clipmin, { Minimum extents of clipping cube }
    clipmax: point; { Maximum extents of clipping cube }
    strtview, { First saved view in list }
    lastview: lgl_addr; { Last saved view in list }
    numviews: asInt; { Total saved view in list }
    linemode: asInt; { Input mode for hori polygons }
    chain: boolean; { True if chaining in lines, vert polys }
    boxmode: asInt; { Input mode for blocks }
    circmode: asInt; { Input mode for arcs }
    polymode: asInt; { Input mode for polygons }
    wallmode: asInt; { Input mode for walls }
    lasteview: asInt; { Last editing view }
    perseyez: aFloat; { Z-coord of perspective eye point }
    perscenz: aFloat; { Z-coord of perspective center pt }
    viewcent: point; { Center of rotation in parallel views }
    targclr: asInt; { Color of globe target }
    lyrborder: boolean;
    lyrextent: boolean;
    lyrvrefresh: boolean;
    cliprefresh: boolean;
    lyrvsize: aFloat;
    numdetails: asInt;
    MSPX, MSPY: asInt;
    TxtAllCaps: boolean;
    PlotSnap: boolean;
    coneang: aFloat; { Cone of vision for persp }
    revtype: asInt; { Rev surface type 0, 1, 2 }
    revbang, reveang: aFloat; { Beginning and ending angles of rev surfs }
    hidewidth: asInt; { Width of hidden lines }
    hidemode: asInt; { Scan mode for HLR process }
    cubemode: asInt; { Input mode for blocks }
    lyrrefresh: boolean; { False suppresses auto refresh }
    revsector: boolean; { True if partial revsurfs are sectors }
    contmode: asInt; { Input mode for contours }
    strmode: asInt; { Selecting mode for 3D stretch }
    contfixz: boolean; { flag - z-coordinates fixed or not }
    movcopy: boolean; { copy while rotating, mirroring, moving }
    pipedest: asInt; { Destination through pipline }

    viewaddr: ViewAddrType;
    snapmode: asInt; { Current 3d snapping mode }
    linehead: lgl_addr; { Head of line stack in hlr. }
    hither: aFloat; { Distance to hither clipping plane. }
    holemode: asInt; { Voids processing mode }
    perslay: lgl_addr; { View for perspective layout }
    contzsor, { Input source for contour z coords }
    contdivs: asInt; { Current divisions for contours }
    rotcopy, { Andcopy flag for 3d rotate. }
    mircopy: boolean; { Andcopy flag for 3d mirror. }
    dynamrot: boolean; { Dynamic flag for 2d rotate }
    symlevel: asInt; { nesting level of symbols }
    persfact: aFloat; { Scaling factor for perspective clipping. }
    perswalk, { Walking distance for perspective }
    persturn: aFloat; { Turning angle for perspective }
    perscmat: modmat; { Post clipping perspective matrix }
    ccw: boolean; { True if get arc 3d, 2 pt clockwise }
    enlcopy: boolean; { Andcopy flag for 3d enlarge. }
    inclmode: asInt; { 0 = vertical, 1 = perpendicular }
    stepdist: aFloat; { Perspective stroll command }
    lookang: aFloat; { Perspective stroll command }
    inpstyl3: asInt;
    line3mode: asInt; { Input mode for 3d lines. }
    FastSymSnap: boolean; { In 2d, snap to ins. point only? }
    dimscale: asInt;
    dodimscale: boolean;
    // ltypfile: string [9]; { name of the userline file to use }
    hidd_div: boolean; { hidden line subdiv. alg. flag }
    hidd_div_x: asInt; { hlr subdivision in x }
    hidd_div_y: asInt; { hlr subdivision in y }
    rem_dbl_lns: asInt; { hlr remove double lines 0, 1, 2 }
    // pic_bckgrnd_on : aSInt;  { background pictures (not) visible }  dpsv removed
    backfacing: boolean; { remove backfacing polygons flag }
    lighttype: asInt; { Type of light }
    edgetype: asInt; { Type of edge }
    lastindex: longint; { global index }
    newwalls: boolean; { old or new wall-handling
      true  : new walls
      false : old walls }
    keepwalls: boolean; { true  : changing of newwalls not possible
      false : changing possible }
    addattr: boolean; { Add attribute to entity or remove }
    autoadd: boolean; { Active article added to each }
    { entity created }
    freezoom: boolean;
    showoutline: boolean;
    dynam: boolean;
    ClipBoardRefPnt: boolean;
    cwall_spcg: aFloat;
    cline_spcg: aFloat;
    roundval: aFloat;
    rounddim: boolean;
    ClipBoardByOrigin: boolean;
    roundtyp: asInt;
    intensity: asInt;
    areascale: asInt;
    attflags: asInt;

    dropmesh: packed record
      nrows, ncols: asInt;
      smooth: boolean;
      diagsvis: boolean;
      dropedge: boolean;
      dropedge_hite: aFloat;
      stiffness: byte;
      smoothpass: byte;
    end;

    strtmultiview, { First saved multi view in list }
    lastmultiview: lgl_addr; { Last saved multi view in list }
    scrollstepdist: aFloat;
    clipNoZ: boolean;
    xclipsmerge: boolean;
    renderbycolor, renderxreflights, renderdogroundplane, rendergroundplaneatbase, renderrememberlastview: boolean;
    rendergroundheight, rendergroundenlg, rendersmoothang, rendersmoothdis: aFloat;
    renderbackcolor, renderlightsvalue: integer;
    NormalDown: boolean;
    BrokenI: asInt;
    stretchcopy: boolean;
    ExplodeTriangulate: boolean;
    Sym_Layer_Control: byte; { 0 = None; 1 = By Definition; 2 = By Instance }   
    ClipStepIncPlan : aFloat;
    ClipStepIncPara : aFloat;

    // Add any new lgl_addrs to TGlobalVarAddrs record as well and update Get/Put procs
    Unused: array [1 .. 456] of byte;
  end; { svtype }

  pwrld = ^wrld;

  wrld = record
    top, { top    of window in world coords }
    bot, { bottom of window in world coords }
    rht, { right  of window in world coords }
    lft, { left   of window in world coords }
    sym, { symbol window edge in world coords }
    pix: aFloat; { size of one pixel in world coords }
    step_sz: aFloat; { offset for thick lines ?? }
    cent: point; { center of screen in world coords }
    minx, miny, maxx, maxy: aFloat; { corners of ndc space in world coords }
  end;

implementation

end.
