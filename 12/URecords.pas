unit URecords;

interface

uses UConstants;

type
   aSInt = Integer;
   pasInt = ^asInt;
   aFloat = Double;
   pafloat = ^aFloat;
   wantType = aSInt;
   action = (Afirst, Alast, Aagain, AlSize);
   crtstat = (res_normal, res_error, res_escape);

   adrmem = pointer;
   adsmem = pointer;
   adsfunc = pointer;

   str80 = string[81];
   pstr80 = ^str80;
   str8 = string[9];
   str16 = string[17];
   str132 = string[133];
   str255 = string[255];
   pstr255 = ^str255;
   pstr132 = ^str132;
   bset256 = ARRAY [0..15] OF aSInt;
   keystr = ShortString;
   fontstr = string[80];
   lyr_name_8 = String[lyr_name_max];
   symstr = string[sym_len];
   set_name = String[8];
   
   lgl_addr = packed RECORD
      page,
      ofs: aSInt;
   END;
   entaddr = lgl_Addr;
   symaddr = lgl_addr;
   atraddr = lgl_addr;
   pvtaddr = lgl_addr;
   viewaddr = lgl_addr;
   lyraddr = lgl_addr;
   plgl_addr = ^lgl_addr;

   point = packed RECORD
      CASE aSInt OF
         0: (x, y, z: aFloat);
         1: (v: ARRAY [0..2] OF aFloat);      { vertex }
   END;
   Point2D = RECORD
      x, y: aFloat;
   END;
              
   timestamp = longint;

   DxfModeType = SET OF (Dxf_Unused07,
      Dxf_Unused06,
      Dxf_Unused05,
      Dxf_Unused04,
      Dxf_Unused03,
      Dxf_Unused02,
      Dxf_Automatic,
      Dxf_Move,
      Dxf_Unused15,
      Dxf_Unused14,
      Dxf_Unused13,
      Dxf_Unused12,
      Dxf_Unused11,
      Dxf_Unused10,
      Dxf_Unused09,
      Dxf_Unused08);

   TxtMaskType = SET OF (TxtM_sText,
      TxtM_sCaseSens,
      TxtM_sFont,
      TxtM_sSize,
      TxtM_Unused03,
      TxtM_Unused02,
      TxtM_Unused01,
      TxtM_Unused00,
      TxtM_Unused15,
      TxtM_Unused14,
      TxtM_Unused13,
      TxtM_Unused12,
      TxtM_Unused11,
      TxtM_Unused10,
      TxtM_Unused09,
      TxtM_Unused08);

   TClrBtnStat = set of (cbtn_normal, cbtn_on, cbtn_off, cbtn_lyractive, cbtn_lyrlocked);
   TDCMenuBtnInfo = packed record
      Name: ShortString;//KeyStr; Cannot use longstring because this record
      // is sometimes part of state record and thus there is no finalization to
      // the string. Hence short string
      Toggle: TClrBtnStat;
      lblfilelblnum: integer;
      Hint: ShortString;
   End;
   keyrec = packed Record
      CSToolbarId: asInt;
      WrtLvl: string[20];
      DCMenuBtns: ARRAY [1..20] OF TDCMenuBtnInfo;
   END;

   handletype = SmallInt;
   Dta = Array[0..79] of byte;
   fileType = PACKED RECORD
      // dpsv - dont think any reason to change this record.
      handle: handleType;
      error: Smallint;
      nextchar: char;
      dummy: Boolean;
      nextvalid: boolean;
      istext: boolean;
      recordlen: SmallInt;
      buf: ARRAY [0..bufSize - 1] OF char;
      ptrBuf: SmallInt; { next char to read in buffer }
      numBuf: SmallInt; { num chars in buffer }
      mode: SmallInt;
   END;
   pFileType = ^fileType;


   RTextType = packed record
      backwards: boolean;
      upsidedown: boolean;
      justification: TEXT_JUST_LEFT..TEXT_JUST_FIT;
      VerticalAlignment: TEXT_VALIGN_BASELINE..TEXT_VALIGN_TOP;
   end;

   vec4 = packed ARRAY [1..4] OF aFloat;
   modmat = packed ARRAY [1..4] OF vec4;
   pntmat = ARRAY [1..4] OF ARRAY [1..4] OF point;  { Points for bezier surface }
   pntarr = ARRAY [1..maxpoly] OF point;
   pnt4arr = ARRAY [1..4] of point;
   polyarr = pntarr;
   ppolyarr = ^polyarr;
   boolarr = ARRAY [1..maxpoly] OF boolean;
   bezarr = ARRAY [1..max_spline] OF point;

   layer = lyraddr;{This is what old DCAL mens}
   Rlayer = packed record
      addr: lyraddr;           { logical address of this layer description }
      addrSpc: adsmem;
      nxt: lyraddr;
      prev: lyraddr;
      frstatr,
      lastatr: atraddr;
      chg: boolean;          { true if changed }
      on: boolean;          { = true if plane is on }
      num: aSInt;          { layer number }
      cnt: longint;          { number of lines on this layer }
      color: aSInt;
      Name: lyr_name_8;     { contains the plane name }
      firstln: entaddr;         { first line on this level }
      lastln: entaddr;         { last line in link }
      gridorg: point;            { grid origin }
      showdivs: point;            { size of displayed grid (1/32's of an inch) }
      shw1divs: point;            { larger display grid }
      snapdivs: point;
      snapgrid: boolean;          { true if snapping to grid }
      showgrid: boolean;          { true if showgrid is displayed }
      shw1grid: boolean;
      stupid03: boolean;
      numdivs: aSInt;          { number of angular divisions }
      gridang: aFloat;
      onoff: aSInt;
      group: aSInt;          { group number for this layer }
      min: point;
      max: point;
      refpnt: point;            { reference point }
      lastindex: longint;         { last index in layer }
      materialindex: Word;
   END;

   polyvert = PACKED RECORD
      addrspc: adsmem;
      addr: pvtaddr;
      Next: pvtaddr;
      prev: pvtaddr;
      shape: aSInt;
      pnt: point;
      bulge: aFloat;
      nextpnt: point;       { Next point in list }
      last: boolean;     { True if this is the last in list }
      info: Byte;  
   END;

  TDisplayTTF = PACKED RECORD
      dooutline: Boolean;
      dofill: Boolean;
      OutlineClr: aSInt; {Range 1..255}
      FillClr: Integer;  {Range 1..255, Add 1000 Offset to fixed RGB}
   END;
  TFontType = (font_shx,font_ttf);

  TDCTimeStamp = packed record
                     Date, Time : integer;
                  end;

   pentity = ^entity;
   entity = packed RECORD
      enttype: byte;   //aSInt;
      ltype: aSInt; // For embedded linetype to work due to parameter passing
      Width: byte;   //aSInt;
      spacing: aFloat;
      ovrshut: aFloat;
      attr: byte;   //aSInt;
      color: aSInt;
      visLvl: byte;   //aSInt;    { visibilty level (in symbols) }
      addr: entaddr;   { pointer to this entity }
      addrSpc: adsmem;
      Next: entaddr;   { pointer to the next entity }
      prev: entaddr;   { pointer to the previous entity }
      lyr: lyraddr;     { layer this line is on }
      user0: aSInt;
      user1: aSInt;
      user2: aSInt;
      user3: aSInt;
      ss: aSInt;    { selection sets that this is a member of (bitmap) }
      frstAtr: atraddr;   { pointers to the list of }
      lastAtr: atraddr;   {  attributes for this entity }
      index: longint;   { global index - number	}
      WallIndex: aSInt;    { index within a wall      }
      lastWallIndex: aSInt;{ Maximal absolute wall-index }
      last: boolean;    { true if this is the last entity in a shape }
      hatched: boolean;    { true if entity is ass. hatched}
      o2cmaterialindex: Word;
      Matrix: modmat;
      FixText: byte;        { For on-the-fly FixText in Symbols/XRefs }
      HatchLocked : byte;
      NoClip   : Byte;
      Not_Snappable : byte;
      Non_Printing : byte;
      winlinetype : Byte;
      winlinespacing : aFloat;

      unusedbytes : array [1..20] of Byte; {Unused bytes always in the end for Undo/Redo} 

      CASE aSInt OF
         entlin: (linpt1,
            linpt2: point);
         entln3: (ln3pt1,
            ln3pt2: point);
         entmrk: (mrkpnt,
            mrkvec: point;
            mrktyp,
            mrksiz: aSInt);   { in pixels }
         entarc: (arccent: point;
            arcrad: afloat;
            arcbang: afloat;
            arceang: afloat;
            arcbase: afloat;
            archite: afloat);  
         entcrc: (crccent: point;
            crcrad: afloat;
            crcbang: afloat;
            crceang: afloat;
            crcbase: afloat;
            crchite: afloat);
         entbez: (bezordr,
            beznpnt: asint;
            bezpnt: bezarr;
            bezbase,
            bezhite: afloat);
         entbsp: (bspordr,
            bspnpnt: asint;
            bsppnt: bezarr;
            bspbase,
            bsphite: afloat);
         entell: (ellcent: point;
            ellrad: point;      { a and b, like radii }
            ellbang: aFloat;
            elleang: aFloat;
            ellang: aFloat;    { angle of rotation }
            ellBase,
            ellHite: aFloat);
         entTxt: (txtpnt: point;{Point around where text will be justified}
            txtpnt2: point;{Second Point for fit/aligned text}
            txttype: RTextType;
            txtsiz: aFloat;
            txtang: aFloat;
            txtslant: aFloat;
            txtaspect: aFloat;      { aspect ratio }
            txtBase: aFloat;
            txtHite: aFloat;
            txtfon: fontstr;
            txtdisplayttf : TDisplayTTF;
            TxtWinfont: TFontType;
            fontNum: aSInt;
            txtunderline,
            txtoverline : boolean;
            txtstr: str255);   { set in dcdata }
         entDim: (
            {** Record 1 **}
            dimpt1: point;   { first dimensioned point }
            dimpt2: point;   { second dimensioned point }
            dimpt3: point;   { third point entered }

            {** Record 2 **}
            dimtxtpt: point;   { text position }
            dimexo1: aFloat;   { extension line offset for first point }
            dimexo2: aFloat;   { extension line offset for second point }
            dimexe: aFloat;   { extension line extension }
            dimdli: aFloat;   { dimension line increment }
            dimang: aFloat;   { used only in rotated }


            {** Record 3 **}
            dimdis: aFloat;   { last associative distance }
            dimangl: aFloat;   { NOT USED }
            dimovr: aFloat;   { dimension line extension amount }
            dimfudge: aFloat;   { NOT USED }
            dimtxtsize: aFloat;   { text size }
            dimarrsize: aFloat;   { arrow size }
            dimarratio: aFloat;   { arrow ratio }

            {** Record 4 **}
            dimtxtaspect: aFloat;   { text aspect ratio }
            dimtxtang: aFloat;   { text angle relative to dimension line }
            dimBase: aFloat;
            dimHite: aFloat;
            dimTxtslant: aFloat;   { text slant amount }
            diminc: aSInt; { used for changing baseline dims }
            dimtm: aSInt; { NOT USED }
            dimtxtofs: aFloat;   { text offset distance }

            {** Record 5 **}
            dimfrstpt: point;   { used with dmpt2 for dis }
            dimtxtclr: aSInt;
            dimticclr: aSInt;
            dimtolp: aFloat;   { plus tolerance }
            dimtolm: aFloat;   { minus tolerance }
            dimtol: boolean; { tolerances }
            dimlim: boolean; { limits }
            dimnolftmrk: boolean; { supress left arrow }
            dimnorhtmrk: boolean; { supress right arrow }
            dimdinstd: boolean; { DIN standard for Soft-Tech }
            dimFilledArrow: boolean; 
            DimWinfont: TFontType;

            {** Record 6 **}
            dimNlpts: aSInt; { number of leader points }
            dimtxtweight: aSInt; { text weight }
            dimtictype: aSInt; { arrow type }
            dimtype: aSInt; { dimensioning style }
            dimticweight: aSInt; { tic mark width }

            dimfont: fontstr;
            dimdispttf : TDisplayTTF;

            dimldr: boolean; { has a leader been added }
            dimtih: boolean; { text inside horiz }
            dimtoh: boolean; { text outside horiz }
            dimse1: boolean; { suppress extension line 1 }
            dimse2: boolean; { suppress extension line 2 }
            dimman: boolean; { text placed manually }
            dimassoc: boolean; { used for savevar.dimautorot for soft-tech }
            dimtad: boolean; { text above dimension line }
            dimsmallfrac : afloat;
            dimnounits :boolean;
            dimstakfrac : boolean;
            dimnosingle : boolean;

            {** Records 7, 8 and 9 **}
            dimldrpnts: ARRAY [1..ldrpts] OF point;
            { leader points }
      );

         entxref: (xrefname: symstr;
            { Not currently used, but might use this space later }
            xrefnameaddr: lgl_addr;
            xrefSpc: asint;   { Do Not Cast to an AddrSpcType }
      );

         entsym: (symname: symstr;
            symnameaddr: lgl_addr;
            symattrib_addr: lgl_addr; { do not modify this variable directly}
            symstrt: lgl_addr;   { start of the symbol header }
      );
         
         enthol: (holnpnt: aSInt;  { Number of points }
            holthick: point;     { Slab thickness }
            holslab: boolean;   { True if closed }
            holconvex: boolean;   { True if convex }
            holpnt: pntarr;   { Array of the points }
            holisln: boolarr;
            holfrstvoid,
            hollastvoid: entaddr);
         entSlb: (slbnpnt: aSInt;  { Number of points }
            slbthick: point;     { Slab thickness }
            slab: boolean;   { True if closed }
            convex: boolean;   { True if convex }
            slbpnt: pntarr;   { Array of the points }
            slbisln: boolarr;
            slbfrstvoid,
            slblastvoid: entaddr);
         entPly: (plynpnt: aSInt;  { Number of points }
            plythick: point;     { Slab thickness }
            plyslab: boolean;   { True if closed }
            plyconvex: boolean;   { True if convex }
            plypnt: pntarr;   { Array of the points }
            plyisln: boolarr;
            plyfrstvoid,
            plylastvoid: entaddr); { list of holes }
         entpln: (plnfrst: pvtaddr;
            plnlast: pvtaddr;
            plnclose: boolean;
            plnunused: boolean;
            plbase: aFloat;
            plnhite: aFloat;
            plnVoids: boolean;   { do we have voids?}
            plnCover: boolean;   { do we have a cover?}
            plnFrstvoid: entaddr;   { List of Pline-Holes }
            plinLastvoid: entaddr;
            plinType: aSInt);

         entar3: (ar3div: aSInt;
            ar3rad: aFloat;
            ar3bang,
            ar3eang: aFloat;
            ar3close: boolean);

         entcon: (condiv: aSInt;
            conrad: aFloat;
            concent: point;
            conbang,
            coneang: aFloat;
            conclose: boolean);

         entcyl: (cyldiv: aSInt;
            cylrad,
            cyllen: aFloat;
            cylbang,
            cyleang: aFloat;
            cylclose: boolean);

         enttrn: (trndiv: aSInt;
            trncent: point;
            trnrad1,
            trnrad2: aFloat;
            trnbang,
            trneang: aFloat;
            trnclose: boolean);

         entdom: (domdiv1,
            domdiv2: aSInt;
            domrad: aFloat;
            dombang1,
            dombang2,
            domeang1,
            domeang2: aFloat;
            domclose: boolean);

         enttor: (tordiv1,
            tordiv2: aSInt;
            torrad1,
            torrad2: aFloat;
            torbang1,
            torbang2,
            torenag1,
            toreang2: aFloat;
            torclose: boolean);

         entsrf: (srfpnt: pntmat;
            srfsdiv,
            srftdiv: aSInt);    // to be release later.

         entblk: (blkpnt: pnt4arr);

         entcnt: (ckpnt: pntarr;
            cntTanPnt1: point;
            cntTanpnt2: point;
            cntNpnt,
            cntType,
            cntdivs: aSInt;
            cntstiff: aFloat);

         entrev: (revbang,
            reveang: aFloat;
            revdiv1,
            revdiv2: aSInt;
            revfrst,
            revlast: lgl_addr;
            revtype: aSInt);
         entBlkAttDef: (Atttxtpnt: point;
            Atttxtpnt2: point;
            atttxttype: RTextType;
            Atttxtsiz: aFloat;
            Attang: aFloat;
            Attslant: aFloat;
            Attaspect: aFloat;      { aspect ratio }
            AttChrBase: aFloat;
            AttchrHite: aFloat;
            Attfont: fontstr;
            attdispttf : TDisplayTTF;
            AttWinfont: TFontType;
            AttfontNum: aSInt;
            AttFlags: asInt;
            AttUniqueTagNum: Longint; {Could be Used only with insert}
            AttTag,
            Attprompt,
            AttDefault: lgl_addr;
            // Use symbol data as the last parameter for this entity type. This
            // variable is not stored in database and even undo/redo system need
            // not know about this. This carries insert data when drawing.
            attsymboldataerror : boolean;
            AttSymbolData: lgl_addr);
         DCNullObject,
         DCPlainTextObject,
         DCRTFTextObject,
         DCXLSObject,
         DCWallObject,
         DCDoorObject,
         DCWindowObject,
         DCCutOutObject,
         DCUnusedObject1,
         DCUnusedObject2,
         DCUnusedObject3,
         DCUnusedObject4,
         DCUnusedObject5,
         DCUnusedObject6,
         DCUnusedObject7,
         DCUnusedObject8: (UnUsedType: aSInt;
            GroupId : TDCTimeStamp;
            SuperData: lgl_addr;
            oobj: TObject {KEEP THIS ONE LAST!! (so undo.pas doesn't need to be changed});
   END;
                  
   ssPtrType = packed RECORD
      ptr: lgl_addr;
      ofs: aSInt;
   END;

   mode_type = packed RECORD
         { Adding variables to this record should change sp_mode_type in usprecords.pas
         and make changes for DCAL as needed then}
      addrSpc: adsmem;
      srch_mode: Byte;    { 0 = line, 1 = shape, 2 = area, 3 = all }
      lyr_mode: Byte;    { 0 = current layer, 1 = all on layers, 2 = all layers
					                     3 = use entity in entaddr }
      greedy_box: byte;       { bit0 = box; bit1=greedy }

      entaddr: lgl_addr;   { start with this entity if lyr_mode = 3 }
      ssNum: aSInt;    { selection set number to use }
      ssPtr: ssPtrType;  { used when reading from a  selection set (lyr_mode = 4) }
      pnt1,                   { min point for inside test }
      pnt2: point;      { max point for inside test }
      lyr_last: lgl_addr;   { last entity on this layer }
      one_lyr: lgl_addr;   { the only layer to search, used in lyr_curr and 1lyr }
      lyr_stop: boolean;    { stop at last entity on every layer ? }
      some_ent: boolean;    { only select some entities }
      ents: ARRAY [firstent..lastent] OF boolean;
      quickcheck: boolean;
      from_dcal: boolean;
      SelectMask: Pointer;//PSelectMaskData;
      shape: boolean;    { stop if end of shape is found }
      one_ent: boolean;
      ignore: boolean;
      fence: boolean;
      atr: boolean;       { check for an attribute ? }
      atrName: string [13];  { attribute to check for }
      npnt: aSInt;
      fmin,
      fmax: point;
      ppnt: ppolyarr;   { .ofs portion of lgl_addr }
   END;
   
   state = packed record    { default type things for lines }
   {Max size of record is 64 bytes}
      color: aSInt;  {4}
      linetype: aSInt; {8}
      Width: aSInt;  {12}
      attr: aSInt;    {16}
      factor: aFloat;  {24}
      ovrshut: aFloat; {32}
      winlinetype : Byte;     {33}
      winlinefactor : aFloat; {41}
      o2cmaterialindex : Word; {43}
      vislvl: Byte; {44} { 0 = all views; 1 = 3D Only; 2 = 2D Only. Used in smart entities }
      unused : array [1..20] of byte; {Makes 64 bytes}
   END;
   pstate = ^state;

   sym_type = packed record
      addr: symaddr;
      addrSpc: adsmem;
      Name: symstr;    { file name for this symbol }
      symnameaddr: lgl_addr; {address where we will store the symbol name}
      frstent,               { first entity in this symbol }
      lastent,               { last entity in this symbol }
      frstatr,               { first database info }
      lastatr,
      o2cmaterials: lgl_addr; { last database info }
      min,
      max: point;      { extents }
      num: aSInt; { number of instances of this symbol at last call to sym_count }
      refFlag: boolean; { set by sym_ref }
      sym_fill1: boolean;
      cnt: longint; { number of entities in this symbol }
      serialno: aSInt;   { set by sym_ref }
      case asInt of
         1: (Next: symaddr;   { next symbol header }
            prev: symaddr;); { previous symbol header }
         2: (frstsym: symaddr;   { first symbol used in symbol}
            lastsym: symaddr;); { last symbol used in symbol}
   END;
   symbol = sym_type;
   psym_type = ^sym_type;

   atrname = string[atr_name_len + 1];
   db_article = packed record
      db_name: str8;
      db_extension: str8;
      db_type: aSInt;
      art_type: aSInt;
      art_name: str80;
   END;

 TAttribXData = packed record
   {This size of record not to exceed 56 bytes}
                     case byte of 
                        0 : ( data : array [0..55] of Byte);
                        1 : (
                             positn    : point;
                             txtsize   : aFloat;
                             txtang    : aFloat;
                             txtslant  : aFloat;
                             txtaspect : aFloat;
                             );
                        2 : (
                              xrefhilitecolor : asint;
                              xrefhiliteltype : asint;
                              xrefhilitespacing : afloat;
                            );
               end;

   attrib = packed record
      atrtype: aSInt;
      addr: atraddr;
      addrSpc: adsmem;
      Next: atraddr;
      prev: atraddr;
      Name: atrname;
      Visible: boolean;{ either data56 or visible is true but not both}
      data56    : boolean;{ either data56 or visible is true but not both}
      SPBvisible: Boolean; // Solid pattern bitmap fill boundary visible
      VarDataSize: Byte; // max space for str255 is 255
      txtcolor: aSInt;
      xdata     : TAttribXData;
      CASE aSInt OF
         atr_str: (str: str80);
         atr_int: (int: aSInt);
         atr_rl: (rl: aFloat);
         atr_dis: (dis: aFloat);
         atr_ang: (ang: aFloat);
         atr_pnt: (pnt: point);
         atr_addr: (lgladdr: lgl_addr);
         atr_db: (db: db_article);
         atr_str255: (shstr: str255);
   END;
              
   scanLineType = packed RECORD
      { describes a hatch scan line }
      ang: aFloat;
      org: point;
      delta: point;
      numdash: Byte;
      dash: ARRAY [0..maxDash - 1] OF aFloat;
      dashDraw: ARRAY [0..maxDash - 1] OF boolean;
         {Do not change the alignments of these fields because they match
         ScanlineAttrib record, which is not defined anymore}
      isfirst: boolean;
   END;

   HatchDefAttrib = PACKED RECORD
      islandtype: Byte;
      hatchmode: Byte;
      outlinevisible: Boolean;
      namelen: Byte;   {4}
      ang: aFloat; {12}
      scale: aFloat; {20}
      Org: Point; {44}
      Name: str255;
   End;
        
   VNameStr = str80; 
   view_type = packed record
      addr: lgl_addr;    { Address of stored view }
      addrspc: adsmem;      { Current drawing file }
      Next: lgl_addr;    { Next view in list if any }
      prev: lgl_addr;    { Previous view in list if any }
      viewmode: aSInt;     { Type of projection }
      vmat: modmat;      { Viewing xform matrix }
      cmat: modmat;      { Post-clipping xform matrix }
      emat: modmat;      { Editing matrix }
      imat: modmat;      { Inverse of editing matrix }
      prspct: boolean;     { True if perspective mode }
      clipon: boolean;     { True if clipping cubes on }
      name_old: string[9];  {Made variable old for DCAL}  { Name of view if any }
      clpmin: point;       { Clipping cube minimum }
      clpmax: point;       { Clipping cube maximum }
      xr: aFloat;       { Window/viewport x position }
      yr: aFloat;       { Window/viewport y position }
      scale: aFloat;       { Window/viewport scale }
      scalei: aSInt;     { View drawing scale }
      perscent: point;       { Perspective center of view }
      perseye: point;       { Perspective eyepoint }
      persdis: aFloat;       { Perspective distance }
      togglelyr: boolean;     { True of toggling of layers on }
      frstlyr: lgl_addr;    { Address of first entry in layer list }
      lastlyr: lgl_addr;    { Address of last entry in layer list }
      currlyr: lgl_addr;    { Address of current layer for this view }
      flag1: byte;        { General purpose flag }
      { Changed in version 7.5 }
      flag2: byte;        { General purpose flag }
      coneang: aFloat; 
      frstatr,
      lastatr: lgl_addr;
      clipNoZ     : boolean;
      name         : VNameStr;
   END;

   list_type = packed RECORD
      addrspc: adsmem;
      addr,
      Next,
      prev: lgl_addr;
      Data: ARRAY [1..6] OF lgl_addr;
   END;



implementation


end.
